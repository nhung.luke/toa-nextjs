import InstaCarousel from "@/src/components/sliders/InstaCarousel";
import TestimonialsCarousel from "@/src/components/sliders/TestimonialsCarousel";
import Layouts from "@/src/layouts/Layouts";
import {sliderProps} from "@/src/sliderProps";
import Link from "next/link";
import {Swiper, SwiperSlide} from "swiper/react";
import Image from 'next/image'
import {useEffect} from "react";
import ReservationForm from "@/src/components/ReservationForm";
import BestMenu from "@/src/components/Menu/BestMenu";

const WelcomeToToA = () => {
    return <div className="subtitles">Willkommen beim Taste-<span style={{color: "red"}}>of</span>-Asia</div>
}
const Index = () => {

    return (
        <Layouts meta={<>
            <title>Entdecken Sie vietnamesische und japanische Köstlichkeiten</title>
        </>}>
            {/* Section Started Slider */}
            <section className="section kf-started-slider">
                <Swiper
                    {...sliderProps.mainSliderSelector}
                    className="swiper-container"
                >
                    <div className="swiper-wrapper">
                        <SwiperSlide className="swiper-slide">
                            <div className="kf-started-item">
                                <div className="slide js-parallax">
                                    <Image
                                        src="/images/toa_hero1.jpg"
                                        fill
                                        objectFit={"cover"}
                                        alt="Background Image"
                                        style={{opacity: 0.7}}
                                    />
                                </div>
                                <div className="container">
                                    <div className="description align-left element-anim-1">
                                        <WelcomeToToA/>
                                        <h2 className="name text-anim-1" data-splitting="chars">
                                            Reservieren Sie jetzt<br/>
                                            Ihren Tisch.
                                        </h2>
                                        <div className="kf-bts">
                                            <Link href="/reservation" target={"_blank"}
                                                  className="kf-btn">
                                                <span>Jetzt buchen</span>
                                                <i className="fas fa-chevron-right"/>
                                            </Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </SwiperSlide>
                        <SwiperSlide className="swiper-slide">
                            <div className="kf-started-item">
                                <div className="slide js-parallax">
                                    <Image
                                        src="/images/toa_hero2.jpg"

                                        fill
                                        objectFit={"cover"}
                                        alt="Background Image"
                                        style={{opacity: 0.7}}
                                    />
                                </div>
                                <div className="container">
                                    <div className="description align-left element-anim-1">
                                        <WelcomeToToA/>
                                        <h2 className="name text-anim-1" data-splitting="chars">
                                            Asiatische Köstlichkeiten <br/> erwarten Sie
                                        </h2>
                                        <div className="kf-bts">
                                            <Link href="/reservation" target={"_blank"}
                                                  className="kf-btn">
                                                <span>Jetzt buchen</span>
                                                <i className="fas fa-chevron-right"/>
                                            </Link>
                                            {/*<Link href="reservation" className="kf-btn dark-btn">*/}
                                            {/*  <span>get delivery</span>*/}
                                            {/*  <i className="fas fa-chevron-right" />*/}
                                            {/*</Link>*/}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </SwiperSlide>
                        <SwiperSlide className="swiper-slide">
                            <div className="kf-started-item">
                                <div className="slide js-parallax">
                                    <Image
                                        src="/images/toa_hero3.jpg"
                                        fill
                                        objectFit={"cover"}
                                        alt="Background Image"
                                        style={{opacity: 0.7}}
                                    />
                                </div>
                                <div className="container">
                                    <div className="description align-left element-anim-1">
                                        <WelcomeToToA/>
                                        <h2 className="name text-anim-1" data-splitting="chars">
                                            Kreuzung kulinarischer  <br/> Traditionen.
                                        </h2>
                                        <div className="kf-bts">
                                            <Link href="menu" className="kf-btn">
                                                <span>Mehr entdecken</span>
                                                <i className="fas fa-chevron-right"/>
                                            </Link>
                                            {/*<Link href="reservation" className="kf-btn dark-btn">*/}
                                            {/*  <span>get delivery</span>*/}
                                            {/*  <i className="fas fa-chevron-right" />*/}
                                            {/*</Link>*/}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </SwiperSlide>
                    </div>
                    <div className="swiper-button-prev"/>
                    <div className="swiper-button-next"/>
                </Swiper>
            </section>
            {/* Section Category */}
            <section
                className="section kf-category"
                style={{backgroundImage: "url(images/category_bg.jpg)"}}
            >
                <div className="container">
                    <div className="row">
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                            <div
                                className="kf-category-items element-anim-1 scroll-animate"
                                data-animate="active"
                            >
                                <div className="kf-category-item">
                                    <div className="image kf-image-hover">
                                        <Link href="menu">
                                            <Image
                                                src="/images/toa_food2.jpg"
                                                fill
                                                style={{objectFit: "cover"}}
                                                alt="Background Image"
                                            />
                                        </Link>
                                    </div>
                                    <div className="desc">
                                        <h5 className="name">VORSPEISEN</h5>
                                    </div>
                                </div>
                                <div className="kf-category-item">
                                    <div className="image kf-image-hover">
                                        <Link href="menu">
                                            <Image
                                                src="/images/toa_food1.jpg"
                                                fill
                                                style={{objectFit: "cover"}}
                                                alt="Background Image"
                                            />
                                        </Link>
                                    </div>
                                    <div className="desc">
                                        <h5 className="name">HAUPTSPEISEN</h5>
                                    </div>
                                </div>
                                <div className="kf-category-item">
                                    <div className="image kf-image-hover">
                                        <Link href="menu">
                                            <Image
                                                src="/images/toa_food3.jpg"
                                                fill
                                                style={{objectFit: "cover"}}
                                                alt="Background Image"
                                            />
                                        </Link>
                                    </div>
                                    <div className="desc">
                                        <h5 className="name">SUSHI</h5>
                                    </div>
                                </div>
                                <div className="kf-category-item">
                                    <div className="image kf-image-hover">
                                        <Link href="menu">
                                            <Image
                                                src="/images/toa_drinks.jpg"
                                                fill
                                                style={{objectFit: "cover"}}
                                                alt="Background Image"
                                            />
                                        </Link>
                                    </div>
                                    <div className="desc">
                                        <h5 className="name">DRINKS</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-5 offset-lg-1 align-self-center">
                            <div className="kf-titles">
                                <div
                                    className="kf-subtitle element-anim-1 scroll-animate"
                                    data-animate="active"
                                >
                                    Entdecken Sie Die Geschmäcker Asiens
                                </div>
                                <h3
                                    className="kf-title element-anim-1 scroll-animate"
                                    data-animate="active"
                                >
                                    Lassen Sie uns unsere kulinarische Reise beginnen.
                                </h3>
                            </div>
                            <div
                                className="kf-text element-anim-1 scroll-animate"
                                data-animate="active"
                            >
                                <p>
                                    Wir freuen uns, Sie in unserem Restaurant begrüßen zu dürfen, wo wir stolz eine
                                    einzigartige Mischung aus vietnamesischen und japanischen Aromen servieren. Beim
                                    Durchstöbern unserer Website werden Sie eine gastronomische Reise entdecken, die Sie
                                    durch die Kulturen und kulinarischen Traditionen des Ostens und des Westens führt.
                                    Unsere Speisekarte ist eine harmonische Fusion der reichen und vielfältigen Aromen
                                    der vietnamesischen und japanischen Küche, die darauf abzielt, Ihre
                                    Geschmacksknospen zu kitzeln und Ihren Gaumen zu befriedigen. Jedes Gericht wird
                                    sorgfältig und liebevoll zubereitet, wobei nur die frischesten Zutaten verwendet
                                    werden, um ein wirklich authentisches kulinarisches Erlebnis zu gewährleisten. Also,
                                    worauf warten Sie?
                                </p>
                            </div>
                            <Link
                                href="menu"
                                className="kf-btn element-anim-1 scroll-animate"
                                data-animate="active"
                            >
                                <span>Mehr entdecken</span>
                                <i className="fas fa-chevron-right"/>
                            </Link>
                        </div>
                    </div>
                </div>
            </section>
            {/* Section Menu */}
            <section className="section kf-menu kf-parallax">
                <div className="container">
                    <div className="kf-titles align-center">
                        <div
                            className="kf-subtitle element-anim-1 scroll-animate"
                            data-animate="active"
                        >
                            Das Beste
                        </div>
                        <h3
                            className="kf-title element-anim-1 scroll-animate"
                            data-animate="active"
                        >
                            Beliebte Speisekarten
                        </h3>
                    </div>
                    <div
                        className="kf-menu-items"
                    >
                        <div className="" style={{opacity: 0.1}}>
                            <Image
                                src="/images/toa_menu_logo.png"
                                layout='fill'
                                objectFit='contain'
                                alt="Background Image"
                            />
                        </div>
                        <div className="row">


                            <BestMenu name={'2. Goi Du Du - Papaya Salat.'}
                                      subName={'RCM Grüne Papaya, Sojasprossen, Karotten, Kräuter, Erdnüsse, Knoblauch, Chili, hausgemachte Dressing,'}
                                      price={'6.5'}
                                      image={'images/bestMenu/2.jpeg'} />

                             <BestMenu name={'4. Goi Cuon - Sommerrollen'}
                                       subName={'Reispapier, Reisnudeln, Ei, Salat, Gurke, Karotten, Koriander und, Mango.wahlweise mit'}
                                       price={'6.5'}
                                       image={'images/bestMenu/3.jpeg'} />

                             <BestMenu
                                 name={'5. Nem Ran - Frühlingsrollen'}
                                 subName={'Traditionelle Frühlingsrollen gefüllt mit Hackfleisch, Garnelen, Karotten, Morcheln und Glasnudeln'}
                                 price={'6.9'}
                                 image={'images/bestMenu/4.jpeg'}
                                 imageSub={'images/bestMenu/4Sub.jpeg'}
                             />

                            <BestMenu
                                 name={'6. Tom Chien Com - Klebreisflocken'}
                                 subName={'Garnelen 2 Black Tiger -Garnelen umwickelt von. knusprigen Klebreisflocken, dazu hausgemachter Dip'}
                                 price={'7,9'}
                                 image={'images/bestMenu/5.jpeg'} />

                            <BestMenu
                                 name={'7. Wantan Chien - Gebackene Wantan'}
                                 subName={'4 knusprige Teigtaschen mit gehacktem Hühnerfleisch und Garnelen.als Füllung, dazu süß-saurer Soße'}
                                 price={'6.9'}
                                 image={'images/bestMenu/6.jpeg'} />


                            <BestMenu
                                 name={'8. Dimsum - Teigtaschen'}
                                 subName={'Gedämpfte Teigtaschen mit verschiedenen Füllungen, dazu süß-saurer Soße'}
                                 price={'5.9'}
                                 image={'images/bestMenu/7.jpeg'} />

                            <BestMenu
                                 name={'9. Khaivi - Vorspeise-set'}
                                 subName={'Große Platte mit.'}
                                 price={'25,9'}
                                 image={'images/bestMenu/8.jpeg'} />

                            <BestMenu
                                name={'11. Edamame'}
                                subName={'Gedämpfte japanische Bohnen mit Meersalz'}
                                price={'5,5'}
                                image={'images/bestMenu/9.png'} />

                            <BestMenu
                                 name={'12. Wakame'}
                                 subName={'Eingelegter Seetang-Salat mit Sesam'}
                                 price={'5,5'}
                                 image={'images/bestMenu/10.jpeg'} />


                            <BestMenu
                                 name={'13. Kim Chi Salat (scharf)'}
                                 subName={'Eingelegter Chinakohl'}
                                 price={'5.9'}
                                 image={'images/bestMenu/11.jpeg'}
                                 imageSub={'images/bestMenu/11Sub.jpg'}
                            />

                             <BestMenu
                                 name={'15. Yaki Tori.'}
                                 subName={'3 stk gegrillte Hühnerspieße mit Teriyaki-Soßeund Sesam'}
                                 price={'6.9'}
                                 image={'images/bestMenu/12.png'} />

                            <BestMenu
                                 name={'16.Oktopus. Salat Oktopus'}
                                 subName={'Grüner Papayasalat mit gegrilltem Oktopus, Sesam, Erdnüssen, Limetten-Vinaigrette und Röstzwiebeln'}
                                 price={'13,9'}
                                 image={'images/bestMenu/13.jpg'} />

                            <BestMenu
                                name={'18. Tuna Tataki'}
                                subName={'Leicht gegrillter Thunfisch im'}
                                price={'11,5'}
                                image={'images/bestMenu/14.jpg'}
                                imageSub={'images/bestMenu/14Sub.jpg'}
                            />

                            <BestMenu name={'19.Pho - Reisbandnudel-Suppe'}
                                      subName={'Traditionelle Suppe des Hauses'}
                                      price={'13,9'}
                                      image={'images/bestMenu/1.jpg'}
                                      imageSub={'images/bestMenu/1.png'}
                            />

                            <BestMenu
                                name={'22. Udon Xao - Gebratene Udon'}
                                subName={'in Wok geschwenkte Udonnudeln, Gemüse und Teriyaki-Soße wahlweise mit'}
                                price={'13,9'}
                                imageSub={'images/bestMenu/15.png'}
                                image={'images/bestMenu/15Sub.jpg'}

                            />

                            <BestMenu
                                name={'28. Uc Vit Nuong - Ducky Duck.'}
                                subName={'250g Gegrillte weibliche Barbarie-Entebrust mit marktfrischem Gemüse in Deluxe Sauce'}
                                price={'20,9'}
                                image={'images/bestMenu/16.png'}
                                imageSub={'images/bestMenu/16Sub.jpg'}
                            />

                            <BestMenu
                                name={'29. Ca Hoi Nuong - Teri Sake Lachs'}
                                subName={'6Stk gegrillte Riesengarnelen ,Spargel, Zuckererbsenund ,SaisonalemGemüse, abgeschmeckt mit Reis in, serviert mit Teriyaki-Soße und Tamarind-Soße'}
                                price={'24,9'}
                                image={'images/bestMenu/17.jpg'}
                                imageSub={'images/bestMenu/17Sub.jpg'}
                            />

                            <BestMenu
                                name={'31. Tom Nuong - Teri Garnalen'}
                                subName={'250g Gegrillter Lachsmit 3 Garnelen, serviert mit Teriyaki-Sofße und. Tamarind-Soße'}
                                price={'17,9'}
                                image={'images/bestMenu/18.png'}
                            />

                            <BestMenu
                                name={'33. Bo Bit Tet - Surf & Turf.'}
                                subName={'3Stk Garnelenspieß auf 250g medium gegrilltem argentinisches Rib-Eye Steak, begleitet von angebratenem Gemüse und der Whisky-Soße mit Süßkartoffeln'}
                                price={'26,9'}
                                image={'images/bestMenu/19.jpg'}
                                imageSub={'images/bestMenu/19Sub.jpeg'}
                            />



                            <BestMenu
                                name={'34. Bo Dua - Pineapple Gold.'}
                                subName={'Rinderhüftsteak aus dem Wok mit Zitronengras, Chili, Ananas, Sellerie, Babymais, Zuckerschoten, Tomaten, Peperoni und Zwiebeln, Serviert in der frischen Ananas, schaft'}
                                price={'21,9'}
                                image={'images/bestMenu/20.jpg'}
                                imageSub={'images/bestMenu/20Sub.jpg'}
                            />

                            <BestMenu
                                name={'35. Tom Dua - Coconut Prawn.'}
                                subName={'5Stk gebratene Riesengarnelen mit markfrischem Gemüse, Babymais in Currysoße, serviert jungen Kokosnuss'}
                                price={'22,9'}
                                image={'images/bestMenu/21.jpg'}
                                imageSub={'images/bestMenu/21Sub.png'}
                            />





                            {/*<div className="col-xs-12 col-sm-12 col-md-12 col-lg-6">*/}
                            {/*    <div*/}
                            {/*        className="kf-menu-item element-anim-1 scroll-animate"*/}
                            {/*        data-animate="active"*/}
                            {/*    >*/}
                            {/*        <div className="image kf-image-hover">*/}
                            {/*            <a href="images/toa_food_1.jpg" className="has-popup-image">*/}
                            {/*                <Image*/}
                            {/*                    src="/images/toa_food_1.jpg"*/}
                            {/*                    fill*/}
                            {/*                    style={{objectFit: "cover"}}*/}
                            {/*                    alt="Background Image"*/}
                            {/*                />*/}
                            {/*            </a>*/}
                            {/*        </div>*/}
                            {/*        <div className="desc">*/}
                            {/*            <h5 className="name">1 PHO</h5>*/}
                            {/*            <div className="subname">Traditionelle Suppe des Hauses</div>*/}
                            {/*            <div className="price">*/}
                            {/*                <span>€6.9</span>*/}
                            {/*            </div>*/}
                            {/*        </div>*/}
                            {/*    </div>*/}
                            {/*</div>*/}
                            {/*<div className="col-xs-12 col-sm-12 col-md-12 col-lg-6">*/}
                            {/*    <div*/}
                            {/*        className="kf-menu-item element-anim-1 scroll-animate"*/}
                            {/*        data-animate="active"*/}
                            {/*    >*/}
                            {/*        <div className="image kf-image-hover">*/}
                            {/*            <a href="images/toa_food_2.jpg" className="has-popup-image">*/}
                            {/*                <Image*/}
                            {/*                    src="/images/toa_food_2.jpg"*/}
                            {/*                    fill*/}
                            {/*                    style={{objectFit: "cover"}}*/}
                            {/*                    alt="Background Image"*/}
                            {/*                />*/}
                            {/*            </a>*/}
                            {/*        </div>*/}
                            {/*        <div className="desc">*/}
                            {/*            <h5 className="name">8 TOM CHIEN COM </h5>*/}
                            {/*            <div className="subname">*/}
                            {/*                Klebreisflocken-Garnelen <br/>*/}
                            {/*                2 Black Tiger-Garnelen umwickelt<br/>*/}
                            {/*                von knusprigen Klebreisflocken, dazu hausgemachter Dip*/}
                            {/*            </div>*/}
                            {/*            <div className="price">€7.5</div>*/}
                            {/*        </div>*/}
                            {/*    </div>*/}
                            {/*</div>*/}
                            {/*<div className="col-xs-12 col-sm-12 col-md-12 col-lg-6">*/}
                            {/*    <div*/}
                            {/*        className="kf-menu-item element-anim-1 scroll-animate"*/}
                            {/*        data-animate="active"*/}
                            {/*    >*/}
                            {/*        <div className="image kf-image-hover">*/}
                            {/*            <a href="images/toa_food_3.jpg" className="has-popup-image">*/}
                            {/*                <Image*/}
                            {/*                    src="/images/toa_food_3.jpg"*/}
                            {/*                    fill*/}
                            {/*                    style={{objectFit: "cover"}}*/}
                            {/*                    alt="Background Image"*/}
                            {/*                />*/}
                            {/*            </a>*/}
                            {/*        </div>*/}
                            {/*        <div className="desc">*/}
                            {/*            <h5 className="name">10 KHAIVI</h5>*/}
                            {/*            <div className="subname">Vorspeise-Set</div>*/}
                            {/*            <div className="price">*/}
                            {/*                <span>€24.0</span>*/}
                            {/*            </div>*/}
                            {/*        </div>*/}
                            {/*    </div>*/}
                            {/*</div>*/}
                            {/*<div className="col-xs-12 col-sm-12 col-md-12 col-lg-6">*/}
                            {/*    <div*/}
                            {/*        className="kf-menu-item element-anim-1 scroll-animate"*/}
                            {/*        data-animate="active"*/}
                            {/*    >*/}
                            {/*        <div className="image kf-image-hover">*/}
                            {/*            <a href="images/toa_food_4.jpg" className="has-popup-image">*/}
                            {/*                <Image*/}
                            {/*                    src="/images/toa_food_4.jpg"*/}
                            {/*                    fill*/}
                            {/*                    style={{objectFit: "cover"}}*/}
                            {/*                    alt="Background Image"*/}
                            {/*                />*/}
                            {/*            </a>*/}
                            {/*        </div>*/}
                            {/*        <div className="desc">*/}
                            {/*            <h5 className="name">29 VIT CHIEN GION</h5>*/}
                            {/*            <div className="subname">*/}
                            {/*                Knusprige Ente*/}
                            {/*            </div>*/}
                            {/*            <div className="price">15.5</div>*/}
                            {/*        </div>*/}
                            {/*    </div>*/}
                            {/*</div>*/}
                            {/*<div className="col-xs-12 col-sm-12 col-md-12 col-lg-6">*/}
                            {/*    <div*/}
                            {/*        className="kf-menu-item element-anim-1 scroll-animate"*/}
                            {/*        data-animate="active"*/}
                            {/*    >*/}
                            {/*        <div className="image kf-image-hover">*/}
                            {/*            <a href="images/toa_food_5.jpg" className="has-popup-image">*/}
                            {/*                <Image*/}
                            {/*                    src="/images/toa_food_5.jpg"*/}
                            {/*                    fill*/}
                            {/*                    style={{objectFit: "cover"}}*/}
                            {/*                    alt="Background Image"*/}
                            {/*                />*/}
                            {/*            </a>*/}
                            {/*        </div>*/}
                            {/*        <div className="desc">*/}
                            {/*            <h5 className="name">31 TOM NUONG</h5>*/}
                            {/*            <div className="subname">gegrillte Riesengarnelen</div>*/}
                            {/*            <div className="price">*/}
                            {/*                <span>€16.9</span>*/}
                            {/*            </div>*/}
                            {/*        </div>*/}
                            {/*    </div>*/}
                            {/*</div>*/}
                            {/*<div className="col-xs-12 col-sm-12 col-md-12 col-lg-6">*/}
                            {/*    <div*/}
                            {/*        className="kf-menu-item element-anim-1 scroll-animate"*/}
                            {/*        data-animate="active"*/}
                            {/*    >*/}
                            {/*        <div className="image kf-image-hover">*/}
                            {/*            <a href="images/toa_food_7.jpg" className="has-popup-image">*/}
                            {/*                <Image*/}
                            {/*                    src="/images/toa_food_7.jpg"*/}
                            {/*                    fill*/}
                            {/*                    style={{objectFit: "cover"}}*/}
                            {/*                    alt="Background Image"*/}
                            {/*                />*/}
                            {/*            </a>*/}
                            {/*        </div>*/}
                            {/*        <div className="desc">*/}
                            {/*            <h5 className="name">32 VIT CHAY</h5>*/}
                            {/*            <div className="subname">Seitan-Ente</div>*/}
                            {/*            <div className="price">*/}
                            {/*                <span>€13.5</span>*/}
                            {/*            </div>*/}
                            {/*        </div>*/}
                            {/*    </div>*/}
                            {/*</div>*/}
                            {/*<div className="col-xs-12 col-sm-12 col-md-12 col-lg-6">*/}
                            {/*    <div*/}
                            {/*        className="kf-menu-item element-anim-1 scroll-animate"*/}
                            {/*        data-animate="active"*/}
                            {/*    >*/}
                            {/*        <div className="image kf-image-hover">*/}
                            {/*            <a href="images/toa_food_8.jpg" className="has-popup-image">*/}
                            {/*                <Image*/}
                            {/*                    src="/images/toa_food_8.jpg"*/}
                            {/*                    fill*/}
                            {/*                    style={{objectFit: "cover"}}*/}
                            {/*                    alt="Background Image"*/}
                            {/*                />*/}
                            {/*            </a>*/}
                            {/*        </div>*/}
                            {/*        <div className="desc">*/}
                            {/*            <h5 className="name">33 CA HOI NUONG</h5>*/}
                            {/*            <div className="subname">gegrillter Lachs <br/>(3 Stk. Garnelen, empfehlensweise*/}
                            {/*                mit Teriyaki-Sauce)*/}
                            {/*            </div>*/}
                            {/*            <div className="price">*/}
                            {/*                <span>€19.9</span>*/}
                            {/*            </div>*/}
                            {/*        </div>*/}
                            {/*    </div>*/}
                            {/*</div>*/}
                            {/*<div className="col-xs-12 col-sm-12 col-md-12 col-lg-6">*/}
                            {/*    <div*/}
                            {/*        className="kf-menu-item element-anim-1 scroll-animate"*/}
                            {/*        data-animate="active"*/}
                            {/*    >*/}
                            {/*        <div className="image kf-image-hover">*/}
                            {/*            <a href="images/toa_food_9.jpg" className="has-popup-image">*/}
                            {/*                <Image*/}
                            {/*                    src="/images/toa_food_9.jpg"*/}
                            {/*                    fill*/}
                            {/*                    style={{objectFit: "cover"}}*/}
                            {/*                    alt="Background Image"*/}
                            {/*                />*/}
                            {/*            </a>*/}
                            {/*        </div>*/}
                            {/*        <div className="desc">*/}
                            {/*            <h5 className="name">34 CA NGU NUONG</h5>*/}
                            {/*            <div className="subname">*/}
                            {/*                gegrillter Thunfisch <br/>(3 Stk. Garnelen, empfehlensweise mit*/}
                            {/*                Teriyaki-Sauce)*/}
                            {/*            </div>*/}
                            {/*            <div className="price">€20.90</div>*/}
                            {/*        </div>*/}
                            {/*    </div>*/}
                            {/*</div>*/}
                            {/*<div className="col-xs-12 col-sm-12 col-md-12 col-lg-6">*/}
                            {/*    <div*/}
                            {/*        className="kf-menu-item element-anim-1 scroll-animate"*/}
                            {/*        data-animate="active"*/}
                            {/*    >*/}
                            {/*        <div className="image kf-image-hover">*/}
                            {/*            <a href="images/toa_food_13.jpg" className="has-popup-image">*/}
                            {/*                <Image*/}
                            {/*                    src="/images/toa_food_13.jpg"*/}
                            {/*                    fill*/}
                            {/*                    style={{objectFit: "cover"}}*/}
                            {/*                    alt="Background Image"*/}
                            {/*                />*/}
                            {/*            </a>*/}
                            {/*        </div>*/}
                            {/*        <div className="desc">*/}
                            {/*            <h5 className="name">37 DEEP BLUE OCEAN</h5>*/}
                            {/*            <div className="subname">*/}
                            {/*                Gegrillte Riesengarnelen, Jakobsmuscheln und Oktopus mit gegrilltem Gemüse,*/}
                            {/*                Kräuterseitlinge mit Tamarind-Soße Grilled king prawns, scallops and octopus*/}
                            {/*                with grilled vegetables, king oyster mushrooms with tamarind sauce*/}
                            {/*            </div>*/}
                            {/*            <div className="price">€26.90</div>*/}
                            {/*        </div>*/}
                            {/*    </div>*/}
                            {/*</div>*/}
                            {/*<div className="col-xs-12 col-sm-12 col-md-12 col-lg-6">*/}
                            {/*    <div*/}
                            {/*        className="kf-menu-item element-anim-1 scroll-animate"*/}
                            {/*        data-animate="active"*/}
                            {/*    >*/}
                            {/*        <div className="image kf-image-hover">*/}
                            {/*            <a href="images/toa_food_10.jpg" className="has-popup-image">*/}
                            {/*                <Image*/}
                            {/*                    src="/images/toa_food_10.jpg"*/}
                            {/*                    fill*/}
                            {/*                    style={{objectFit: "cover"}}*/}
                            {/*                    alt="Background Image"*/}
                            {/*                />*/}
                            {/*            </a>*/}
                            {/*        </div>*/}
                            {/*        <div className="desc">*/}
                            {/*            <h5 className="name">38. SURF & TURF</h5>*/}
                            {/*            <div className="subname">. SURF & TURF 26,9*/}
                            {/*                Garnelenspieß auf 250g medium gegrilltem*/}
                            {/*                argentinisches Rib-Eye Steak, begleitet von*/}
                            {/*                angebratenem Gemüse un der KOHAKU*/}
                            {/*                Whisky-Soße mit Süßkartoffeln*/}
                            {/*                Prawn skewers on 250g medium grilled*/}
                            {/*                Argentinian rib-eye steak, accompanied by*/}
                            {/*                roasted vegetables and KOHAKU whiskey*/}
                            {/*                sauce with sweet potatoes*/}
                            {/*            </div>*/}
                            {/*            <div className="price">*/}
                            {/*                <span>€26.9</span>*/}
                            {/*            </div>*/}
                            {/*        </div>*/}
                            {/*    </div>*/}
                            {/*</div>*/}
                            {/*<div className="col-xs-12 col-sm-12 col-md-12 col-lg-6">*/}
                            {/*    <div*/}
                            {/*        className="kf-menu-item element-anim-1 scroll-animate"*/}
                            {/*        data-animate="active"*/}
                            {/*    >*/}
                            {/*        <div className="image kf-image-hover">*/}
                            {/*            <a href="images/toa_food_11.jpg" className="has-popup-image">*/}
                            {/*                <Image*/}
                            {/*                    src="/images/toa_food_11.jpg"*/}
                            {/*                    fill*/}
                            {/*                    style={{objectFit: "cover"}}*/}
                            {/*                    alt="Background Image"*/}
                            {/*                />*/}
                            {/*            </a>*/}
                            {/*        </div>*/}
                            {/*        <div className="desc">*/}
                            {/*            <h5 className="name">39. PINEAPPLE GOLD</h5>*/}
                            {/*            <div className="subname">*/}
                            {/*                Rinderhüftsteak aus dem Wok mit*/}
                            {/*                Zitronengras, Chili, Ananas, Sellerie,*/}
                            {/*                Babymais, Zuckerschoten, Tomaten,*/}
                            {/*                Peperoni und Zwiebeln, Serviert in der*/}
                            {/*                frischen Ananas, schaft*/}
                            {/*                Beef sirloin steak from the wok with*/}
                            {/*                lemongrass, chili, pineapple, celery, baby*/}
                            {/*                corn, snow peas, tomatoes, pepperoni and*/}
                            {/*                onions, served in the fresh pineapple shaft*/}
                            {/*            </div>*/}
                            {/*            <div className="price">*/}
                            {/*                <span>€26.9</span>*/}
                            {/*            </div>*/}
                            {/*        </div>*/}
                            {/*    </div>*/}
                            {/*</div>*/}
                            {/*<div className="col-xs-12 col-sm-12 col-md-12 col-lg-6">*/}
                            {/*    <div*/}
                            {/*        className="kf-menu-item element-anim-1 scroll-animate"*/}
                            {/*        data-animate="active"*/}
                            {/*    >*/}
                            {/*        <div className="image kf-image-hover">*/}
                            {/*            <a href="images/toa_food_12.jpg" className="has-popup-image">*/}
                            {/*                <Image*/}
                            {/*                    src="/images/toa_food_12.jpg"*/}
                            {/*                    fill*/}
                            {/*                    style={{objectFit: "cover"}}*/}
                            {/*                    alt="Background Image"*/}
                            {/*                />*/}
                            {/*            </a>*/}
                            {/*        </div>*/}
                            {/*        <div className="desc">*/}
                            {/*            <h5 className="name">74 Taste of Asia Extreme Special (8 Stk.)</h5>*/}
                            {/*            <div className="subname">*/}
                            {/*                Garnelentempura, Lachs, Lachshaut, Tamago, <br/>*/}
                            {/*                Gurke, Masago, Tobiko, Unagisoße*/}
                            {/*            </div>*/}
                            {/*            <div className="price">*/}
                            {/*                <span>€14.9</span>*/}
                            {/*            </div>*/}
                            {/*        </div>*/}
                            {/*    </div>*/}
                            {/*</div>*/}
                        </div>
                    </div>
                </div>
            </section>
            {/* Section Team */}
            {/*<section className="section kf-team kf-section-transparent">
        <div className="container">
          <div className="kf-titles align-center">
            <div
              className="kf-subtitle element-anim-1 scroll-animate"
              data-animate="active"
            >
              Experience Team Member
            </div>
            <h3
              className="kf-title element-anim-1 scroll-animate"
              data-animate="active"
            >
              Meet Our Professional Chefs
            </h3>
          </div>
          <div className="kf-team-items row">
            <div className="col-xs-12 col-sm-12 col-md-6 col-lg-3">
              <div
                className="kf-team-item element-anim-1 scroll-animate"
                data-animate="active"
                style={{ backgroundColor: "#090c0f" }}
              >
                <div className="desc">
                  <h5 className="name">Anthony J. Bowman</h5>
                  <div className="subname">Senior Chefs</div>
                </div>
                <div className="image kf-image-hover">
                  <img src="images/team1.jpg" alt="image" />
                  <div className="info">
                    <div className="label">bowmankf@gmail.com</div>
                    <div className="label">+012 (345) 678 99</div>
                  </div>
                  <div className="social">
                    <a href="#">
                      <i className="fab fa-facebook-f" />
                    </a>
                    <a href="#">
                      <i className="fab fa-twitter" />
                    </a>
                    <a href="#">
                      <i className="fab fa-linkedin" />
                    </a>
                    <a href="#">
                      <i className="fab fa-youtube" />
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-xs-12 col-sm-12 col-md-6 col-lg-3">
              <div
                className="kf-team-item element-anim-1 scroll-animate"
                data-animate="active"
                style={{ backgroundColor: "#090c0f" }}
              >
                <div className="desc">
                  <h5 className="name">Kenny V. Gonzalez</h5>
                  <div className="subname">Senior Chefs</div>
                </div>
                <div className="image kf-image-hover">
                  <img src="images/team2.jpg" alt="image" />
                  <div className="info">
                    <div className="label">gonzalezkf@gmail.com</div>
                    <div className="label">+012 (345) 678 99</div>
                  </div>
                  <div className="social">
                    <a href="#">
                      <i className="fab fa-facebook-f" />
                    </a>
                    <a href="#">
                      <i className="fab fa-twitter" />
                    </a>
                    <a href="#">
                      <i className="fab fa-linkedin" />
                    </a>
                    <a href="#">
                      <i className="fab fa-youtube" />
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-xs-12 col-sm-12 col-md-6 col-lg-3">
              <div
                className="kf-team-item element-anim-1 scroll-animate"
                data-animate="active"
                style={{ backgroundColor: "#090c0f" }}
              >
                <div className="desc">
                  <h5 className="name">Joseph M. Lawrence</h5>
                  <div className="subname">Senior Chefs</div>
                </div>
                <div className="image kf-image-hover">
                  <img src="images/team3.jpg" alt="image" />
                  <div className="info">
                    <div className="label">lawrencekf@gmail.com</div>
                    <div className="label">+012 (345) 678 99</div>
                  </div>
                  <div className="social">
                    <a href="#">
                      <i className="fab fa-facebook-f" />
                    </a>
                    <a href="#">
                      <i className="fab fa-twitter" />
                    </a>
                    <a href="#">
                      <i className="fab fa-linkedin" />
                    </a>
                    <a href="#">
                      <i className="fab fa-youtube" />
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-xs-12 col-sm-12 col-md-6 col-lg-3">
              <div
                className="kf-team-item element-anim-1 scroll-animate"
                data-animate="active"
                style={{ backgroundColor: "#090c0f" }}
              >
                <div className="desc">
                  <h5 className="name">Charles K. Smith</h5>
                  <div className="subname">Senior Chefs</div>
                </div>
                <div className="image kf-image-hover">
                  <img src="images/team4.jpg" alt="image" />
                  <div className="info">
                    <div className="label">smithkf@gmail.com</div>
                    <div className="label">+012 (345) 678 99</div>
                  </div>
                  <div className="social">
                    <a href="#">
                      <i className="fab fa-facebook-f" />
                    </a>
                    <a href="#">
                      <i className="fab fa-twitter" />
                    </a>
                    <a href="#">
                      <i className="fab fa-linkedin" />
                    </a>
                    <a href="#">
                      <i className="fab fa-youtube" />
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>*/}
            {/* Section Reservation */}

            <section className="section kf-reservation kf-section-no-margin">
                <div className="container">
                    <div
                        className="kf-reservation-form element-anim-1 scroll-animate"
                        data-animate="active"
                    >
                        <div className="image-left">

                            <Image
                                src="/images/toa_reservation1.jpeg"
                                layout='fill'
                                objectFit='cover'
                                alt="Background Image"
                            />
                        </div>
                        <div className="image-right">

                            <Image
                                src="/images/toa_kg_3.jpg"
                                layout='fill'
                                objectFit='cover'
                                alt="Background Image"
                            />
                        </div>
                        <ReservationForm />
                        <div className="alert-success" style={{display: "none"}}>
                            <p>Thanks, your message is sent successfully.</p>
                        </div>
                    </div>
                </div>
            </section>
            {/* Section About-2 */}
            <section
                className="section kf-about-2"
                style={{backgroundImage: "url(images/category_bg.jpg)"}}
            >
                <div className="container">
                    <div className="row">
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-5 align-self-center">
                            <div className="kf-titles">
                                <div
                                    className="kf-subtitle element-anim-1 scroll-animate"
                                    data-animate="active"
                                >
                                    Taste of Asia
                                </div>
                                <h3
                                    className="kf-title element-anim-1 scroll-animate"
                                    data-animate="active"
                                >
                                    Genießen Sie eine schöne und angenehme  <br/> Zeit  mit unseren Speisen.
                                </h3>
                            </div>
                            <div
                                className="kf-text element-anim-1 scroll-animate"
                                data-animate="active"
                            >
                                {/*<p>*/}
                                {/*  Sed ut perspiciatis unde omnis iste natus error voluptate*/}
                                {/*  accusantium doloremque laudantium, totam rem aperiam eaque*/}
                                {/*  ipsa quae abillo inventore veritatis*/}
                                {/*</p>*/}
                            </div>
                            <div className="kf-choose-list">
                                <ul>
                                    <li
                                        className="element-anim-1 scroll-animate"
                                        data-animate="active"
                                    >
                                        {/*<div className="icon">*/}
                                        {/*  <img src="images/choose_icon2.png" alt="image" />*/}
                                        {/*</div>*/}
                                        {/*<div className="desc">*/}
                                        {/*  <h5 className="name">100% ISO Certification</h5>*/}
                                        {/*  <div className="subname">*/}
                                        {/*    Sed ut perspiciatis unde omnis iste natus error*/}
                                        {/*    voluptate accusantium doloremque*/}
                                        {/*  </div>*/}
                                        {/*</div>*/}
                                    </li>
                                </ul>
                            </div>
                            <Link
                                href="menu"
                                className="kf-btn element-anim-1 scroll-animate"
                                data-animate="active"
                            >
                                <span>Mehr entdecken</span>
                                <i className="fas fa-chevron-right"/>
                            </Link>
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-6 offset-lg-1 align-self-center">
                            <div className="kf-services-items-2 row">
                                <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div
                                        className="kf-services-item-2 element-anim-1 scroll-animate"
                                        data-animate="active"
                                    >
                                        <div className="image">
                                            <img src="images/thumb-up.png" alt="image"/>
                                        </div>
                                        <div className="desc">
                                            <h5 className="name">Unvergessliche Aromen</h5>
                                            <div className="subname">Von unserer Küche auf Ihren Tisch</div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div
                                        className="kf-services-item-2 element-anim-1 scroll-animate"
                                        data-animate="active"
                                    >
                                        <div className="image">
                                            <img src="images/healthy-food.png" alt="image"/>
                                        </div>
                                        <div className="desc">
                                            <h5 className="name">Frische Zutaten</h5>
                                            <div className="subname">Qualität, die man schmeckt</div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div
                                        className="kf-services-item-2 element-anim-1 scroll-animate"
                                        data-animate="active"
                                    >
                                        <div className="image">
                                            <img src="images/chef.png" alt="image"/>
                                        </div>
                                        <div className="desc">
                                            <h5 className="name">Erfahrene Köche</h5>
                                            <div className="subname">Fachkenntnisse in jedem Bissen</div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div
                                        className="kf-services-item-2 element-anim-1 scroll-animate"
                                        data-animate="active"
                                    >
                                        <div className="image">
                                            <img src="images/ramen.png" alt="image"/>
                                        </div>
                                        <div className="desc">
                                            <h5 className="name">Innovation & Tradition</h5>
                                            <div className="subname">Eine perfekte Fusion</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            {/* Section Testimonials Carousel */}
            <TestimonialsCarousel/>
            {/* Section Video */}
            {/*<div className="section kf-video kf-video-full">
        <VideoPlayer
          videoBg={"images/video_bg.jpg"}
          extraClass={"kf-parallax"}
        />
      </div>*/}
            {/* Section Numbers-2 */}
            {/*<section className="section kf-numbers-2 section-bg">*/}
            {/*  <div className="container">*/}
            {/*    <div className="kf-numbers-items-2 row">*/}
            {/*      <div className="col-xs-12 col-sm-12 col-md-6 col-lg-3">*/}
            {/*        <div*/}
            {/*          className="kf-numbers-item-2 element-anim-1 scroll-animate"*/}
            {/*          data-animate="active"*/}
            {/*        >*/}
            {/*          <div className="icon">*/}
            {/*            <i className="las la-gem" />*/}
            {/*          </div>*/}
            {/*          <div className="num">256+</div>*/}
            {/*          <div className="desc">*/}
            {/*            <h5 className="name">Premium Clients</h5>*/}
            {/*            <div className="subname">Sed ut perspiciatis unde</div>*/}
            {/*          </div>*/}
            {/*        </div>*/}
            {/*      </div>*/}
            {/*      <div className="col-xs-12 col-sm-12 col-md-6 col-lg-3">*/}
            {/*        <div*/}
            {/*          className="kf-numbers-item-2 element-anim-1 scroll-animate"*/}
            {/*          data-animate="active"*/}
            {/*        >*/}
            {/*          <div className="icon">*/}
            {/*            <i className="las la-user-tie" />*/}
            {/*          </div>*/}
            {/*          <div className="num">36+</div>*/}
            {/*          <div className="desc">*/}
            {/*            <h5 className="name">Professional Chefs</h5>*/}
            {/*            <div className="subname">Sed ut perspiciatis unde</div>*/}
            {/*          </div>*/}
            {/*        </div>*/}
            {/*      </div>*/}
            {/*      <div className="col-xs-12 col-sm-12 col-md-6 col-lg-3">*/}
            {/*        <div*/}
            {/*          className="kf-numbers-item-2 element-anim-1 scroll-animate"*/}
            {/*          data-animate="active"*/}
            {/*        >*/}
            {/*          <div className="icon">*/}
            {/*            <i className="las la-trophy" />*/}
            {/*          </div>*/}
            {/*          <div className="num">753+</div>*/}
            {/*          <div className="desc">*/}
            {/*            <h5 className="name">Winning Awards</h5>*/}
            {/*            <div className="subname">Sed ut perspiciatis unde</div>*/}
            {/*          </div>*/}
            {/*        </div>*/}
            {/*      </div>*/}
            {/*      <div className="col-xs-12 col-sm-12 col-md-6 col-lg-3">*/}
            {/*        <div*/}
            {/*          className="kf-numbers-item-2 element-anim-1 scroll-animate"*/}
            {/*          data-animate="active"*/}
            {/*        >*/}
            {/*          <div className="icon">*/}
            {/*            <i className="lar la-grin-stars" />*/}
            {/*          </div>*/}
            {/*          <div className="num">100+</div>*/}
            {/*          <div className="desc">*/}
            {/*            <h5 className="name">5 Star Reviews</h5>*/}
            {/*            <div className="subname">Sed ut perspiciatis unde</div>*/}
            {/*          </div>*/}
            {/*        </div>*/}
            {/*      </div>*/}
            {/*    </div>*/}
            {/*  </div>*/}
            {/*</section>*/}
            {/* Section CTA */}
            <section
                className="section kf-cta kf-parallax"
                style={{backgroundImage: "url(images/cta_bg.jpg)"}}
            >
                <div className="container">
                    <div className="row">
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-8">
                            <div className="kf-titles">
                                <div
                                    className="kf-subtitle element-anim-1 scroll-animate"
                                    data-animate="active"
                                >
                                    Reservierung
                                </div>
                                <h3
                                    className="kf-title element-anim-1 scroll-animate"
                                    data-animate="active"
                                >
                                    Sie benötigen einen Tisch für Ihre Familie
                                </h3>
                            </div>
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-4 align-self-center align-right">
                            <Link
                                href="/reservation" target={"_blank"}
                                className="kf-btn element-anim-1 scroll-animate"
                                data-animate="active"
                            >
                                <span>Jetzt buchen</span>
                                <i className="fas fa-chevron-right"/>
                            </Link>
                        </div>
                    </div>
                </div>
            </section>
            {/* Section Latest Blog */}
            {/*<section className="section kf-latest-blog section-bg">*/}
            {/*  <div className="container">*/}
            {/*    <div className="kf-titles align-center">*/}
            {/*      <div*/}
            {/*        className="kf-subtitle element-anim-1 scroll-animate"*/}
            {/*        data-animate="active"*/}
            {/*      >*/}
            {/*        Get Every Single Update*/}
            {/*      </div>*/}
            {/*      <h3*/}
            {/*        className="kf-title element-anim-1 scroll-animate"*/}
            {/*        data-animate="active"*/}
            {/*      >*/}
            {/*        Read Some Latest Blog &amp; News*/}
            {/*      </h3>*/}
            {/*    </div>*/}
            {/*    <div className="kf-blog-grid-items row">*/}
            {/*      <div className="col-xs-12 col-sm-12 col-md-12 col-lg-4">*/}
            {/*        <div*/}
            {/*          className="kf-blog-grid-item element-anim-1 scroll-animate"*/}
            {/*          data-animate="active"*/}
            {/*        >*/}
            {/*          <div className="image kf-image-hover">*/}
            {/*            <Link href="blog-single">*/}
            {/*              <img src="images/latest_blog1.jpg" alt="image" />*/}
            {/*            </Link>*/}
            {/*          </div>*/}
            {/*          <div className="desc">*/}
            {/*            <h5 className="name">*/}
            {/*              SWR React Hooks With Next Increm Ental Static Regeneration*/}
            {/*            </h5>*/}
            {/*            <div className="kf-date">*/}
            {/*              <i className="far fa-calendar-alt" />*/}
            {/*              25 Sep 2021*/}
            {/*            </div>*/}
            {/*            <div className="kf-comm">*/}
            {/*              <i className="far fa-comments" />*/}
            {/*              Comments (7)*/}
            {/*            </div>*/}
            {/*          </div>*/}
            {/*        </div>*/}
            {/*      </div>*/}
            {/*      <div className="col-xs-12 col-sm-12 col-md-12 col-lg-4">*/}
            {/*        <div*/}
            {/*          className="kf-blog-grid-item element-anim-1 scroll-animate"*/}
            {/*          data-animate="active"*/}
            {/*        >*/}
            {/*          <div className="image kf-image-hover">*/}
            {/*            <Link href="blog-single">*/}
            {/*              <img src="images/latest_blog2.jpg" alt="image" />*/}
            {/*            </Link>*/}
            {/*          </div>*/}
            {/*          <div className="desc">*/}
            {/*            <h5 className="name">*/}
            {/*              Decisions For Building Flexible Components DevTools Browser*/}
            {/*            </h5>*/}
            {/*            <div className="kf-date">*/}
            {/*              <i className="far fa-calendar-alt" />*/}
            {/*              25 Sep 2021*/}
            {/*            </div>*/}
            {/*            <div className="kf-comm">*/}
            {/*              <i className="far fa-comments" />*/}
            {/*              Comments (7)*/}
            {/*            </div>*/}
            {/*          </div>*/}
            {/*        </div>*/}
            {/*      </div>*/}
            {/*      <div className="col-xs-12 col-sm-12 col-md-12 col-lg-4">*/}
            {/*        <div*/}
            {/*          className="kf-blog-grid-item element-anim-1 scroll-animate"*/}
            {/*          data-animate="active"*/}
            {/*        >*/}
            {/*          <div className="image kf-image-hover">*/}
            {/*            <Link href="blog-single">*/}
            {/*              <img src="images/latest_blog3.jpg" alt="image" />*/}
            {/*            </Link>*/}
            {/*          </div>*/}
            {/*          <div className="desc">*/}
            {/*            <h5 className="name">*/}
            {/*              SWR React Hooks With Next Increm Ental Static Regeneration*/}
            {/*            </h5>*/}
            {/*            <div className="kf-date">*/}
            {/*              <i className="far fa-calendar-alt" />*/}
            {/*              25 Sep 2021*/}
            {/*            </div>*/}
            {/*            <div className="kf-comm">*/}
            {/*              <i className="far fa-comments" />*/}
            {/*              Comments (7)*/}
            {/*            </div>*/}
            {/*          </div>*/}
            {/*        </div>*/}
            {/*      </div>*/}
            {/*    </div>*/}
            {/*    <div className="align-center">*/}
            {/*      <Link*/}
            {/*        href="blog-grid"*/}
            {/*        className="kf-btn element-anim-1 scroll-animate"*/}
            {/*        data-animate="active"*/}
            {/*      >*/}
            {/*        <span>view all</span>*/}
            {/*        <i className="fas fa-chevron-right" />*/}
            {/*      </Link>*/}
            {/*    </div>*/}
            {/*  </div>*/}
            {/*</section>*/}
            {/* Section Insta Carousel */}
            <InstaCarousel/>
            {/* Section Brands */}
            {/*<div className="section kf-brands">
                <div className="container">
                    <div className="kf-brands-items row">

                        <div className="col-xs-12 col-sm-12 col-md-4 col-lg-2">
                            <div
                                className="kf-brands-item element-anim-1 scroll-animate"
                                data-animate="active"
                            >
                                <div className="image">
                                    <img src="images/brand2.png" alt="image"/>
                                </div>
                            </div>
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-4 col-lg-2">
                            <div
                                className="kf-brands-item element-anim-1 scroll-animate"
                                data-animate="active"
                            >
                                <div className="image">
                                    <img src="images/brand3.png" alt="image"/>
                                </div>
                            </div>
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-4 col-lg-2">
                            <div
                                className="kf-brands-item element-anim-1 scroll-animate"
                                data-animate="active"
                            >
                                <div className="image">
                                    <img src="images/brand4.png" alt="image"/>
                                </div>
                            </div>
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-4 col-lg-2">
                            <div
                                className="kf-brands-item element-anim-1 scroll-animate"
                                data-animate="active"
                            >
                                <div className="image">
                                    <img src="images/brand5.png" alt="image"/>
                                </div>
                            </div>
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-4 col-lg-2">
                            <div
                                className="kf-brands-item element-anim-1 scroll-animate"
                                data-animate="active"
                            >
                                <div className="image">
                                    <img src="images/brand6.png" alt="image"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>*/}
        </Layouts>
    );
};
export default Index;
