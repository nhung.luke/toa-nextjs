import Preloader from "@/src/layouts/Preloader";
import "@/styles/globals.css";
import {Fragment} from "react";
import {NextUIProvider} from "@nextui-org/react";
import {ThemeProvider as NextThemesProvider} from "next-themes";
import { ToastContainer } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

const App = ({Component, pageProps}) => {
    return (
        <NextUIProvider
            // theme={{type: 'dark'}}
            // defaultTheme="dark"
        >
            <NextThemesProvider attribute="class" defaultTheme="dark">
                <Preloader/>
                <Component {...pageProps} />
                <ToastContainer
                    position="top-right"
                    autoClose={5000}
                    hideProgressBar={false}
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                    theme="light"
                />
            </NextThemesProvider>
        </NextUIProvider>
    );
};
export default App;
