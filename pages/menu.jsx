import InstaCarousel from "@/src/components/sliders/InstaCarousel";
import Layouts from "@/src/layouts/Layouts";
import Link from "next/link";
import {
    Divider,
    Listbox,
    ListboxItem,
    Table,
    TableBody,
    TableCell,
    TableColumn,
    TableHeader,
    TableRow
} from "@nextui-org/react";
import MenuFood from "@/src/components/Menu/MenuFood";
import React, {useEffect, useRef, useState} from "react";
import beer from '@/images/menu/beer.png';
import champagne from '@/images/menu/champagne.jpeg';
import Vorspeisen from '@/images/bgFood/Vorspeisen.jpeg';
import JapanischeVorspeisen from '@/images/bgFood/JapanischeVorspeisen.jpeg';
import Pho from '@/images/foodImg/Pho.jpg';
import UdonXao from '@/images/foodImg/UdonXao.jpg';
import SushiTOA from '@/images/bgFood/SushiTOA.jpeg';
import SushiTOA1 from '@/images/bgFood/SushiTOA1.jpeg';
import Hauptspeisen1 from '@/images/bgFood/Hauptspeisen1.png';
import Hauptspeisen2 from '@/images/bgFood/Hauptspeisen2.jpeg';
import Vegetarisch1 from '@/images/bgFood/Vegetarisch1.jpeg';
import SpicyTofu from '@/images/foodImg/spicytofu.jpg';
import Donburi1 from '@/images/bgFood/Donburi1.jpeg';
import Donburi2 from '@/images/bgFood/Donburi2.jpeg';
import Ramen1 from '@/images/foodImg/Ramen1.webp';
import Ramen2 from '@/images/foodImg/Ramen2.webp';
import MakiRoll from '@/images/foodImg/makiroll.jpeg';
import SushiExtreme from '@/images/bgFood/SushiExtreme.jpg';
import InsideOutRolls from '@/images/bgFood/InsideOutRolls.jpeg';
import Futomaki1 from '@/images/foodImg/futomaki-01.png';
import Futomaki2 from '@/images/foodImg/Futomaki2.png';
import VegetarianSushi2 from '@/images/foodImg/VEGETARISCHESUSHI2.webp';
import Vegetarisch from '@/images/bgFood/vegetarisch.jpg';
import Nigiri1 from '@/images/bgFood/Nigiri.jpg';
import Sashimi1 from '@/images/bgFood/sashimibg.jpeg';
import Sashimi2 from '@/images/bgFood/Sashimi3.png';
import tempura1 from '@/images/bgFood/tempura1.jpg';
import tempura2 from '@/images/bgFood/tempura2.jpg';
import KombiEssen1 from '@/images/bgFood/KombiEssen1.jpeg';
import combo2 from '@/images/foodImg/combo2.jpg';
import Dessert1 from '@/images/foodImg/Dessert1.jpg';
import Dessert2 from '@/images/bgFood/kemxoi.png';
import RuouNep from '@/images/foodImg/ruou_nep_moi.png';
import Riesling from '@/images/foodImg/Riesling.png';
import Narassa from '@/images/foodImg/NARASSA.png';
import Cocktail from '@/images/bgFood/Cocktail.png';
import Cola from '@/images/foodImg/Cola.png';
import Coffee from '@/images/bgFood/Coffee.png';
import Homemade from '@/images/bgFood/Homemade.png';
import Tea from '@/images/bgFood/tea.png';
import BgMenu from '@/images/bgFood/BgMenu1.jpg';
import {
    appetizerMenu,
    asianNoodleDishes1,
    asianNoodleDishes2,
    cateMenu,
    dessert1,
    dessert2,
    donburi1,
    donburi2,
    futomaki1,
    futomaki2,
    hauptspeisen1,
    hauptspeisen2,
    insideOutRolls,
    japaneseAppetizers,
    maki,
    nigiri,
    ramenNudeln1,
    ramenNudeln2,
    sasimi1,
    sasimi2,
    sushiExtreme,
    sushiMenus1,
    sushiMenus2,
    tempura10stk,
    tempura6stk,
    vegetarischesSushi1,
    vegetarischesSushi2,
    vegetarischTasteOfAsia1,
    vegetarischTasteOfAsia2
} from "@/src/dataMenu";
import FloatButton from "@/src/components/FloatButton";
import MenuDrink from "@/src/components/Menu/MenuDrink";

const Menu = () => {
    const [selectMenuItem, setSelectMenuItem] = useState('')
    const [pauseScrolling, setPauseScrolling] = useState(true)

    const menuPop = useRef();
    useEffect(() => {
        if (pauseScrolling) {
            const targetIds = cateMenu.map(item => item.id)
            const observerOptions = {
                root: null,
                rootMargin: '0px',
                threshold: 0.2,
            };
            const observer = new IntersectionObserver(entries => {
                entries.forEach(entry => {
                    if (entry.isIntersecting) {
                        const targetId = entry.target.id;
                        setSelectMenuItem(targetId);
                    }
                });
            }, observerOptions);
            targetIds.forEach(targetId => {
                const target = document.getElementById(targetId);
                target && observer.observe(target);
            });

            const sectionObserver = new IntersectionObserver(entries => {
                entries.forEach(entry => {
                    if (entry.isIntersecting) {
                        menuPop.current?.classList?.remove("d-none")
                    } else {
                        menuPop.current?.classList?.add("d-none")
                    }
                });
            },);
            const menuList = document.getElementById("menuSection");
            menuList && sectionObserver.observe(menuList)
        }
    }, []);

    const scrollToMenu = (item) => {
        setSelectMenuItem(item)
        const listItem = document.getElementById(item);
        listItem.scrollIntoView({behavior: 'smooth'});
    }

    return (
        <Layouts>

            <div className={"absolute h-full right-[10px] max-xl:hidden "}>
                <div className="sticky top-2/4  z-10 bg-[#433e3e] w-auto rounded-xl">
                    <div ref={menuPop} className="container mx-auto p-4 d-none">
                        <Listbox
                            items={cateMenu.map(item => {
                                return item.id === selectMenuItem ? {
                                    ...item,
                                    selected: true
                                } : item
                            })}
                            color={'primary'}
                            onAction={(item) => scrollToMenu(item)}
                        >
                            {(item) => <ListboxItem key={item.id}
                                                    classNames={{base: item.selected ? 'bg-primary' : ''}}
                            >
                                {item.name}
                            </ListboxItem>}
                        </Listbox>
                    </div>
                </div>
            </div>

            <FloatButton items={cateMenu} onAction={scrollToMenu}/>

            <section className="section kf-started-inner">
                <div
                    className="hidden lg:block kf-parallax-bg js-parallax"
                    style={{backgroundImage: `url(${BgMenu.src})`, opacity: 0.4}}
                />
                <div
                    className="lg:hidden kf-parallax-bg "
                    style={{backgroundImage: `url(${BgMenu.src})`, opacity: 0.4, objectPosition: "bottom"}}
                />
                <div className="container">
                    <h13
                        className="kf-h-title text-anim-1 scroll-animate"
                        data-splitting="chars"
                        data-animate="active"
                    >
                        Speisekarte
                    </h13>
                </div>
            </section>


            <section className={'mt-12 max-lg:hidden'}>
                <div className="container h-48">
                    <div className={'flex'}>
                        <ul className={'list-none flex flex-wrap justify-center mb-6'}>
                            {
                                cateMenu.map(e => {
                                    return (
                                        <li key={e.id}
                                            className={'bg-black rounded-full inline-block m-2.5 py-2 px-2'}>
                                            <a onClick={() => scrollToMenu(e.id)}
                                               className={'py-2 px-11 no-underline text-[#c4c0c0] '}>{e.name}</a>
                                        </li>
                                    )
                                })
                            }
                        </ul>
                    </div>
                    <Divider className="mx-auto px-auto w-6/12 bg-white"/>
                </div>
            </section>
            <div id={"menuSection"}>
                <div>
                    <section className="section kf-menu" id={'appetizers'}>
                        <div className="container">
                            <div className="row">
                                <MenuFood title={'VORSPEISEN'}
                                          data={appetizerMenu}
                                          banner={Vorspeisen}
                                          propsStyle={{backgroundPositionY: '70%'}}
                                />
                                <MenuFood title={'JAPANISCHE VORSPEISEN'} data={japaneseAppetizers}
                                          banner={JapanischeVorspeisen}/>
                            </div>
                        </div>
                    </section>


                    <section className="section kf-menu" id={'noodleDishes'}>
                        <div className="container">
                            <TitleMenuCate title={'ASIATISCHE NUDEL GERICHTE'}/>

                            <div className="row">
                                <MenuFood data={asianNoodleDishes1} banner={Pho}/>

                                <MenuFood data={asianNoodleDishes2} banner={UdonXao}/>
                            </div>
                        </div>
                    </section>

                    <section className="section kf-menu" id={'mainCourses'}>
                        <div className="container">

                            <TitleMenuCate title={'HAUPTSPEISEN & TASTE OF ASIA DELUXE'}/>

                            <div className="row">
                                <MenuFood data={hauptspeisen1} banner={Hauptspeisen2}/>
                                <MenuFood data={hauptspeisen2} banner={Hauptspeisen1}/>
                            </div>
                        </div>
                    </section>

                    <section className="section kf-menu" id={'vegetarianDishes'}>
                        <div className="container">
                            <TitleMenuCate title={'VEGETARISCH STYLE TASTE OF ASIA'}/>
                            <div className="row">
                                <MenuFood data={vegetarischTasteOfAsia1} banner={Vegetarisch1}/>

                                <MenuFood data={vegetarischTasteOfAsia2} banner={SpicyTofu}/>
                            </div>
                        </div>
                    </section>

                    <section className="section kf-menu" id={'donburi'}>
                        <div className="container">
                            <TitleMenuCate title={'DONBURI'}/>
                            <div className="row">
                                <MenuFood data={donburi1} banner={Donburi1}/>

                                <MenuFood data={donburi2} banner={Donburi2}/>
                            </div>
                        </div>
                    </section>

                    <section className="section kf-menu" id={'ramenNoodles'}>
                        <div className="container">
                            <div className="row">
                                <MenuFood title={'RAMEN NUDELN'} data={ramenNudeln1} banner={Ramen1}/>

                                <MenuFood title={'RAMEN NUDELN'} data={ramenNudeln2} banner={Ramen2}/>
                            </div>
                        </div>
                    </section>
                    <div id={'sushiTOA'}>
                        <section className="section kf-menu">
                            <div className="container">
                                <TitleMenuCate title={'SUSHI TASTE OF ASIA'}/>
                                <div
                                    style={{
                                        backgroundImage: `url(${SushiTOA1.src})`,
                                        visibility: 'visible',
                                        backgroundRepeat: 'no-repeat',
                                        backgroundSize: 'cover',
                                        aspectRatio: '1280 / 738',
                                    }}
                                    className={'lg:hidden'}
                                />


                                <div className="row">
                                    <MenuFood data={nigiri} title={'NIGIRI (2Stk)'} banner={Nigiri1}
                                              propsStyle={{backgroundPositionY: '20%'}}/>
                                    <MenuFood data={maki} title={'MAKI (8Stk)'} banner={MakiRoll}
                                              propsStyle={{backgroundPositionY: '60%'}}/>
                                </div>
                            </div>
                        </section>

                        <section className="section kf-menu">
                            <div className="container">
                                <div className="row">
                                    <MenuFood data={insideOutRolls} title={'INSIDE OUT ROLLS (8 Stk)'}
                                              banner={InsideOutRolls}/>
                                    <MenuFood data={sushiExtreme} title={'SUSHI EXTREME(8Stk)'} banner={SushiExtreme}/>
                                </div>
                            </div>
                        </section>

                        <section className="section kf-menu" id={'sasimi'}>
                            <div className="container">
                                <TitleMenuCate title={'Sashimi'}/>

                                <div className="row">
                                    <MenuFood data={sasimi1} banner={Sashimi1}/>
                                    <MenuFood data={sasimi2} banner={Sashimi2}
                                              propsStyle={{backgroundPositionY: '70%'}}/>
                                </div>
                            </div>
                        </section>

                        <section className="section kf-menu">
                            <div className="container">
                                <TitleMenuCate title={'VEGETARISCHE SUSHI'}/>
                                <div className="row">
                                    <MenuFood data={vegetarischesSushi1} banner={Vegetarisch}/>
                                    <MenuFood data={vegetarischesSushi2} banner={VegetarianSushi2}/>
                                </div>
                            </div>
                        </section>
                    </div>

                    <section className="section kf-menu">
                        <div className="container">
                            <TitleMenuCate title={'FUTO MAKI (8Stk)'}/>

                            <div className="row">
                                <MenuFood data={futomaki1} banner={Futomaki1}/>
                                <MenuFood data={futomaki2} banner={Futomaki2}/>
                            </div>
                        </div>
                    </section>


                    <section className="section kf-menu" id={'tempura'}>
                        <div className="container">
                            <TitleMenuCate title={'TEMPURA'}/>

                            <div className="row">
                                <MenuFood data={tempura10stk} title={'BIG ROLLS (10 Stk) MAKI. MINI'}
                                          banner={tempura1}/>
                                <MenuFood data={tempura6stk} title={'BIG ROLLS (6Stk)'} banner={tempura2}/>
                            </div>
                        </div>
                    </section>

                    <section className="section kf-menu" id={'combo'}>
                        <div className="container">
                            <TitleMenuCate title={'SUSHI MENÜS'}/>

                            <div className="row">
                                <div className="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                                    <div
                                        className="kf-menu-image-2 left element-anim-1 scroll-animate"
                                        data-animate="active"
                                        style={{backgroundImage: `url(${KombiEssen1.src})`}}
                                    />
                                    <div className="kf-menu-items-2">

                                        {sushiMenus1.map((item, index) => {
                                            const {name, sup, price, includes} = item;
                                            return (
                                                <div key={index}
                                                     className="kf-menu-item-2 element-anim-1 scroll-animate"
                                                     data-animate="active">


                                                    <div className="detail">
                                                        <h4 className="name">{name}
                                                            <sup className={'cate'}>{sup}</sup>
                                                        </h4>
                                                        {includes.map((dish, index) => {
                                                                return (
                                                                    <div key={index}
                                                                         className={"desc flex justify-between "}>
                                                                        <p>{dish.title}</p>
                                                                        <span>{dish.cate}</span>
                                                                    </div>
                                                                )
                                                            }
                                                        )}
                                                        {price && <div className="price">
                                                            <span>{price}€</span>
                                                        </div>}
                                                    </div>
                                                </div>
                                            );
                                        })}
                                    </div>

                                </div>
                                <div className="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                                    <div
                                        className="kf-menu-image-2 right element-anim-1 scroll-animate"
                                        data-animate="active"
                                        style={{backgroundImage: `url(${combo2.src})`}}
                                    />
                                    <div className="kf-menu-items-2">
                                        {sushiMenus2.map((item, index) => {
                                            const {name, desc, sup, price, includes} = item;
                                            return (
                                                <div key={index}
                                                     className="kf-menu-item-2 element-anim-1 scroll-animate"
                                                     data-animate="active">


                                                    <div className="detail">
                                                        <h4 className="name">{name}
                                                            <sup className={'cate'}>{sup}</sup>
                                                        </h4>
                                                        {desc && <div className="desc whitespace-normal">{desc}</div>}

                                                        {includes?.map((dish, index) => {
                                                                return (
                                                                    <div key={index}
                                                                         className={"desc flex justify-between "}>
                                                                        <p>{dish.title}</p>
                                                                        <span>{dish.cate}</span>
                                                                    </div>
                                                                )
                                                            }
                                                        )}
                                                        {price && <div className="price">
                                                            <span>{price}€</span>
                                                        </div>}
                                                    </div>
                                                </div>
                                            );
                                        })}

                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section className="section kf-menu" id={'dessert'}>
                        <div className="container">
                            <TitleMenuCate title={'DESERT'}/>

                            <div className="row">
                                <MenuFood data={dessert1} banner={Dessert2}/>
                                <MenuFood data={dessert2} banner={Dessert1}/>
                            </div>
                        </div>
                    </section>

                    <MenuDrink
                        id={'drinks'}
                        classWapper={'lg:mt-48'}
                        title={'HOMEMADE'}
                        image={Homemade}
                        classImg={'lg:max-h-[1000px] lg:max-w-[600px]'}
                        alt={'HOMEMADE'}
                        children={<div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-8 justify-center">
                            <Table removeWrapper className={"bg-transparent mt-2 "}>
                                <TableHeader>
                                    <TableColumn width={"90%"} className={"bg-transparent !text-2xl"}/>
                                    <TableColumn width={"10%"} className={"bg-transparent !text-2xl"}/>
                                </TableHeader>
                                <TableBody>
                                    <TableRow key="130">
                                        <TableCell style={{fontSize: "20px"}}>
                                            <h3 className={'text-xl'}>130. Limetten-Limonade.</h3>
                                        </TableCell>
                                        <TableCell style={{fontSize: "20px"}} className={''}>5,5€</TableCell>
                                    </TableRow>
                                    <TableRow key="131">
                                        <TableCell style={{fontSize: "20px"}}>
                                            <h3 className={'text-xl'}>131. Maracuja Crush.</h3>
                                        </TableCell>
                                        <TableCell style={{fontSize: "20px"}} className={''}>5,5€</TableCell>
                                    </TableRow>
                                    <TableRow key="132">
                                        <TableCell style={{fontSize: "20px"}}>
                                            <h3 className={'text-xl'}>132. Vinpearl Eistee.</h3>
                                        </TableCell>
                                        <TableCell style={{fontSize: "20px"}} className={''}>5,5€</TableCell>
                                    </TableRow>
                                    <TableRow key="133">
                                        <TableCell style={{fontSize: "20px"}}>
                                            <h3 className={'text-xl'}>133. Pfirsich Oolong-Tee.</h3>
                                        </TableCell>
                                        <TableCell style={{fontSize: "20px"}} className={''}>5,5€</TableCell>
                                    </TableRow>
                                    <TableRow key="134">
                                        <TableCell style={{fontSize: "20px"}}>
                                            <h3 className={'text-xl'}>134. Mango Lassi.</h3>
                                        </TableCell>
                                        <TableCell style={{fontSize: "20px"}} className={''}>5,5€</TableCell>
                                    </TableRow>
                                    <TableRow key="135">
                                        <TableCell style={{fontSize: "20px"}}>
                                            <h3 className={'text-xl'}>135. Cucumber Crush.</h3>
                                        </TableCell>
                                        <TableCell style={{fontSize: "20px"}} className={''}>5,5€</TableCell>
                                    </TableRow>
                                    <TableRow key="136">
                                        <TableCell style={{fontSize: "20px"}}>
                                            <h3 className={'text-xl'}>136. Kumquat Crush.</h3>
                                        </TableCell>
                                        <TableCell style={{fontSize: "20px"}} className={''}>5,5€</TableCell>
                                    </TableRow>
                                    <TableRow key="137">
                                        <TableCell style={{fontSize: "20px"}}>
                                            <h3 className={'text-xl'}>137. Exotic Passion</h3>
                                        </TableCell>
                                        <TableCell style={{fontSize: "20px"}} className={''}>5,5€</TableCell>
                                    </TableRow>
                                    <TableRow key="138">
                                        <TableCell style={{fontSize: "20px"}}>
                                            <h3 className={'text-xl'}>138. Frisch Gepresster Orangensaft.</h3>
                                        </TableCell>
                                        <TableCell style={{fontSize: "20px"}} className={''}>5,5€</TableCell>
                                    </TableRow>


                                </TableBody>
                            </Table>
                        </div>

                        }
                    />


                    <MenuDrink
                        id={'drinks'}
                        classWapper={'lg:mt-48'}
                        title={'SOFT DRINKS'}
                        image={Cola}
                        classImg={'lg:max-h-[1000px] lg:max-w-[600px]'}
                        alt={'SOFT DRINKS'}
                        children={<div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-8 justify-center">
                            <div className={'flex flex-col justify-center h-full'}>
                                <Table removeWrapper className={"bg-transparent mt-2"}>

                                    <TableHeader className={"bg-transparent "}>
                                        <TableColumn width={"80%"} className={"bg-transparent !text-2xl"}>

                                        </TableColumn>
                                        <TableColumn width={"10%"} className={"bg-transparent !text-2xl "}>
                                            <h3>0,3l</h3>
                                        </TableColumn>
                                        <TableColumn width={"10%"} className={"bg-transparent !text-2xl text-center "}>
                                            <h3>0,5l</h3>
                                        </TableColumn>
                                    </TableHeader>

                                    <TableBody>
                                        <TableRow key="139">
                                            <TableCell style={{fontSize: "20px"}}>
                                                <h3 className={'text-xl'}>139. Cola
                                                    <span className={'text-[#eb8b2e] text-base ml-1'}>
                                                            (Cola Zero, Fanta, Sprite, Paulaner Spezi)
                                                        </span>
                                                </h3>
                                            </TableCell>
                                            <TableCell style={{fontSize: "20px"}} className={''}>3,2€</TableCell>
                                            <TableCell style={{fontSize: "20px"}} className={''}>4,2€</TableCell>
                                        </TableRow>

                                    </TableBody>
                                </Table>

                                <Table removeWrapper className={"bg-transparent"}>

                                    <TableHeader className={"bg-transparent "}>
                                        <TableColumn width={"80%"} className={"bg-transparent !text-2xl"}>

                                        </TableColumn>
                                        <TableColumn width={"10%"} className={"bg-transparent !text-2xl  "}>
                                            <h3>0,2l</h3>
                                        </TableColumn>
                                        <TableColumn width={"10%"} className={"bg-transparent !text-2xl  "}>
                                            <h3>0,4l</h3>
                                        </TableColumn>
                                    </TableHeader>

                                    <TableBody>

                                        <TableRow key="140">
                                            <TableCell style={{fontSize: "20px"}}>
                                                <h3 className={'text-xl'}>140. Ginger Ale
                                                    <span className={'text-[#eb8b2e] text-base ml-1'}>
                                                            (Bitter Lemon, Tonic)
                                                        </span>
                                                </h3>
                                            </TableCell>
                                            <TableCell style={{fontSize: "20px"}} className={''}>3,2€</TableCell>
                                            <TableCell style={{fontSize: "20px"}} className={''}>4,5€</TableCell>
                                        </TableRow>
                                        <TableRow key="141">
                                            <TableCell style={{fontSize: "20px"}}>
                                                <h3 className={'text-xl'}>141. Säfte.
                                                    <span
                                                        className={'text-[#eb8b2e] text-base ml-1'}>
                                                            (Apfel, Banane, Kirsch, Kiba, Mango,Lychee, Guave)
                                                        </span>
                                                </h3>
                                            </TableCell>
                                            <TableCell style={{fontSize: "20px"}} className={''}>3,2€</TableCell>
                                            <TableCell style={{fontSize: "20px"}} className={''}>5,5€</TableCell>
                                        </TableRow>
                                        <TableRow key="142">
                                            <TableCell style={{fontSize: "20px"}}>
                                                <h3 className={'text-xl'}>142. Saftschorlen
                                                    <span className={'text-[#eb8b2e] text-base ml-1'}>
                                                            (Apfel, Banane, Kirsch, Mango,Lychee, Guave)
                                                        </span>
                                                </h3>
                                            </TableCell>
                                            <TableCell style={{fontSize: "20px"}} className={''}>3,2€</TableCell>
                                            <TableCell style={{fontSize: "20px"}} className={''}>5,5€</TableCell>
                                        </TableRow>
                                    </TableBody>
                                </Table>

                                <Table removeWrapper className={"bg-transparent"}>

                                    <TableHeader className={"bg-transparent "}>
                                        <TableColumn width={"80%"} className={"bg-transparent !text-2xl"}>

                                        </TableColumn>
                                        <TableColumn width={"10%"} className={"bg-transparent !text-2xl "}>
                                            <h3>0,2l</h3>
                                        </TableColumn>
                                        <TableColumn width={"10%"} className={"bg-transparent !text-2xl "}>
                                            <h3>0,4l</h3>
                                        </TableColumn>
                                    </TableHeader>

                                    <TableBody>


                                        <TableRow key="143">
                                            <TableCell style={{fontSize: "20px"}}>
                                                <h3 className={'text-xl'}>143. Stilles Wasser
                                                    <span className={'text-[#eb8b2e] text-base ml-1'}>
                                                        (Spritz/Sprudel)
                                                    </span>
                                                </h3>
                                            </TableCell>
                                            <TableCell style={{fontSize: "20px"}} className={''}>2,9€</TableCell>
                                            <TableCell style={{fontSize: "20px"}} className={''}>3,9€</TableCell>
                                        </TableRow>

                                    </TableBody>
                                </Table>
                                <Table removeWrapper className={"bg-transparent"}>

                                    <TableHeader className={"bg-transparent "}>
                                        <TableColumn width={"90%"} className={"bg-transparent !text-2xl"}>

                                        </TableColumn>
                                        <TableColumn width={"10%"} className={"bg-transparent !text-2xl "}>
                                            <h3>0,75l</h3>
                                        </TableColumn>
                                    </TableHeader>

                                    <TableBody>

                                        <TableRow key="144">
                                            <TableCell style={{fontSize: "20px"}}>
                                                <h3 className={'text-xl'}>144. Flasche Wasser
                                                    <span className={'text-[#eb8b2e] text-base ml-1'}>
                                                        (Spritz/Sprudel)
                                                    </span>
                                                </h3>
                                            </TableCell>
                                            <TableCell style={{fontSize: "20px"}} className={''}>6,5€</TableCell>
                                        </TableRow>
                                    </TableBody>
                                </Table>


                            </div>
                        </div>

                        }
                    />

                    <MenuDrink
                        id={'drinks'}
                        classImg={'lg:mt-48'}
                        title={'Tee'}
                        image={Tea}
                        classImg={'lg:max-h-[1000px] lg:max-w-[600px]'}
                        alt={'TEES'}
                        children={<div
                            className="relative col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-8 justify-center">
                            <div className={'flex flex-col justify-center h-full'}>
                                <Table removeWrapper className={"bg-transparent mt-2"}>

                                    <TableHeader className={"bg-transparent "}>
                                        <TableColumn width={"90%"} className={"bg-transparent !text-2xl"}>

                                        </TableColumn>
                                        <TableColumn width={"10%"} className={"bg-transparent !text-2xl "}>
                                        </TableColumn>
                                    </TableHeader>

                                    <TableBody>
                                        <TableRow key="145">
                                            <TableCell style={{fontSize: "20px"}}>
                                                <h3 className={'text-xl'}>145. Heiße Zitrone.
                                                </h3>
                                            </TableCell>
                                            <TableCell style={{fontSize: "20px"}} className={''}>4,2€</TableCell>
                                        </TableRow>
                                        <TableRow key="146">
                                            <TableCell style={{fontSize: "20px"}}>
                                                <h3 className={'text-xl'}>
                                                    146. Frischer Minztee.
                                                </h3>
                                            </TableCell>
                                            <TableCell style={{fontSize: "20px"}} className={''}>4,2€</TableCell>
                                        </TableRow>
                                        <TableRow key="147">
                                            <TableCell style={{fontSize: "20px"}}>
                                                <h3 className={'text-xl'}>
                                                    147. Grüner Tee.
                                                </h3>
                                            </TableCell>
                                            <TableCell style={{fontSize: "20px"}} className={''}>4,2€</TableCell>
                                        </TableRow>
                                        <TableRow key="148">
                                            <TableCell style={{fontSize: "20px"}}>
                                                <h3 className={'text-xl'}>
                                                    148. Jasmintee.
                                                </h3>
                                            </TableCell>
                                            <TableCell style={{fontSize: "20px"}} className={''}>4,2€</TableCell>
                                        </TableRow>
                                        <TableRow key="149">
                                            <TableCell style={{fontSize: "20px"}}>
                                                <h3 className={'text-xl'}>
                                                    149. Frischer Ingwertee.
                                                </h3>
                                            </TableCell>
                                            <TableCell style={{fontSize: "20px"}} className={''}>4,2€</TableCell>
                                        </TableRow>
                                        <TableRow key="150">
                                            <TableCell style={{fontSize: "20px"}}>
                                                <h3 className={'text-xl'}>
                                                    150. Tra Sa Gung'Zitronengras-Tee.
                                                    <span className={'text-[#eb8b2e] text-base ml-1'}>
                                                            Ingwer, Limetten, Zitronengras, Honig
                                                        </span>
                                                </h3>
                                            </TableCell>
                                            <TableCell style={{fontSize: "20px"}} className={''}>4,2€</TableCell>
                                        </TableRow>

                                    </TableBody>
                                </Table>


                            </div>
                        </div>

                        }
                    />

                    <MenuDrink
                        title={'KAFFEES'}
                        image={Coffee}
                        classImg={'lg:max-h-[1000px] lg:max-w-[600px]'}

                        alt={'KAFFEES'}
                        children={<div
                            className="relative col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-8 justify-center">
                            <Table removeWrapper className={"bg-transparent mt-2"}>
                                <TableHeader>
                                    <TableColumn width={"90%"} className={"bg-transparent !text-2xl"}/>
                                    <TableColumn width={"10%"} className={"bg-transparent !text-2xl"}/>
                                </TableHeader>
                                <TableBody>
                                    <TableRow key="151">
                                        <TableCell style={{fontSize: "20px"}}>
                                            <h3 className={'text-xl'}>151. CAFÉ Trung Nguyen (Schwarz)
                                                <span className={'text-[#eb8b2e] text-base ml-1'}>
                                                (Aromatischer Filterkaffee mit einer Schokonote)
                                            </span>
                                            </h3>
                                        </TableCell>
                                        <TableCell style={{fontSize: "20px"}} className={''}>4,5€</TableCell>
                                    </TableRow>
                                    <TableRow key="152">
                                        <TableCell style={{fontSize: "20px"}}>
                                            <h3 className={'text-xl'}>152. CAFÉ Sua
                                                <span className={'text-[#eb8b2e] text-base ml-1'}>(Filterkaffee mit süßer Kondensmilch)</span>
                                            </h3>
                                        </TableCell>
                                        <TableCell style={{fontSize: "20px"}} className={''}>5,5€</TableCell>
                                    </TableRow>
                                    <TableRow key="153">
                                        <TableCell style={{fontSize: "20px"}}>
                                            <h3 className={'text-xl'}>153. CAFÉ Sua Da
                                                <span className={'text-[#eb8b2e] text-base ml-1'}>(Filterkaffee mit süßer Kondensmilch und Eiswürfeln)</span>
                                            </h3>
                                        </TableCell>
                                        <TableCell style={{fontSize: "20px"}} className={''}>5,5€</TableCell>
                                    </TableRow>
                                    <TableRow key="154">
                                        <TableCell style={{fontSize: "20px"}}>
                                            <h3 className={'text-xl'}>154. CAFÉ Sua Dual
                                                <span className={'text-[#eb8b2e] text-base ml-1'}>(COCOS-Coffee Filterkaffee mit eiskalter Kokosmilch)</span>
                                            </h3>
                                        </TableCell>
                                        <TableCell style={{fontSize: "20px"}} className={''}>5,5€</TableCell>
                                    </TableRow>

                                </TableBody>
                            </Table>

                        </div>

                        }
                    />

                    <MenuDrink
                        title={'BIER'}
                        image={beer}
                        classImg={'lg:max-h-[1000px] lg:max-w-[600px]'}

                        alt={'BIER'}
                        children={<div
                            className="relative col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-8 justify-center">
                            <div className={'flex flex-col justify-center h-full'}>

                                <Table removeWrapper className={"bg-transparent"}>
                                    <TableHeader className={"bg-transparent "}>
                                        <TableColumn width={"80%"}
                                                     className={"bg-transparent !text-2xl"}></TableColumn>
                                        <TableColumn width={"10%"} className={"bg-transparent !text-2xl"}>
                                            <h3>0,3l</h3></TableColumn>
                                        <TableColumn width={"10%"} className={"bg-transparent !text-2xl"}>
                                            <h3>0,5l</h3></TableColumn>
                                    </TableHeader>
                                    <TableBody>
                                        <TableRow key="155">
                                            <TableCell style={{fontSize: "20px"}}>
                                                <h3 className={'text-xl'}>155. Püls-bräu hell <span
                                                    className={'text-[#eb8b2e] text-base'}>(alc. 4.6%)</span> weismainer
                                                    brauerei</h3>
                                            </TableCell>
                                            <TableCell style={{fontSize: "20px"}} className={''}>2,9€</TableCell>
                                            <TableCell style={{fontSize: "20px"}} className={''}>3,9€</TableCell>
                                        </TableRow>

                                        <TableRow key="156">
                                            <TableCell style={{fontSize: "20px"}}>
                                                <h3 className={'text-xl'}>156. Püls-bräu weißbier<span
                                                    className={'text-[#eb8b2e] text-base'}>(alc. 5.2%)</span> weismainer
                                                    brauerei</h3>
                                            </TableCell>
                                            <TableCell style={{fontSize: "20px"}} className={''}>2,9€</TableCell>
                                            <TableCell style={{fontSize: "20px"}} className={''}>3,9€</TableCell>
                                        </TableRow>
                                        <TableRow key="157">
                                            <TableCell style={{fontSize: "20px"}}>
                                                <h3 className={'text-xl'}>157. Püls-bräu flechterla zwick’ldunkles
                                                    unfiltertes bier
                                                    <span
                                                        className={'text-[#eb8b2e] text-base'}>(alc.5,%)</span> weismainer
                                                    brauerei</h3>
                                            </TableCell>
                                            <TableCell style={{fontSize: "20px"}} className={''}>2,9€</TableCell>
                                            <TableCell style={{fontSize: "20px"}} className={''}>3,9€</TableCell>
                                        </TableRow>
                                        <TableRow key="158">
                                            <TableCell style={{fontSize: "20px"}}>
                                                <h3 className={'text-xl'}>158. Vierzehnheiliger bier nothelfer
                                                    lager <span
                                                        className={'text-[#eb8b2e] text-base'}>(alc. 4.7%)</span> brauerei
                                                    trunk</h3>
                                            </TableCell>
                                            <TableCell style={{fontSize: "20px"}}></TableCell>
                                            <TableCell style={{fontSize: "20px"}}>3,9€</TableCell>
                                        </TableRow>
                                        <TableRow key="159">
                                            <TableCell style={{fontSize: "20px"}}>
                                                <h3 className={'text-xl'}>159. Radler <span
                                                    className={'text-[#eb8b2e] text-base'}>(Bier mit Zitronenlimo)</span>
                                                </h3>
                                            </TableCell>
                                            <TableCell style={{fontSize: "20px"}}>2,9€</TableCell>
                                            <TableCell style={{fontSize: "20px"}}>3,9€</TableCell>
                                        </TableRow>
                                        <TableRow key="160">
                                            <TableCell style={{fontSize: "20px"}}>
                                                <h3 className={'text-xl'}>160. Diesel <span
                                                    className={'text-[#eb8b2e] text-base'}>(Bier mit Cola)</span>
                                                </h3>
                                            </TableCell>
                                            <TableCell style={{fontSize: "20px"}}>2,9€</TableCell>
                                            <TableCell style={{fontSize: "20px"}}>3,9€</TableCell>
                                        </TableRow>
                                    </TableBody>
                                </Table>

                                <Table removeWrapper className={"bg-transparent"}>
                                    <TableHeader className={"bg-transparent "}>
                                        <TableColumn width={"90%"} className={"bg-transparent "}></TableColumn>
                                        <TableColumn width={"10%"} className={"bg-transparent text-2xl"}>
                                            <h3>0,33l</h3></TableColumn>
                                    </TableHeader>
                                    <TableBody>
                                        <TableRow key="161">
                                            <TableCell style={{fontSize: "20px"}}>
                                                <h3 className={'text-xl'}>161. Vierzehnheiliger bier nothelfer
                                                    pils <span
                                                        className={'text-[#eb8b2e] text-base'}>(alc. 5.1%)</span> brauerei
                                                    trunk</h3>
                                            </TableCell>
                                            <TableCell style={{fontSize: "20px"}} className={''}>3,9€</TableCell>
                                        </TableRow>
                                        <TableRow key="162">
                                            <TableCell style={{fontSize: "20px"}}>
                                                <h3 className={'text-xl'}>162. Sapporo premium beer lager <span
                                                    className={'text-[#eb8b2e] text-base'}>(alc. 5%)</span> sapporo
                                                    breweries</h3>
                                            </TableCell>
                                            <TableCell style={{fontSize: "20px"}}>4,5€</TableCell>
                                        </TableRow>
                                        <TableRow key="163">
                                            <TableCell style={{fontSize: "20px"}}>
                                                <h3 className={'text-xl'}>163. Neumarkter lammsbräu alkoholfrei
                                                    bio <span
                                                        className={'text-[#eb8b2e] text-base'}>(alc. 0%)</span> Neumarkter
                                                    lammsbräu brauerei</h3>
                                            </TableCell>
                                            <TableCell style={{fontSize: "20px"}}>3,0€</TableCell>
                                        </TableRow>
                                        <TableRow key="164">
                                            <TableCell style={{fontSize: "20px"}}>
                                                <h3 className={'text-xl'}>164. Sapporo premium beer lager
                                                    alcohol <span
                                                        className={'text-[#eb8b2e] text-base'}>free (alc. 0%)</span> Sapporo
                                                    breweries</h3>
                                            </TableCell>
                                            <TableCell style={{fontSize: "20px"}}>4,5€</TableCell>
                                        </TableRow>
                                        <TableRow key="165">
                                            <TableCell style={{fontSize: "20px"}}>
                                                <h3 className={'text-xl'}>165. Bananen-Kirschweizen.</h3>
                                            </TableCell>
                                            <TableCell style={{fontSize: "20px"}}>4,5€</TableCell>
                                        </TableRow>
                                        <TableRow key="156">
                                            <TableCell style={{fontSize: "20px"}}>
                                                <h3 className={'text-xl'}>166. Vietnamesisches Saigon Bier.</h3>
                                            </TableCell>
                                            <TableCell style={{fontSize: "20px"}}>4,9€</TableCell>
                                        </TableRow>
                                    </TableBody>
                                </Table>
                            </div>
                        </div>
                        }
                    />

                    <MenuDrink
                        id={'alkohol'}
                        title={'APERATIFS, COCKTAILS & CO'}
                        image={Cocktail}
                        classImg={'lg:max-h-[1000px] lg:max-w-[600px]'}
                        alt={'APERATIFS, COCKTAILS & CO'}
                        children={<div
                            className="relative col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-8 justify-center">


                            <Table removeWrapper className={"bg-transparent mt-2"}>
                                <TableHeader>
                                    <TableColumn width={"90%"} className={"bg-transparent !text-2xl"}/>
                                    <TableColumn width={"10%"} className={"bg-transparent !text-2xl"}/>
                                </TableHeader>
                                <TableBody>
                                    <TableRow key="167">
                                        <TableCell style={{fontSize: "20px"}}>
                                            <h3 className={'text-xl'}>167. Fruity Gin.
                                                <span className={'text-[#eb8b2e] text-base ml-1'}>
                                                (Gin, Limette, Apfelsaft,Zitronengras, Himbeere, Mandel- und Mangosirup)
                                            </span>
                                            </h3>
                                        </TableCell>
                                        <TableCell style={{fontSize: "20px"}} className={''}>7,9€</TableCell>
                                    </TableRow>
                                    <TableRow key="168">
                                        <TableCell style={{fontSize: "20px"}}>
                                            <h3 className={'text-xl'}>168. Mojito.
                                                <span className={'text-[#eb8b2e] text-base ml-1'}>(Bacardi, Limette, Rohrzucker, Soda, Minze)</span>
                                            </h3>
                                        </TableCell>
                                        <TableCell style={{fontSize: "20px"}} className={''}>7,9€</TableCell>
                                    </TableRow>
                                    <TableRow key="169">
                                        <TableCell style={{fontSize: "20px"}}>
                                            <h3 className={'text-xl'}>169. Pink Mojito.
                                                <span className={'text-[#eb8b2e] text-base ml-1'}>(Bacardi, Limette, Rohrzucker, Soda, verfeinert mit Himbeeren)</span>
                                            </h3>
                                        </TableCell>
                                        <TableCell style={{fontSize: "20px"}} className={''}>7,9€</TableCell>
                                    </TableRow>
                                    <TableRow key="170">
                                        <TableCell style={{fontSize: "20px"}}>
                                            <h3 className={'text-xl'}>170. Gin Gin Mule.
                                                <span className={'text-[#eb8b2e] text-base ml-1'}>(Gin, Minze, Limette, Spicy Ginger)</span>
                                            </h3>
                                        </TableCell>
                                        <TableCell style={{fontSize: "20px"}} className={''}>7,9€</TableCell>
                                    </TableRow>
                                    <TableRow key="171">
                                        <TableCell style={{fontSize: "20px"}}>
                                            <h3 className={'text-xl'}>171. Moscow Mule.
                                                <span className={'text-[#eb8b2e] text-base ml-1'}>(Wodka, Limette, Spicy Ginger, Gurke)</span>
                                            </h3>
                                        </TableCell>
                                        <TableCell style={{fontSize: "20px"}} className={''}>7,9€</TableCell>
                                    </TableRow>
                                    <TableRow key="172">
                                        <TableCell style={{fontSize: "20px"}}>
                                            <h3 className={'text-xl'}>172. Aperol Spritz
                                                <span className={'text-[#eb8b2e] text-base ml-1'}>(Aperol, Prosecco, Soda, Orange)</span>
                                            </h3>
                                        </TableCell>
                                        <TableCell style={{fontSize: "20px"}} className={''}>7,9€</TableCell>
                                    </TableRow>
                                    <TableRow key="173">
                                        <TableCell style={{fontSize: "20px"}}>
                                            <h3 className={'text-xl'}>173. Hugo.
                                                <span className={'text-[#eb8b2e] text-base ml-1'}>(Prosecco, Limette, Minze, Holundersirup, Soda)</span>
                                            </h3>
                                        </TableCell>
                                        <TableCell style={{fontSize: "20px"}} className={''}>7,9€</TableCell>
                                    </TableRow>
                                    <TableRow key="174">
                                        <TableCell style={{fontSize: "20px"}}>
                                            <h3 className={'text-xl'}>174. Wild Berry Lillet.
                                                <span className={'text-[#eb8b2e] text-base ml-1'}>(Lillet Blanc, Russian Wild Berry)</span>
                                            </h3>
                                        </TableCell>
                                        <TableCell style={{fontSize: "20px"}} className={''}>7,9€</TableCell>
                                    </TableRow>

                                </TableBody>
                            </Table>

                        </div>}
                    />


                    <MenuDrink
                        title={'SEKT & SPIRITUOSEN'}
                        image={RuouNep}
                        alt={'RuouNep'}
                        classImg={'lg:max-h-[1000px] lg:max-w-[600px]'}
                        children={<div
                            className="relative col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-8 justify-center">

                            <div className={'flex flex-col justify-center h-full'}>

                                <Table removeWrapper className={"bg-transparent"}>
                                    <TableHeader className={"bg-transparent "}>
                                        <TableColumn width={"80%"}
                                                     className={"bg-transparent !text-2xl"}></TableColumn>
                                        <TableColumn width={"10%"} className={"bg-transparent !text-2xl"}>
                                            <h3>0,1l</h3></TableColumn>
                                        <TableColumn width={"10%"} className={"bg-transparent !text-2xl"}>
                                            <h3>0,75l</h3></TableColumn>
                                    </TableHeader>
                                    <TableBody>
                                        <TableRow key="175">
                                            <TableCell style={{fontSize: "20px"}}>
                                                <h3 className={'text-xl'}>175. Prosecco.
                                                    <span className={'text-[#eb8b2e] text-base ml-1'}>
                                                            (Scavi & Ray Spumante, elegant und geschmeidig.)
                                                        </span>
                                                </h3>
                                            </TableCell>
                                            <TableCell style={{fontSize: "20px"}}>6,7€</TableCell>
                                            <TableCell style={{fontSize: "20px"}}>29,0€</TableCell>
                                        </TableRow>
                                        <TableRow key="176">
                                            <TableCell style={{fontSize: "20px"}}>
                                                <h3 className={'text-xl'}>176. Sake
                                                    <span className={'text-[#eb8b2e] text-base ml-1'}>
                                                          (Japanischer Reiswein)
                                                        </span>
                                                </h3>
                                            </TableCell>
                                            <TableCell style={{fontSize: "20px"}}>5,9€</TableCell>
                                            <TableCell style={{fontSize: "20px"}}></TableCell>
                                        </TableRow>
                                    </TableBody>
                                </Table>

                                <Table removeWrapper className={"bg-transparent"}>
                                    <TableHeader className={"bg-transparent "}>
                                        <TableColumn width={"80%"}
                                                     className={"bg-transparent !text-2xl"}></TableColumn>
                                        <TableColumn width={"10%"} className={"bg-transparent !text-2xl"}>
                                            <h3>0,1cl</h3></TableColumn>
                                        <TableColumn width={"10%"} className={"bg-transparent !text-2xl"}>
                                            <h3>0,2cl</h3></TableColumn>
                                    </TableHeader>
                                    <TableBody>
                                        <TableRow key="177">
                                            <TableCell style={{fontSize: "20px"}}>
                                                <h3 className={'text-xl'}>177. Nep Moi.
                                                    <span className={'text-[#eb8b2e] text-base ml-1'}>
                                                            (Vietnamesischer Reiswodka)
                                                        </span>
                                                </h3>
                                            </TableCell>
                                            <TableCell style={{fontSize: "20px"}}>3,5€</TableCell>
                                            <TableCell style={{fontSize: "20px"}}>5,5€</TableCell>
                                        </TableRow>
                                        <TableRow key="178">
                                            <TableCell style={{fontSize: "20px"}}>
                                                <h3 className={'text-xl'}>178. Absolut Vodka.
                                                </h3>
                                            </TableCell>
                                            <TableCell style={{fontSize: "20px"}}>3,5€</TableCell>
                                            <TableCell style={{fontSize: "20px"}}>3,9€</TableCell>
                                        </TableRow>
                                        <TableRow key="179">
                                            <TableCell style={{fontSize: "20px"}}>
                                                <h3 className={'text-xl'}>179. Gin - Hendrick's Gin.
                                                </h3>
                                            </TableCell>
                                            <TableCell style={{fontSize: "20px"}}>3,5€</TableCell>
                                            <TableCell style={{fontSize: "20px"}}>5,5€</TableCell>
                                        </TableRow>
                                    </TableBody>
                                </Table>

                            </div>
                        </div>}
                    />


                    <MenuDrink
                        title={'SWEIWEIN'}
                        image={Riesling}
                        alt={'Riesling'}
                        classImg={'lg:max-h-[1000px] lg:max-w-[600px]'}
                        children={<div
                            className="relative col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-8 justify-center">

                            <div className={'flex flex-col justify-center h-full'}>
                                <Table removeWrapper className={"bg-transparent"}>
                                    <TableHeader className={"bg-transparent "}>
                                        <TableColumn width={"80%"}
                                                     className={"bg-transparent !text-2xl"}></TableColumn>
                                        <TableColumn width={"10%"} className={"bg-transparent !text-2xl"}>
                                            <h3>0,2l</h3></TableColumn>
                                        <TableColumn width={"10%"} className={"bg-transparent !text-2xl"}>
                                            <h3>0,75l</h3></TableColumn>
                                    </TableHeader>
                                    <TableBody>
                                        <TableRow key="180">
                                            <TableCell style={{fontSize: "20px"}}>
                                                <div className="flex flex-col">
                                                    <h3 className={'text-xl'}>180. Riesling</h3>
                                                    <p className={'text-[#eb8b2e] text-sm font-medium indent-1'}>
                                                        Sachsen | Weingut Schloss Proschwitz.
                                                        VdP. Gutswein, trocken Feine Zitrusaromen, ein Hauch von
                                                        Apfel und. Pfirsich; feinste Mineralik sowie ein delikates
                                                        Süße-Säure-Spiel.
                                                    </p>
                                                </div>
                                            </TableCell>
                                            <TableCell style={{fontSize: "20px"}}>8,5€</TableCell>
                                            <TableCell style={{fontSize: "20px"}}>29,0€</TableCell>
                                        </TableRow>

                                        <TableRow key="181">
                                            <TableCell style={{fontSize: "20px"}}>
                                                <div className="flex flex-col">
                                                    <h3 className={'text-xl'}>181. Chardonnay „Mont Mès"</h3>
                                                    <p className={'text-[#eb8b2e] text-sm font-medium indent-1'}>
                                                        Südtirol | Weingut Castelfeder.
                                                        Vigneti delle Dolomiti IGT, trocken Fruchtig, frisch &
                                                        anregend im Trunk. mit sätigem, fruchtigem Abgang.
                                                        Unkompliziert & erfrischend.
                                                    </p>
                                                </div>
                                            </TableCell>
                                            <TableCell style={{fontSize: "20px"}}>5,9€</TableCell>
                                            <TableCell style={{fontSize: "20px"}}>23,0€</TableCell>
                                        </TableRow>

                                        <TableRow key="182">
                                            <TableCell style={{fontSize: "20px"}}>
                                                <div className="flex flex-col">
                                                    <h3 className={'text-xl'}>182. Rheingau Riesling Réserve</h3>
                                                    <p className={'text-[#eb8b2e] text-sm font-medium indent-1'}>
                                                        Rheingau | Weingut Schumann-Nägler.
                                                        QbA, trocken voller Körper, tiefgründig, aromareich,
                                                        fruchtig & krätigvoll mit viel Biss, aber gleichzeitig
                                                        harmonisch
                                                    </p>
                                                </div>
                                            </TableCell>
                                            <TableCell style={{fontSize: "20px"}}>5,9€</TableCell>
                                            <TableCell style={{fontSize: "20px"}}>22,0€</TableCell>
                                        </TableRow>

                                        <TableRow key="183">
                                            <TableCell style={{fontSize: "20px"}}>
                                                <div className="flex flex-col">
                                                    <h3 className={'text-xl'}>183. Aimery Chardonnay.</h3>
                                                    <p className={'text-[#eb8b2e] text-sm font-medium indent-1'}>
                                                        Frankreich - Languedoc l.
                                                        Aimery Sieur D'Arques Trocken. Aromen von
                                                        Akazie, Banane & Grapefruit, krätig im Geschmack mit voller
                                                        Frucht
                                                    </p>
                                                </div>
                                            </TableCell>
                                            <TableCell style={{fontSize: "20px"}}>5,9€</TableCell>
                                            <TableCell style={{fontSize: "20px"}}>22,0€</TableCell>
                                        </TableRow>


                                        <TableRow key="184">
                                            <TableCell style={{fontSize: "20px"}}>
                                                <div className="flex flex-col">
                                                    <h3 className={'text-xl'}>184. Asio Otus Vino Bianco.</h3>
                                                    <p className={'text-[#eb8b2e] text-sm font-medium indent-1'}>
                                                        Italien | MGM Mondo del Vino.
                                                        Chardonnay & Sauvignon Blanc, halbtrocken Frisch & satig,
                                                        aromatische
                                                        nach Birne & Zitrusfrüchten.
                                                        Angenehme Süße im Nachgang.
                                                    </p>
                                                </div>
                                            </TableCell>
                                            <TableCell style={{fontSize: "20px"}}>5,9€</TableCell>
                                            <TableCell style={{fontSize: "20px"}}>22,0€</TableCell>
                                        </TableRow>

                                        <TableRow key="185">
                                            <TableCell style={{fontSize: "20px"}}>
                                                <div className="flex flex-col">
                                                    <h3 className={'text-xl'}>185. Müller-Thurgau.</h3>
                                                    <p className={'text-[#eb8b2e] text-sm font-medium indent-1'}>
                                                        Baden | Alde Goti.
                                                        Qualitätswein, halbtrocken & harmonisch Feine Muskatnote, im
                                                        Abgang weich & elegant.
                                                    </p>
                                                </div>
                                            </TableCell>
                                            <TableCell style={{fontSize: "20px"}}>5,9€</TableCell>
                                            <TableCell style={{fontSize: "20px"}}>22,0€</TableCell>
                                        </TableRow>

                                    </TableBody>
                                </Table>
                            </div>
                        </div>
                        }
                    />
                    <MenuDrink
                        title={'ROTWEIN'}
                        image={Narassa}
                        alt={'ROTWEIN'}
                        classImg={'lg:max-h-[1000px] lg:max-w-[600px]'}
                        children={<div
                            className="relative col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-8 justify-center">
                            <div className={'flex flex-col justify-center h-full'}>
                                <Table removeWrapper className={"bg-transparent"}>
                                    <TableHeader className={"bg-transparent "}>
                                        <TableColumn width={"80%"}
                                                     className={"bg-transparent !text-2xl"}></TableColumn>
                                        <TableColumn width={"10%"} className={"bg-transparent !text-2xl"}>
                                            <h3>0,2l</h3></TableColumn>
                                        <TableColumn width={"10%"} className={"bg-transparent !text-2xl"}>
                                            <h3>0,75l</h3></TableColumn>
                                    </TableHeader>
                                    <TableBody>
                                        <TableRow key="186">
                                            <TableCell style={{fontSize: "20px"}}>
                                                <div className="flex flex-col">
                                                    <h3 className={'text-xl'}>186. Swissness Pinot Noir.</h3>
                                                    <p className={'text-[#eb8b2e] text-sm font-medium indent-1'}>
                                                        Schweiz - La Côte | UVAVINS.
                                                        Spätburgunder, trocken Santies Buketi mit Aromen von
                                                        Kirschen, Himbeeren und schwarzen Johannisbeeren; süffig &
                                                        fruchtig, begleite von feinen Tanninen im Nachgang.
                                                    </p>
                                                </div>
                                            </TableCell>
                                            <TableCell style={{fontSize: "20px"}}>5,9€</TableCell>
                                            <TableCell style={{fontSize: "20px"}}>22,0€</TableCell>
                                        </TableRow>

                                        <TableRow key="187">
                                            <TableCell style={{fontSize: "20px"}}>
                                                <div className="flex flex-col">
                                                    <h3 className={'text-xl'}>187. Lafage La Narassa.</h3>
                                                    <p className={'text-[#eb8b2e] text-sm font-medium indent-1'}>
                                                        Frankreich-Roussillon | Domaine Lafage.
                                                        70% Grenache, 30% Syrah, IGP, trocken Dominante Aromen von
                                                        Brombeere, getrockneten Kräutern, Unterholz & Pfeffer,
                                                        voller Körper mit santien Tanninen & dezenter Fruchtsüße.
                                                        Frischer Abgang mit überraschender Eleganz.
                                                    </p>
                                                </div>
                                            </TableCell>
                                            <TableCell style={{fontSize: "20px"}}>8,5€</TableCell>
                                            <TableCell style={{fontSize: "20px"}}>29,0€</TableCell>
                                        </TableRow>

                                        <TableRow key="188">
                                            <TableCell style={{fontSize: "20px"}}>
                                                <div className="flex flex-col">
                                                    <h3 className={'text-xl'}>188. Reserve de L'Aube
                                                        Syrah-Merlot.</h3>
                                                    <p className={'text-[#eb8b2e] text-sm font-medium indent-1'}>
                                                        Frankreich | Père Anselm.
                                                        65% Syrah, 35% Merlot, VdP D'Oc, trocken
                                                        Intensive Aromen dunkler Früchte, Morello-Kirschen & Cassis,
                                                        interessant, komplex & würzig mit krätigen, seidigen
                                                        Tanninen und guter Struktur.
                                                    </p>
                                                </div>
                                            </TableCell>
                                            <TableCell style={{fontSize: "20px"}}>5,9€</TableCell>
                                            <TableCell style={{fontSize: "20px"}}>22,0€</TableCell>
                                        </TableRow>

                                        <TableRow key="189">
                                            <TableCell style={{fontSize: "20px"}}>
                                                <div className="flex flex-col">
                                                    <h3 className={'text-xl'}>189. Asio Otus Vino Rosso.</h3>
                                                    <p className={'text-[#eb8b2e] text-sm font-medium indent-1'}>
                                                        Italien | MGM Mondo del Vino.
                                                        Cabernet Sauvignon, Merlot, Syrah, feinherb Explosives
                                                        Bouquet, harmonisch, körperreich, ausgeprägt & sensationell
                                                        würzig
                                                    </p>
                                                </div>
                                            </TableCell>
                                            <TableCell style={{fontSize: "20px"}}>5,9€</TableCell>
                                            <TableCell style={{fontSize: "20px"}}>22,0€</TableCell>
                                        </TableRow>

                                    </TableBody>
                                </Table>
                            </div>
                        </div>
                        }
                    />
                    <MenuDrink
                        title={'ROSÉ'}
                        image={champagne}
                        alt={'ROSÉ'}
                        classImg={'lg:max-h-[1000px] lg:max-w-[600px]'}
                        children={<div
                            className="relative col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-8 justify-center">
                            <div className={'flex flex-col justify-center h-full'}>
                                <Table removeWrapper className={"bg-transparent"}>
                                    <TableHeader className={"bg-transparent "}>
                                        <TableColumn width={"80%"}
                                                     className={"bg-transparent !text-2xl"}></TableColumn>
                                        <TableColumn width={"10%"} className={"bg-transparent !text-2xl"}>
                                            <h3>0,2l</h3></TableColumn>
                                        <TableColumn width={"10%"} className={"bg-transparent !text-2xl"}>
                                            <h3>0,75l</h3></TableColumn>
                                    </TableHeader>
                                    <TableBody>
                                        <TableRow key="190">
                                            <TableCell style={{fontSize: "20px"}}>
                                                <div className="flex flex-col">
                                                    <h3 className={'text-xl'}>190. Swissness Rosé de Gamay.
                                                    </h3>
                                                    <p className={'text-[#eb8b2e] text-sm font-medium indent-1'}>
                                                        Schweiz La Côte | UVAVINS 100% Gamay, trocken.
                                                        Feine Noten reifer Früchte wie Himbeeren & aromatische
                                                        Waldbeeren; satiig & geschmeidig, dabei herrlich frisch mit
                                                        gut ausbalancierter Säure und leichter Restsüfße.
                                                    </p>
                                                </div>
                                            </TableCell>
                                            <TableCell style={{fontSize: "20px"}}>5,9€</TableCell>
                                            <TableCell style={{fontSize: "20px"}}>22,0€</TableCell>
                                        </TableRow>

                                        <TableRow key="191">
                                            <TableCell style={{fontSize: "20px"}}>
                                                <div className="flex flex-col">
                                                    <h3 className={'text-xl'}>191. Rosé D'Anjou.</h3>
                                                    <p className={'text-[#eb8b2e] text-sm font-medium indent-1'}>
                                                        Frankreich - Val de Loire | Caves Monmousseau.
                                                        Lieblich und fruchtbetont, harmonisch im Trunk mit
                                                        deutlicher Süße.
                                                    </p>
                                                </div>
                                            </TableCell>
                                            <TableCell style={{fontSize: "20px"}}>8,5€</TableCell>
                                            <TableCell style={{fontSize: "20px"}}>29,0€</TableCell>
                                        </TableRow>


                                    </TableBody>
                                </Table>
                            </div>
                        </div>
                        }
                    />


                </div>
            </div>
            {/* Section CTA */}
            <section
                className="section kf-cta kf-parallax my-24"
                style={{backgroundImage: "url(images/cta_bg3.jpg)"}}
            >
                <div className="container">
                    <div className="row">
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-8">
                            <div className="kf-titles">
                                <div
                                    className="kf-subtitle element-anim-1 scroll-animate"
                                    data-animate="active"
                                >
                                    RESERVIERUNG
                                </div>
                                <h3
                                    className="kf-title element-anim-1 scroll-animate"
                                    data-animate="active"
                                >
                                    Sie benötigen einen Tisch für Ihre Familie
                                </h3>
                            </div>
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-4 align-self-center align-right">
                            <Link
                                href="reservation"
                                className="kf-btn element-anim-1 scroll-animate"
                                data-animate="active"
                            >
                                <span>JETZT BUCHEN</span>
                                <i className="fas fa-chevron-right"/>
                            </Link>
                        </div>
                    </div>
                </div>
            </section>
            <InstaCarousel/>
        </Layouts>
    );
};
export default Menu;

export const TitleMenuCate = ({title}) => {
    return (
        <div className="kf-titles align-center">
            <div
                className="kf-subtitle element-anim-1 scroll-animate"
                data-animate="active"
            >
            </div>
            <h3
                className="kf-title element-anim-1 scroll-animate"
                data-animate="active"
            >
                {title}
            </h3>
        </div>
    )
}