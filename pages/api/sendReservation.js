import dbConnect from '@/database/mongodb';
import ReservationModel from '@/models/ReservationModel';
import transporter from '@/utils/email'

export default async function handler(req, res) {
    if (req.method === 'POST') {
        return handlePOST(req, res);
    } else {
        return res.status(405).send({message: 'Method Not Allowed'});
    }
}

async function handlePOST(req, res) {
    await Promise.all([saveToDB, sendEmail].map(async f => await f(req.body)))
    return res.status(200).send({message: "Received Info"})

}

function createEmailOptions(body) {
    const {name, email, tel, persons, date, time, message} = body
    const mailOptions = {
        from: process.env.EMAIL_USER,
        to: process.env.EMAIL_RECEIVER,
        bcc: process.env.EMAIL_CC,
        subject: 'Reservierung',
        html: `
        <p><strong>Name:</strong> ${name}</p>
        <p><strong>Email:</strong> ${email}</p>
        <p><strong>Telefonnummer:</strong> ${tel}</p>
        <p><strong>Anzahl von Personen:</strong> ${persons} </p>
        <p><strong>Datum:</strong> ${date}</p>
        <p><strong>Zeit:</strong> ${time}</p>
        <p><strong>Nachricht:</strong> ${message}</p>
        `
    };
    return mailOptions;
}

async function sendEmail(body) {
    const mailOptions = createEmailOptions(body)
    try {
        await transporter.sendMail(mailOptions);
        console.log("finish sending email")
    } catch (error) {
        console.error("ERROR WHEN SENDING EMAIL", error);
    }
}

async function saveToDB(body) {
    const {name, email, tel, persons, date, time, message} = body
    try {

        await dbConnect()
        const newReservation = await ReservationModel.create({
            name,
            email,
            tel,
            persons,
            date,
            time,
            message
        });
        console.log("finish save to db")
    } catch (error) {
        console.error("ERROR WHEN PERSIT DB", error);

    }
}