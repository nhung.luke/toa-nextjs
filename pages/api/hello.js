// pages/api/hello.js

import { NextApiRequest, NextApiResponse } from 'next';

export default async function handler(req, res) {
  if (req.method === 'GET') {
    handleGET(req, res);
  } else if (req.method === 'POST') {
    handlePOST(req, res);
  } else {
    res.status(405).json({ message: 'Method Not Allowed' });
  }
}

function handleGET(req, res) {
  res.status(200).json({ message: 'Hello from GET!' });
}

function handlePOST(req, res) {
  const { name } = req.body;
  res.status(200).json({ message: `Hello, ${name}` });
}
