import NemRan from '@/images/foodImg/NemRan.jpeg';
import Pho from '@/images/foodImg/Pho.jpg';
import VitChien from '@/images/foodImg/vitchien.jpg';
import SakeAvocado from '@/images/foodImg/SakeAvocado.jpeg';
import GyuDon from '@/images/foodImg/ComTam.jpg';
import SakeDon from '@/images/bgFood/Hauptspeisen2.jpeg';
import UdonXao from '@/images/foodImg/UdonXao.jpg';

export const cateMenu = [
    {id: 'appetizers', name: 'VORSPEISEN'},
    {id: 'noodleDishes', name: 'ASIATISCHE NUDELGERICHTE'},
    {id: 'mainCourses', name: 'HAUPTSPEISEN'},
    {id: 'vegetarianDishes', name: 'VEGETARISCH STYLE TASTE OF ASIA'},
    {id: 'donburi', name: 'DONBURI'},
    {id: 'ramenNoodles', name: 'RAMEN NUDELN'},
    {id: 'sushiTOA', name: 'SUSHI TASTE OF ASIA'},
    {id: 'sasimi', name: 'SASIMI'},
    {id: 'tempura', name: 'TEMPURA'},
    {id: 'combo', name: 'SUSHI MENÜS'},
    {id: 'dessert', name: 'NACHTISCH'},
    {id: 'drinks', name: 'GETRÄNKE'},
    {id: 'alkohol', name: 'ALKOHOL'},
]

export const appetizerMenu = [
    {
        key: 1,
        name: 'Kokoscreme-Suppe.',
        sup: 'G',
        desc: 'Kokoscreme, Hühnerbrühe Gemüse, Pilze und Kräuter, wahlweise mit:',
        options: [
            {
                name: 'Tofu(m)',
                price: '5,5'
            },
            {
                name: 'Hühnerfleisch.',
                price: '6,5'
            },
            {
                name: 'Garnelen.',
                price: '6,5'
            },
        ]
    },
    {
        key: 2,
        name: 'Goi Du Du - Papaya Salat.',
        sup: 'B.C.M',
        desc: 'RCM Grüne Papaya, Sojasprossen, Karotten, Kräuter, Erdnüsse, Knoblauch, Chili, hausgemachte Dressing,',
        options: [
            {
                name: 'Vegan',
                price: '6,5'
            },
            {
                name: 'Hühnerfleisch.',
                price: '7,5'
            },
            {
                name: 'Garnelen.',
                price: '8,5'
            },
        ]
    },
    {
        key: 3,
        name: 'Goi Vit - Ente Mango Salat',
        sup: 'B.C.M',
        desc: 'frische Mango, Ente, Karotten, Minze, Erdnüsse, Röstzwiebeln, Feldsalat. (saisonal mit Pomelo) und hausgemachtes Dressing',
        price: '10,5'
    },
    {
        key: 4,
        name: 'Goi Cuon - Sommerrollen',
        sup: 'A.M',
        desc: 'Reispapier, Reisnudeln, Ei, Salat, Gurke, Karotten, Koriander und, Mango.wahlweise mit',
        options: [
            {
                name: 'Vegetarisch.',
                price: '5,9'
            },
            {
                name: 'Hühnerfleisch.',
                price: '6,5'
            },
            {
                name: 'Garnelen.',
                price: '7,5'
            },
        ]
    },
    {
        key: 5,
        name: 'Nem Ran - Frühlingsrollen',
        sup: 'E.M',
        desc: 'Traditionelle Frühlingsrollen gefüllt mit Hackfleisch, Garnelen, Karotten, Morcheln und Glasnudeln',
        price: '6,9',
        img:NemRan,
    },
    {
        key: 6,
        name: 'Tom Chien Com - Klebreisflocken',
        sup: 'D',
        desc: 'Garnelen 2 Black Tiger -Garnelen umwickelt von. knusprigen Klebreisflocken, dazu hausgemachter Dip',
        price: '7,9'
    },
    {
        key: 7,
        name: 'Wantan Chien - Gebackene Wantan',
        sup: 'D.E',
        desc: '4 knusprige Teigtaschen mit gehacktem Hühnerfleisch und Garnelen.als Füllung, dazu süß-saurer Soße',
        price: '6,9',
    },
    {
        key: 8,
        name: 'Dimsum - Teigtaschen',
        sup: '(D)',
        desc: 'Gedämpfte Teigtaschen mit verschiedenen Füllungen, dazu süß-saurer Soße',
        options: [
            {
                name: 'Vegetarisch.',
                sup: 'M',
                price: '5,9'
            },
            {
                name: 'Garnelen.',
                sup: 'E',
                price: '6,5'
            },
        ]
    },
];

export const japaneseAppetizers = [
    {
        key: 9,
        name: 'Khaivi - Vorspeise-set',
        desc: 'Große Platte mit.',
        options: [
            {
                name: 'Klebreisflocken-Garnelen',
                sup: 'D.F',
                price: '25,9',
            },
            {
                name: 'Papaya-Salat, Sommerrollen und Wantan.',
                sup: 'B.C, D.E',
                price: '25,9',
            },
        ]
    },
    {
        key: 10,
        name: 'Miso-Suppe',
        sup: 'M',
        desc: 'Sojabrühe, Seetang und Frühlingszwiebeln mit:',
        options: [
            {
                name: 'Seidentofu.',
                price: '6,5'
            },
            {
                name: 'Lachsstreifen & Seidentofu.',
                price: '7,5',
                sup: 'CM'
            },
        ]
    },
    {
        key: 11,
        name: 'Edamame',
        sup: 'M',
        desc: 'Gedämpfte japanische Bohnen mit Meersalz',
        price: '5,5'
    },
    {
        key: 12,
        name: 'Wakame',
        sup: 'M',
        desc: 'Eingelegter Seetang-Salat mit Sesam',
        price: '5,5'
    },
    {
        key: 13,
        name: 'Kim Chi Salat (scharf)',
        desc: 'Eingelegter Chinakohl',
        price: '5,9'
    },
    {
        key: 14,
        name: 'Gyoza ',
        sup: ' D.M',
        desc: 'Gefüllte Teigtaschen mit Soja-Dip,wahlweise mit:',
        options: [
            {
                name: 'Gemüse.',
                price: '6,5'
            },
            {
                name: 'Hühnerfleisch',
                price: '6,9',
            },
        ]
    },
    {
        key: 15,
        name: 'Yaki Tori.',
        desc: '3 stk gegrillte Hühnerspieße mit Teriyaki-Soßeund Sesam',
        price: '6,9'
    },
    {
        key: 16,
        name: 'Grüner Papaya Salad & Oktopus. Salat Oktopus',
        desc: 'Grüner Papayasalat mit gegrilltem Oktopus, Sesam, Erdnüssen, Limetten-Vinaigrette und Röstzwiebeln',
        price: '13,9',
        options: [
            {
                name: 'Oktopus Seafood.',
                price: '10,9',
                desc: 'Gegrillter Oktopus, Limetten mit Spezialsoße',
            },
        ]
    },
    {
        key: 17,
        name: 'Salmon Tataki',
        sup: 'C',
        desc: 'Leicht gegrillter Lachs im Sesam-Mantel.',
        price: '10,5'
    },
    {
        key: 18,
        name: 'Tuna Tataki',
        desc: 'Leicht gegrillter Thunfisch im',
        price: '11,5'
    },
];

export const asianNoodleDishes1 = [
    {
        key: 19,
        name: 'Pho - Reisbandnudel-Suppe',
        sup: 'M',
        desc: 'Traditionelle 12-Kräuter-Brühe mit Reisbandnudeln, Kräutern und Lauchzwiebeln, wahlweise mit',
        options: [
            {
                name: 'Hühnerfleisch.',
                price: '13,9',
            },
            {
                name: 'Rindfleisch.',
                price: '14,9',
            },
            {
                name: 'Hühner- & Rindfleisch.',
                price: '14,9',
            },
        ],
        img:Pho
    },
    {
        key: 20,
        name: 'Pho Xao - Gebratene Reisbandnudeln',
        sup: 'A.M',
        desc: 'in Wok geschwenkte Reisbandnudeln, Gemüse, Ei, Sojasprossen und. Röstzwiebeln, wahlweise mit:',
        options: [
            {
                name: 'Tofu',
                sup: 'M',
                price: '13,9',
            },
            {
                name: 'Hühnerfleisch.',
                price: '13,9',
            },
            {
                name: 'Rindfleisch',
                price: '14,9',
            },
            {
                name: 'Garnelen',
                sup: 'E',
                price: '15,9',
            },
        ]
    },
    {
        key: 21,
        name: 'Pho Tron Reisbandnudel-Salat',
        sup: 'B.M.V',
        desc: 'in Wok geschwenkte Reisbandnudeln, Gemüse, Ei, Sojasprossen und. Röstzwiebeln, wahlweise mit:',
        options: [
            {
                name: 'Tofu',
                sup: 'M',
                price: '12,9',
            },
            {
                name: 'Hühnerfleisch.',
                price: '13,9',
            },
            {
                name: 'Rindfleisch',
                price: '14,9',
            },
            {
                name: 'Garnelen',
                sup: 'E',
                price: '15,9',
            },
        ]
    },
];

export const asianNoodleDishes2 = [
    {
        key: 22,
        name: 'Udon Xao - Gebratene Udon',
        sup: 'D.G',
        desc: 'in Wok geschwenkte Udonnudeln, Gemüse und Teriyaki-Soße wahlweise mit',
        options: [
            {
                name: 'Tofu',
                sup: 'M',
                price: '13,9',
            },
            {
                name: 'Rindfleisch',
                price: '14,9',
            },
            {
                name: 'Garnelen',
                sup: 'E',
                price: '16,5',
            },
        ]
    },
    {
        key: 23,
        name: 'Udon Curry - Gebratene Udon',
        sup: 'D.G',
        desc: 'hausgemachte Curry-Soße, wahlweise mitin Wok geschwenkte Udonnudeln, Gemüse',
        options: [
            {
                name: 'Tofu',
                sup: 'M',
                price: '13,9',
            },
            {
                name: 'Hühnerfleisch',
                price: '14,9',
            },
            {
                name: 'Garnelen',
                sup: 'E',
                price: '15,9',
            },
        ],
        img:UdonXao
    },
];

export const hauptspeisen1 = [
    {
        key: 24,
        name: 'Ga Chien Gion',
        sup: 'D',
        desc: 'Knuspriges Hühnerfilet',
        price: '14,5',
    },
    {
        key: 25,
        name: 'Vit chien Gion',
        sup: 'D',
        desc: 'Knusprige Ente',
        price: '16,5',
        img:VitChien
    },
    {
        key: 26,
        name: 'Dau Xao - Gebratene Tofu',
        sup: 'M',
        desc: 'in Wok geschwenkter Tofu, Gemüse, wahlweise mit:',
        options: [
            {
                name: 'Teriyaki-Soße',
                price: '13,9',
            },
            {
                name: 'Curry-Soße ',
                price: '13,9',
            },
        ],
    },
    {
        key: 27,
        name: 'Uc Ga Nuong - Phönix Style.',
        desc: '250g Gegrillte Hähnchenfilet aus Thailand, Spargel, Zuckererbsenund Saisonalem Gemüse, abgeschmeckt mit Reis in Deluxe Sauce',
        price: '18,9',
    },
    {
        key: 28,
        name: 'Uc Vit Nuong - Ducky Duck.',
        desc: '250g Gegrillte weibliche Barbarie-Entebrust mit marktfrischem Gemüse in Deluxe Sauce',
        price: '20,9',
    },
    {
        key: 29,
        name: 'Ca Hoi Nuong - Teri Sake Lachs ',
        sup: 'E,C',
        desc: '250g Gegrillter Lachsmit 3 Garnelen, serviert mit Teriyaki-Sofße und. Tamarind-Soße',
        price: '24,9',
    },
];

export const hauptspeisen2 = [
    {
        key: 30,
        name: 'Ca Ngu Nuong - Teri Maguro Steak.',
        sup: 'E,C',
        desc: '250g Gegrillter Thunfisch mit 3 Garnelen,serviert mit Teriyaki-Soße und Tamarind-Soße',
        price: '26,9',
    },
    {
        key: 31,
        name: 'Tom Nuong - Teri Garnalen',
        sup: 'E',
        desc: '6Stk gegrillte Riesengarnelen ,Spargel, Zuckererbsenund ,SaisonalemGemüse, abgeschmeckt mit Reis in, serviert mit Teriyaki-Soße und Tamarind-Soße',
        price: '17,9',
    },
    {
        key: 32,
        name: 'Hai San Bien - Deep Blue Ocean',
        desc: '3stk Gegrillte Riesengarnelen, 3stk Jakobsmuscheln und Oktopus mit gegrilltem Gemüse, Kräuterseitlinge mit Tamarind-Soße',
        price: '26,9',
    },
    {
        key: 33,
        name: 'Bo Bit Tet - Surf & Turf.',
        sup: 'E',
        desc: '3Stk Garnelenspieß auf 250g medium gegrilltem argentinisches Rib-Eye Steak, begleitet von angebratenem Gemüse und der Whisky-Soße mit Süßkartoffeln',
        price: '26,9',
    },
    {
        key: 34,
        name: 'Bo Dua - Pineapple Gold.',
        desc: 'Rinderhüftsteak aus dem Wok mit Zitronengras, Chili, Ananas, Sellerie, Babymais, Zuckerschoten, Tomaten, Peperoni und Zwiebeln, Serviert in der frischen Ananas, schaft',
        price: '21,9',
    },
    {
        key: 35,
        name: 'Tom Dua - Coconut Prawn.',
        sup: 'E',
        desc: '5Stk gebratene Riesengarnelen mit markfrischem Gemüse, Babymais in Currysoße, serviert jungen Kokosnuss',
        price: '22,9',
    },
];

export const vegetarischTasteOfAsia1 = [
    {
        key: 36,
        name: 'Tamarinde Tofu.',
        sup: 'M',
        desc: 'Bestes verschiedenes Gemüse der Saison mit Tamarinde Soße und Tofu',
        price: '12,9',
    },
    {
        key: 37,
        name: 'White Fungus.',
        sup: 'M',
        desc: 'Weißer Fungus, knuspriger Tofu und Gemüse mit Soja Sesam-Soße',
        price: '13,9',
    },
];

export const vegetarischTasteOfAsia2 = [
    {
        key: 38,
        name: 'Spicy Tofu. ',
        sup: 'M',
        desc: 'Verschiedenes Gemüse mit Zitonengras, Chili und Tofu leicht schar',
        price: '13,9',
    },
    {
        key: 39,
        name: 'Vit chay',
        sup: 'D.M',
        desc: 'Seitan Ente, Zuckererbsen und saisonalem Gemüse, abgeschmeckt mit Reis in Deluxe Sauce',
        price: '14,9',
    },
];

export const donburi1 = [
    {
        key: 40,
        name: 'Gyu-Don.',
        desc: 'Eisschale, Teriyaki Rind.',
        price: '14,5',
        img:GyuDon
    },
    {
        key: 41,
        name: 'Sake-Don.',
        desc: 'Reisschale, Teriyaki Lachs.',
        price: '15,9',
        img:SakeDon
    },
    {
        key: 42,
        name: 'Tori-Don.',
        desc: 'Reisschale, Teriyaki Huhn.',
        price: '13,9',
    },
];

export const donburi2 = [
    {
        key: 43,
        name: 'Yasai-Don.',
        desc: 'Reisschale, Gemüse.',
        price: '11,9',
    },
    {
        key: 44,
        name: 'Maguro-Don.',
        desc: 'Reisschale, Teriyaki Thunfisch.',
        price: '16,9',
    },
    {
        key: 45,
        name: 'Unagi-Don.',
        desc: 'Reisschale, Unagi.',
        price: '16,9',
    },
];

export const ramenNudeln1 = [
    {
        key: 46,
        name: 'Spicy Signature Ramen Brühe',
        sup: 'A.M',
        desc: 'Schweinebrühe & scharfe dunkle Sojabohnen Einlagen: gehacktenes Hühnerbrustfilet, japanische Pilze, Frühlingszwiebeln, Morcheln, Kimchi, ein gekochtes Ei',
        price: '14,9',
    },
    {
        key: 47,
        name: 'Miso Ramen Brühe',
        sup: 'A.M',
        desc: 'Hühner- und Sojabohnenbrühe (8 Stunden Kochzeit). Einlagen: gehacktes Hähnchen, Schweine Charsiu, Mais, Morcheln, Frühlingszwiebeln, ein gekochtes Ei',
        price: '14,9',
    },
];

export const ramenNudeln2 = [
    {
        key: 48,
        name: 'Tonkotsu Ramen Brühe',
        sup: 'A',
        desc: 'Schweine- und Hühnerbrühe (8 Stunden Kochzeit). Einlagen: Schweine Charsiu, geschmortes Schweinefleisch, Mais, Morcheln, Porreestreifen, ein gekochtes Ei',
        price: '14,9',
    },
    {
        key: 49,
        name: 'Veggie Ramen Brühe',
        sup: 'A.M',
        desc: 'Dashi- und Sojabohnenbrühe (8 Stunden Kochzeit). Beilagen: Mais, japanische Pilze, Kimchi, Morcheln, Frühlingszwiebeln, ein gekochtes Ei',
        price: '13,9',
    },
];

export const nigiri = [
    {
        key: 50,
        name: 'Sake Lachs',
        sup: 'C',
        desc: '(flambiert + 0,5€)',
        price: '5,5',
    },
    {
        key: 51,
        name: 'Maguro Thunfisch.',
        desc: '(flambiert + 0,5€)',
        price: '5,5',
    },
    {
        key: 52,
        name: 'Spicy Sake',
        sup: 'C',
        desc: 'Marinierter Lachs.',
        price: '4,9',
    },
    {
        key: 53,
        name: 'Spicy Tekka',
        sup: 'C',
        desc: 'Marinierter Thunfisch.',
        price: '5,9',
    },
    {
        key: 54,
        name: 'Ebi',
        sup: 'E',
        desc: 'Garnelen, Gurke.',
        price: '5,5',
    },
    {
        key: 55,
        name: 'Unagic',
        desc: 'gegrillter Aal.',
        price: '5,9',
    },
    {
        key: 56,
        name: 'Hotategaih',
        desc: 'Jakobsmuscheln.',
        price: '5,9',
    },
    {
        key: 57,
        name: 'Hamachi.',
        price: '6,9',
    },
    {
        key: 58,
        name: 'Tamago',
        sup: 'A',
        price: '3,9',
    },
    {
        key: 59,
        name: 'Inari-Sushi',
        sup: 'M',
        desc: 'Süße Tofutasche gefüllt mit.',
        price: '3,9',
    },
];

export const maki = [
    {
        key: 60,
        name: 'Sake',
        sup: 'C',
        desc: 'Lachs.',
        price: '5,5',
    },
    {
        key: 61,
        name: 'Tuna.',
        sup: 'C',
        desc: 'Thunfisch.',
        price: '5,9',
    },
    {
        key: 62,
        name: 'Tekka Tuna.',
        sup: 'C',
        desc: 'Scharfer Thunfisch',
        price: '5,5',
    },
    {
        key: 63,
        name: 'Sake Avocado',
        sup: 'C',
        desc: 'Lachs,Avocado.',
        price: '5,5',
        img:SakeAvocado
    },
    {
        key: 64,
        name: 'Sake Kappa.',
        sup: 'C',
        desc: 'Lachs Gurke.',
        price: '5,5',
    },
    {
        key: 65,
        name: 'Ebi',
        sup: 'E',
        desc: 'Garnelen',
        price: '5,9',
    },
    {
        key: 66,
        name: 'Ebi. Avocado',
        sup: 'E',
        desc: 'Garnelen Avocado',
        price: '5,9',
    },
    {
        key: 67,
        name: 'California.',
        sup: 'E',
        desc: 'Surimi,Avocado.',
        price: '4,9',
    },
    {
        key: 68,
        name: 'Hühnchen',
        price: '5,5',
    },
    {
        key: 69,
        name: 'Hühnchen,',
        desc: 'Avocado.',
        price: '5,9',
    },
];

export const insideOutRolls = [
    {
        key: 70,
        name: 'California.',
        sup: 'AE',
        desc: 'Surimi, Avocado, Mayo.',
        price: '8,9',
    },
    {
        key: 71,
        name: 'Kani.',
        sup: 'AE',
        desc: 'Krebsfleisch. Avocado,Mayo.',
        price: '9,9',
    },
    {
        key: 72,
        name: 'Sake Avo.',
        sup: 'AC',
        desc: 'Lachs,Avocado,Mayo.',
        price: '9,9',
    },
    {
        key: 73,
        name: 'Maguro.',
        sup: 'AC',
        desc: 'Thunfisch,Gurke,Mayo.',
        price: '10,9',
    },
    {
        key: 74,
        name: 'Ebi.',
        sup: 'AE',
        desc: 'Großgarnelen,Avocado,Mayo.',
        price: '10,9',
    },
    {
        key: 75,
        name: 'Philly Cheese.',
        sup: 'CG',
        desc: 'Gurke,Avocado,Philly Cheese.',
        price: '8,9',
    },
    {
        key: 76,
        name: 'Salmon Skin.',
        sup: 'C',
        desc: 'Gegrillte Lachshaut,Avocado.',
        price: '9,9',
    },
    {
        key: 77,
        name: 'Unagi.',
        sup: 'C',
        desc: 'Süßwasser-Aal,Avocado.',
        price: '10,5',
    },
    {
        key: 78,
        name: 'Vegetarian.Inside-Out.',
        desc: 'Shiitake,O-Shinko Avocado',
        price: '8,9',
    },
    {
        key: 79,
        name: 'Asparagus Rooll.',
        sup: 'G',
        desc: 'Grüner Spargel,Philly Cheese,Schnittlauch',
        price: '8,9',
    },
];

export const sushiExtreme = [
    {
        key: 80,
        name: 'Spicy Chicken Roll (8 Stk.)',
        sup: 'A',
        desc: 'Chilimayo Hühnchen, Gurke, außen Avocado',
        price: '13,9',
    },
    {
        key: 81,
        name: 'Spicy Tuna Roll (8 Stk.)',
        sup: 'A,C,L',
        desc: 'Chilimayo ,Avocado.,Thunfisch, Gurke, Lauch, Sesam',
        price: '14,9',
    },
    {
        key: 82,
        name: 'Kalani (8Stk)',
        sup: 'C,E',
        desc: 'Gegrillte Lachshaut, Avocado, Großgarnelen, Unagisoße',
        price: '13,9',
    },
    {
        key: 83,
        name: "My Best Friend's Roll (8 Stk.)",
        sup: 'A,C,D',
        desc: 'Acavodo , Lachs, Spezial-Soße, Kresse, Mayo.',
        price: '14,9',
    },
    {
        key: 84,
        name: 'Taste of Asia Extreme Special (8 Stk.)',
        sup: 'A,C,D,E',
        desc: 'Garnelentempura, Lachs, Lachshaut, Tamago, Gurke, Masago, Tobiko,Unagisoße',
        price: '15,9',
    },
    {
        key: 85,
        name: "Mr. Miyagi's Pizza Sushi (4 Stk.)",
        sup: 'A,C,B',
        desc: 'Frittierter Sushireis, Lachs, Chilimayo, Gurken, Lauch, Masago',
        price: '10,9',
    },
    {
        key: 86,
        name: 'TNT Roll. (8 Stk.)',
        sup: 'A,C ',
        desc: 'Unagi ummantelt, Avocado, Gurke, Jalapeño, Koriander, Chili, süße Chilisoße, Unagisoße, Chilimayo',
        price: '14,9',
    },
    {
        key: 87,
        name: 'Cajun Salmon (8 Stk.)',
        sup: 'C,G',
        desc: 'Flambierter Lachs ummantelt, Lachs, Avocado,Spargel Cajun, Philly',
        price: '13,9',
    },
];

export const sasimi1 = [
    {
        key:88,
        name: 'Sake lachs',
        sup: 'C',
        price: '13,9',
        desc: '(6 Stk.)'
    },
    {
        key:89,
        name: 'Tuna thunfisch.',
        sup: 'C',
        price: '15,9',
        desc: '(6 Stk.)'
    },
]
export const sasimi2 = [
    {
        key:90,
        name: 'Tuna thunfisch, Sake-lachs',
        sup: 'C',
        price: '15,9',
        desc: '(6 Stk.)'
    },
    {
        key:91,
        name: ' Tuna thunfisch, Sake lachs, Hamachi',
        sup: 'C',
        price: '15,9',
        desc: '(6 Stk.)'
    },

]

export const vegetarischesSushi1 = [
    {
        key: 92,
        name: 'Inari-Sushi (2 Stk)',
        desc: 'Nigiri',
        price: '3,5',
    },
    {
        key: 93,
        name: 'Avocado (2 STK)',
        desc: 'Nigiri',
        price: '3,5',
    },
    {
        key: 94,
        name: 'Oshinko (2 STK)',
        desc: 'Nigiri',
        price: '3,5',
    },
    {
        key: 95,
        name: 'Tamago (2 STK)',
        desc: 'Nigiri',
        price: '3,5',
    },
    {
        key: 96,
        name: 'Kappa. Gurke (8 Stk.)',
        desc: 'Maki',
        price: '3,9',
    },
    {
        key: 97,
        name: 'Avocado.(8 Stk.)',
        desc: 'Maki',
        price: '3,9',
    },
];

export const vegetarischesSushi2 = [
    {
        key: 98,
        name: 'O-Shinko.(8 Stk.)',
        desc: 'Maki Marinierter Rettich.',
        price: '3,9',
    },
    {
        key: 99,
        name: 'Shiitake.(8 Stk.)',
        desc: 'Maki Shitake. Süße Tofutasche gefüllt mit Reis',
        price: '3,9',
    },
    {
        key: 100,
        name: 'Tamago (8 Stk.)',
        sup: 'A',
        desc: 'Maki Gesüßtes Omelette.',
        price: '4,5',
    },
    {
        key: 101,
        name: 'Vegetarian.(8 Stk.)',
        desc: 'Inside-Out Shiitake, O-Shinko, Avocado',
        price: '7,9',
    },
    {
        key: 102,
        name: 'Asparagus (8 Stk)',
        sup: 'G',
        desc: 'Inside-Out Grüner Spargel, Philly Cheese, Schnittlauch',
        price: '7,9',
    },
    {
        key: 103,
        name: 'Vegie Tempura (10 Stk.)',
        sup: 'D',
        desc: 'Maki. Mini Avocado, Mango, Paprika, Spargel, Tamago,Philadelphia',
        price: '6,5',
    },
    {
        key: 104,
        name: 'Vegie (6 Stk.)',
        sup: 'D.A',
        desc: 'Tempura Avocado, Gurke, Mango, Paprika, Spargel, Tamago, Philadelphia',
        price: '6,9',
    },
];

export const futomaki1 = [
    {
        key: 105,
        name: 'Sake',
        sup: 'C',
        desc: 'Lachs, Avocado, Gurke.',
        price: '10,9'
    },
    {
        key: 106,
        name: 'Tekka.',
        sup: 'C',
        desc: 'Thunfisch, Avocado, Gurke.',
        price: '10,9'
    },
]
export const futomaki2 = [

    {
        key: 107,
        name: 'Veggie.',
        sup: 'A',
        desc: 'Tamago (jap. Omelette) Gurke, Avocado, Rucola, Frischkäse',
        price: '9,5'
    },
]

export const tempura10stk = [
    {
        key: 108,
        name: 'Tempura Chicken.',
        sup: 'D',
        desc: 'Avocado, Gurke ,gebackenes ,Hühnerbrustfilet mit Mango-Chili-Dip',
        price: '10,9'
    },
    {
        key: 109,
        name: 'Salmon Tempura.',
        sup: 'C,G',
        desc: 'Maki Lachs, Avocado, Philadelphia, Sakura Kresse und Spezialsoße',
        price: '7,5'
    },
    {
        key: 110,
        name: 'Tuna Tempura.',
        sup: 'C,G',
        desc: 'Maki Thunfisch, Avocado, Philadelphia, Sakura Kresse und Spezialsoße',
        price: '7,9'
    },
    {
        key: 111,
        name: 'Hotategai Tempura.',
        sup: 'D,G',
        desc: 'Maki Jakobsmuschel, Avocado, Philadelphia, Sakura Kresse und Spezialsoße',
        price: '7,9'
    },
    {
        key: 112,
        name: 'Ebi Tempura.',
        sup: 'E,G',
        desc: 'Maki Große Garnelen, Avocado, Philadelphia, Sakura Kresse und Spezial-Soße',
        price: '7,9'
    },
    {
        key: 113,
        name: 'Hühnchen, TempuraMaki',
        desc: 'Hühnchen,Avocado, Philadelphia, Sakura Kresse und Spezial-Soße',
        price: '7,9'
    },
    {
        key: 114,
        name: 'Vegie Tempura',
        sup: 'D',
        desc: 'Maki Avocado, Mango, Paprika, Spargel,Tamago, Philadelphia ,Kresse und Spezial-Soße',
        price: '6,5'
    },
];
export const tempura6stk = [
    {
        key: 115,
        name: 'Salmon Tempura',
        sup: 'C,D,G',
        desc: 'Lachs, Avocado, Philadelphia, Sakura Kresse und Spezialsoße',
        price: '7,9'
    },
    {
        key: 116,
        name: 'Tuna Tempura',
        sup: 'C,D,G',
        desc: 'Thunfisch, Avocado, Philadelphia, Sakura Kresse und Spezialsoße',
        price: '7,9'
    },
    {
        key: 117,
        name: 'Hotategai Tempura.',
        sup: 'D,G,N',
        desc: 'Jakobsmuschel, Avocado, Philadelphia, Sakura Kresse und Spezialsoße',
        price: '8,9'
    },
    {
        key: 118,
        name: 'Ebi Tempura.',
        sup: 'D,G',
        desc: 'Große Garnelen, Avocado, Philadelphia, Sakura Kresse und Spezialsoße',
        price: '8,9'
    },
    {
        key: 119,
        name: 'The Taste of Big Roll.',
        sup: 'C.D.E.G',
        desc: 'Lachs, Thunfisch, Garnelen, Salzwasseraal, Avocado',
        price: '9,9'
    },
    {
        key: 120,
        name: 'Hühnchen, TSpicy Chicken .',
        desc: 'Tempura Hühnchen,Avocado, Philadelphia, Sakura Kresse und Spezial-Soße',
        price: '6,9'
    },
];

export const sushiMenus1 = [
    {
        name: 'MENÜ 1-VEGGIE MIX.',
        sup: 'A.D',
        price: '15,5',
        includes:
            [
                {
                    title: '2 Stk.Tamago -Oshinko.',
                    cate: 'Nigiri',
                },
                {
                    title: '8 Stk.Kappa Gurke',
                    cate: 'Maki',
                },
                {
                    title: '8 Stk.Acavado.',
                    cate: 'Maki',
                },
                {
                    title: '6 Stk.Tamago.',
                    cate: 'Tempura',
                },
            ]
    },
    {
        name: 'MENÜ 2- MAKI MIX.',
        sup: 'C.E',
        price: '16,5',
        includes:
            [
                {
                    title: '8 Stk. Sake Avocado Maki',
                },
                {
                    title: '8 Stk. Tekka Tuna.',
                    cate: 'Maki',
                },
                {
                    title: '10 Stk.Ebi',
                    cate: 'Tempura-Maki.Mini',
                },
            ]
    },
    {
        name: 'MENÜ 3-HELLO CALIFORNIA',
        sup: 'C.E',
        price: '19,9',
        includes:
            [
                {
                    title: '8 Stk. Cali Inside-Out ce.',
                },
                {
                    title: '8 Stk. Ebi -Garnelen',
                    cate: 'Maki',
                },
                {
                    title: '10 Stk. Sake -lachs Avocado',
                    cate: 'Tempura-Maki.Mini',
                },
            ]
    },
    {
        name: 'MENÜ 4- TASTE OF THE SEA. (für 2 Persone )',
        sup: 'C.E',
        price: '44,9',
        includes:
            [
                {
                    title: '2 Stk.Tuna Thunfisch',
                    cate: 'Maki',
                },
                {
                    title: '4 Stk. Lachs.',
                    cate: 'Sashimi',
                },
                {
                    title: '8 Stk. Sake Avocado.',
                    cate: 'Maki',
                },
                {
                    title: '8 Stk. Kali lnside',
                    cate: 'Out Roll',
                },
                {
                    title: '8 Stk. Spicy Tuna Roll',
                    cate: 'Extreme',
                }, {
                title: '6 Stk. Salmon',
                cate: 'Tempura',
            },
            ]
    },
]
export const sushiMenus2 = [
    {
        name: 'MENÜ 5- LOVE YOU 3000! (für 2 Persone )',
        sup: 'C.E.D',
        price: '52,9',
        includes:
            [
                {
                    title: '2 Stk.Ebi.',
                    cate: 'Nigiri',
                },
                {
                    title: '6 Stk. Tuna .Lachs. Hamachi',
                    cate: 'Sashimi',
                },
                {
                    title: '8 Stk. Kappa. Gurke',
                    cate: 'Maki',
                },
                {
                    title: '8 Stk. Sake Avocado',
                    cate: 'Makil',
                },
                {
                    title: '8 Stk. Spicy Tuna Roll',
                    cate: 'Extreme',
                }, {
                title: '6 Stk. Salmon',
                cate: 'Tempura',
            },
            ]
    },
    {
        name: 'MENÜ 6-LOST IN TOKYO (für 2-3 Personen)',
        sup: 'C.D.E.G',
        price: '66,9',
        includes:
            [
                {
                    title: '2 Stk. Tuna. Thunfisch',
                    cate: 'Nigiri',
                },
                {
                    title: '2 Stk. Hamachi.',
                    cate: 'Nigiri',
                },
                {
                    title: '4 Stk. Sake Lachs',
                    cate: 'Sashimi',
                },
                {
                    title: '8 Stk. Ebi. Garnelen.',
                    cate: 'Maki',
                },
                {
                    title: '8 Stk. Maguro.Thunfisch',
                    cate: 'Maki',
                },
                {
                    title: '8 Stk. Acavado',
                    cate: 'Maki',
                },
                {
                    title: '8 Stk. Sake-Lachs',
                    cate: 'Inside-Out',
                },
                {
                    title: '8 Stk. Cajun salmon.',
                    cate: 'Extreme',
                },
                {
                    title: '10 Stk Hühnchen.',
                    cate: 'tempura Maki. Mini',
                },
                {
                    title: '6 Stk.Salmon.',
                    cate: 'Tempura',
                },
            ]
    },
    {
        name: 'MENU 7-TASTE OF ASIA (für 4 Personen)',
        sup: 'C.E.A.G',
        price: '85,9',
        includes:
            [
                {
                    title: '2 Stk .Hamachi.',
                    cate: 'Nigiri',
                },
                {
                    title: '2 Stk .Ebi. - Garnelen.',
                    cate: 'Nigiri',
                },
                {
                    title: '2 Stk. Maguro -Thunfisch',
                    cate: 'Nigiri',
                },
                {
                    title: '6 Stk. Sake. -Lachs-Hamachi -',
                    cate: 'Sashimi',
                },
                {
                    title: '8 Stk. Sake. - Lachs. Avocado',
                    cate: 'Maki',
                },
                {
                    title: '8 Stk. Kappa',
                    cate: 'Maki',
                },
                {
                    title: '8 Stk. California',
                    cate: 'Inside-Out',
                },
                {
                    title: '8 Stk. Taste of Asia. Big Roll',
                    cate: 'Extreme Special',
                },
                {
                    title: '8 Stk. Spicy Chicken Roll.',
                    cate: 'Extreme Special',
                },
                {
                    title: '10 Stk.Ebi -Garnelen.',
                    cate: 'Tempura -Maki. Mini',
                },
                {
                    title: '12Stk. Salmon.',
                    cate: 'Tempura',
                },
            ]
    },
    {
        name: 'MENU 8 - THE STORY OF SUSHI( für 5-6 Personen)',
        price: '120',
        desc: "Lassen Sie sich von unserem Chef's Kreationen überrasschen!!"
    },


]

export const dessert1 = [
    {
        key: 121,
        name: 'Kem Xoi (3 Stk)',
        sup: 'GL',
        desc: 'leicht süßer Klebreis, Vanille-Eis, Kokoscreme, Sesam',
        price: '6,5'
    },
    {
        key: 122,
        name: 'Mochi',
        sup: 'DL',
        desc: 'Reiskuchen mit verschiedenen Füllung-Sorten zur Auswahl: Matcha, Sesam, Taro, Mango',
        price: '5,9'
    },
    {
        key: 123,
        name: 'Mochi Eisplatte (2 Stk)',
        sup: 'D.GL',
        desc: 'Reiskuchen, gefüll mit Eis, wahlweise mit saisonale Früchten. 2 Stk zur. Auswahl: Matcha, Kokosnuss, Erdbeer, Schokolade, Mango, VanilleNachspeisen',
        price: '5,9'
    },
    {
        key: 124,
        name: 'Koreanische Pfannkuchen.',
        desc: '(gefüllt mit Sesame oder Zimt)',
        price: '6,5'
    },
];
export const dessert2 = [
    {
        key: 125,
        name: 'Sesambällchen mit roten Bohnen.',
        price: '6,5'
    },
    {
        key: 126,
        name: 'Gebackene Banane.',
        desc: 'mit Honig flambiert',
        price: '6,5'
    },
    {
        key: 127,
        name: 'Gebackene Ananas.',
        desc: 'mit Honig',
        price: '6,5'
    },
    {
        key: 128,
        name: 'Kokoseis in einer Kokosnuss.',
        desc: 'Kokosnussraspeln, Fructose, LAKTOSE und MILCHproteine Kokosnussmilchpulver, getrocknetes Kokosnussmark, Maltodextrine, Natriumkaseinat (MILCH), Emulgator Stabilisatoren',
        price: '6,9'
    },
    {
        key: 129,
        name: 'Kokosbällchen.',
        desc: 'Bánh rán dừa 5stk, Wasser, Klebreismehl, Kokosnuss, Zucker, Pandanusbla',
        price: '6,5'
    },
];

export const homemades = [
    {
        key: 130,
        name: 'Limetten-Limonade.',
        desc: 'frische Limetten, Minze, stilles Wasser (oder Soda)',
        price: '5,5'
    },
    {
        key: 131,
        name: 'Maracuja Crush.',
        desc: 'Maracujasaft, Maracuja, Stilles Wasser (oder Soda)',
        price: '5,5'
    },
    {
        key: 132,
        name: 'Vinpearl Eistee.',
        desc: 'Schwarzer Tee, Limettensaft, Limette, Orange',
        price: '5,5'
    },
    {
        key: 133,
        name: 'Pfirsich Oolong-Tee.',
        desc: 'Oolong-Tee, Zitronengras, Orange, süßer Pfirsich',
        price: '5,5'
    },
    {
        key: 134,
        name: 'Mango Lassi.',
        desc: 'Mangopüree, Milch, Naturjoghurt',
        price: '5,5'
    },
    {
        key: 135,
        name: 'Cucumber Crush.',
        sup: 'C,D',
        desc: 'Gurke, Minze, Rohrzucker, Limette, Soda',
        price: '5,5'
    },
    {
        key: 136,
        name: 'Kumquat Crush.',
        sup: 'CD',
        desc: 'Kumquat-Frucht, Rohrzucker, Ingwer, Minze, Zitronengras, Soda',
        price: '5,5'
    },
    {
        key: 137,
        name: 'Exotic Passion',
        desc: 'Lychee, Kokos, Limette, Minze, Maracuja',
        price: '5,5'
    },
    {
        key: 138,
        name: 'Frisch Gepresster Orangensaft.',
        price: '5,5'
    },
];

export const softDrinks = [
    {
        name: 'Cola',
        sup: 'A,F',
        desc: 'Cola Zero, Fanta, Sprite, Paulaner Spezi',
        options: [
            {
                name: '0,3l',
                price: '3,2'
            },
            {
                name: '0,5l',
                price: '4,2',
            },
        ]
    },
    {
        name: 'Ginger Ale',
        desc: 'Bitter Lemon, Tonic',
        options: [
            {
                name: '0,2l',
                price: '3,2'
            },
            {
                name: '0,4l.',
                price: '4,5',
            },

        ]
    },
    {
        name: 'Säfte',
        desc: 'Apfel, Banane, Kirsch, Kiba, Mango, Lychee, Guave',
        options: [
            {
                name: '0,2l',
                price: '3,2'
            },
            {
                name: '0,4l.',
                price: '5,5',
            },
        ]
    },
    {
        name: 'Saftschorlen',
        desc: 'Apfel, Banane, Kirsch, Mango, Lychee, Guave',
        options: [
            {
                name: '0,2l',
                price: '3,2'
            },
            {
                name: '0,4l.',
                price: '5,5',
            },
        ]
    },
    {
        name: 'Stilles Wasser.',
        desc: 'Spritz/Sprudel',
        options: [
            {
                name: '0,3l',
                price: '2,9'
            },
            {
                name: '0,5l.',
                price: '3,9',
            },
        ]
    },
    {
        name: 'Flasche Wasser.',
        desc: 'Spritz/Sprudel',
        options: [
            {
                name: '0,75l.',
                price: '6,9'
            },
        ]
    },
]

export const tees = [
    {
        key: 145,
        name: 'Heiße Zitrone.',
        price: '4,2',
    },
    {
        key: 146,
        name: 'Frischer Minztee.',
        price: '4,2',
    },
    {
        key: 147,
        name: 'Grüner Tee.',
        price: '4,2',
    },
    {
        key: 148,
        name: 'Jasmintee.',
        price: '4,2',
    },
    {
        key: 149,
        name: 'Frischer Ingwertee.',
        price: '4,2',
    },
    {
        key: 150,
        name: "Tra Sa Gung'Zitronengras-Tee.",
        desc: 'Ingwer, Limetten, Zitronengras, Honig',
        price: '4,2',
    },
];

export const kaffees = [
    {
        key: 151,
        name: 'CAFÉ Trung Nguyen (Schwarz)',
        sup: 'F',
        desc: 'Aromatischer Filterkaffee mit einer Schokonote',
        price: '4,5',
    },
    {
        key: 152,
        name: 'CAFÉ Sua',
        sup: '4F',
        desc: 'Filterkaffee mit süßer Kondensmilch',
        price: '4,9',
    },
    {
        key: 153,
        name: 'CAFÉ Sua Da',
        sup: '4F',
        desc: 'Filterkaffee mit süßer Kondensmilch und Eiswürfeln',
        price: '4,9',
    },
    {
        key: 154,
        name: 'CAFÉ Sua Dual',
        sup: '4F',
        desc: 'COCOS-Coffee Filterkaffee mit eiskalter Kokosmilch',
        price: '6,5',
    },
];


export const aperatifsCocktails = [
    {
        key: 167,
        name: 'Fruity Gin.',
        desc: 'Gin, Limette, Apfelsaft,Zitronengras, Himbeere, Mandel- und Mangosirup',
        price: '7,9',
    },
    {
        key: 168,
        name: 'Mojito.',
        desc: 'Bacardi, Limette, Rohrzucker, Soda, Minze',
        price: '7,9',
    },
    {
        key: 169,
        name: 'Pink Mojito.',
        desc: 'Bacardi, Limette, Rohrzucker, Soda, verfeinert mit Himbeeren',
        price: '7,9',
    },
    {
        key: 170,
        name: 'Gin Gin Mule.',
        desc: 'Gin, Minze, Limette, Spicy Ginger',
        price: '7,9',
    },
    {
        key: 171,
        name: 'Moscow Mule.',
        desc: 'Wodka, Limette, Spicy Ginger, Gurke',
        price: '7,9',
    },
    {
        key: 172,
        name: 'Aperol Spritz.',
        desc: 'Aperol, Prosecco, Soda, Orange',
        price: '7,9',
    },
    {
        key: 173,
        name: 'Hugo.',
        desc: 'Prosecco, Limette, Minze, Holundersirup, Soda',
        price: '7,9',
    },
    {
        key: 174,
        name: 'Wild Berry Lillet.',
        desc: 'Lillet Blanc, Russian Wild Berry',
        price: '7,9',
    },
    // {
    //     key: 175,
    //     name: 'Prosecco.',
    //     desc: 'Scavi & Ray Spumante, elegant und geschmeidig.',
    //     price: '7,9',
    // }, {
    //     key: 176,
    //     name: 'Sake',
    //     desc: '(Japanischer Reiswein)',
    //     price: '7,9',
    // },
    // {
    //     key: 175,
    //     name: 'Caipirinha.',
    //     desc: '(sirup limetten bacadi,rot zucker.ginge ale)',
    //     price: '7,9',
    // },
    // {
    //     key: 176,
    //     name: 'Maitai',
    //     desc: '(bacadi, limetten, organgensaft)',
    //     price: '7,9',
    // },
    // {
    //     key: 177,
    //     name: 'Lemon Capi. (Non-alcoholic cocktails)',
    //     desc: '(sirup limetten, rot zucker, ginge ale)',
    //     price: '6,9',
    // },
    // {
    //     key: 178,
    //     name: 'Aloe Vera. (Non-alcoholic cocktails)',
    //     desc: '(Aloesaft, SchwarzTee)',
    //     price: '6,9',
    // },
    // {
    //     key: 179,
    //     name: 'Cocos Sahne. (Non-alcoholic cocktails)',
    //     desc: '(sirupCocos, Sahne AnanasSaft.Cocos Milch)',
    //     price: '6,9',
    // },
];

export const sektSpiritusen = [
    {
        name: 'Prosecco(Scavi & Ray Spumante)',
        option: [
            {
                name: '0,1l',
                price: '6,7'
            },
            {
                name: '0,75l',
                price: '29,0'
            }
        ]
    },
    {
        name: 'Sake (Japanischer Reiswein)',
        option: [
            {
                name: '0,1l',
                price: '5,9'
            }
        ]
    },
    {
        name: 'Nep Moi. (Vietnamesischer Reiswodka)',
        option: [
            {
                name: '0,1cl',
                price: '3,5'
            },
            {
                name: '0,2cl',
                price: '5,5'
            },
        ]
    },
    {
        name: 'Absolut Vodka.',
        option: [
            {
                name: '0,1cl',
                price: '3,5'
            },
            {
                name: '0,2cl',
                price: '3,9'
            },
        ]
    },
    {
        name: "Gin - Hendrick's Gin",
        option: [
            {
                name: '0,1cl',
                price: '3,5'
            },
            {
                name: '0,2cl',
                price: '5,5'
            },
        ]
    }
]

export const weiwein = [
    {
        name: 'Riesling Sachsen | Weingut Schloss Proschwitz.',
        desc: 'VdP. Gutswein, trocken Feine Zitrusaromen, ein Hauch von Apfel und. Pfirsich; feinste Mineralik sowie ein delikates Süße-Säure-Spiel.',
        option: [
            {
                name: '0,20l',
                price: '8,5'
            },
            {
                name: '0,75l',
                price: '29,0'
            },
        ]
    },
    {
        name: 'Chardonnay "Mont Mès" Südtirol | Weingut Castelfeder.',
        desc: 'Vigneti delle Dolomiti IGT, trocken Fruchtig, frisch & anregend im Trunk. mit sätigem, fruchtigem Abgang. Unkompliziert & erfrischend.',
        option: [
            {
                name: '0,20l',
                price: '5,9'
            },
            {
                name: '0,75l',
                price: '23,0'
            },
        ]
    },
    {
        name: 'Rheingau Riesling Réserve Rheingau | Weingut Schumann-Nägler.',
        desc: 'QbA, trocken voller Körper, tiefgründig, aromareich, fruchtig & krätigvoll mit viel Biss, aber gleichzeitig harmonisch',
        option: [
            {
                name: '0,20l',
                price: '5,9'
            },
            {
                name: '0,75l',
                price: '22,0'
            },
        ]
    },
    {
        name: 'Aimery Chardonnay. Frankreich - Languedoc l.',
        desc: "Aimery Sieur D'Arques Trocken. Aromen von Akazie, Banane & Grapefruit, krätig im Geschmack mit voller Frucht",
        option: [
            {
                name: '0,20l',
                price: '5,9'
            },
            {
                name: '0,75l',
                price: '22,0'
            },
        ]
    },
    {
        name: 'Asio Otus Vino Bianco. Italien | MGM Mondo del Vino.',
        desc: 'Chardonnay & Sauvignon Blanc, halbtrocken Frisch & satig, aromatische nach Birne & Zitrusfrüchten. Angenehme Süße im Nachgang.',
        option: [
            {
                name: '0,20l',
                price: '5,9'
            },
            {
                name: '0,75l',
                price: '22,0'
            },
        ]
    },
    {
        name: 'Müller-Thurgau. Baden | Alde Goti.',
        desc: 'Qualitätswein, halbtrocken & harmonisch Feine Muskatnote, im Abgang weich & elegant.',
        option: [
            {
                name: '0,20l',
                price: '5,9'
            },
            {
                name: '0,75l',
                price: '22,0'
            },
        ]
    },
]

export const rotweino = [
    {
        name: 'Schweiz - La Côte | UVAVINS.',
        desc: 'Spätburgunder, trocken Santies Buketi mit Aromen von Kirschen, Himbeeren und schwarzen Johannisbeeren; süffig & fruchtig, begleite von feinen Tanninen im Nachgang.',
        option: [
            {
                name: '0,20l',
                price: '5,9'
            },
            {
                name: '0,75l',
                price: '22,0'
            },
        ]
    },
    {
        name: 'Lafage La Narassa. Frankreich-Roussillon | Domaine Lafage.',
        desc: '70% Grenache, 30% Syrah, IGP, trocken Dominante Aromen von Brombeere, getrockneten Kräutern, Unterholz & Pfeffer, voller Körper mit santien Tanninen & dezenter Fruchtsüße. Frischer Abgang mit überraschender Eleganz.',
        option: [
            {
                name: '0,20l',
                price: 'l'
            },
            {
                name: '0,75l',
                price: '29,0'
            },
        ]
    },
    {
        name: "Reserve de L'Aube Syrah-Merlotm. Frankreich | Père Anselm.",
        desc: "65% Syrah, 35% Merlot, VdP D'Oc, trocken Intensive Aromen dunkler Früchte, Morello-Kirschen & Cassis, interessant, komplex & würzig mit krätigen, seidigen Tanninen und guter Struktur.",
        option: [
            {
                name: '0,20l',
                price: '5,9'
            },
            {
                name: '0,75l',
                price: '22,0'
            },
        ]
    },
    {
        name: 'Asio Otus Vino Rosso. Italien | MGM Mondo del Vino.',
        desc: 'Cabernet Sauvignon, Merlot, Syrah, feinherb Explosives Bouquet, harmonisch, körperreich, ausgeprägt & sensationell würzig.',
        option: [
            {
                name: '0,20l',
                price: '5,9'
            },
            {
                name: '0,75l',
                price: '22,0'
            }
        ]
    }
]
export const rose = [
    {
        name: 'Swissness Rosé de Gamay. Schweiz La Côte | UVAVINS 100% Gamay, trocken.',
        desc: 'Feine Noten reifer Früchte wie Himbeeren & aromatische Waldbeeren; satiig & geschmeidig, dabei herrlich frisch mit gut ausbalancierter Säure und leichter Restsüfße.',
        option: [
            {
                name: '0,20l',
                price: '5,9'
            },
            {
                name: '0,75l',
                price: '22,0'
            },
        ]
    },
    {
        name: 'asdsda',
        desc: 'asdasd',
        option: [
            {
                name: '0,20l',
                price: 'adfasd'
            },
            {
                name: '0,75l',
                price: 'adfasd'
            },
        ]
    },
    {
        name: 'asdsda',
        desc: 'asdasd',
        option: [
            {
                name: '0,20l',
                price: 'adfasd'
            },
            {
                name: '0,75l',
                price: 'adfasd'
            },
        ]
    },
    {
        name: 'asdsda',
        desc: 'asdasd',
        option: [
            {
                name: '0,20l',
                price: 'adfasd'
            },
            {
                name: '0,75l',
                price: 'adfasd'
            },
        ]
    },
    {
        name: 'asdsda',
        desc: 'asdasd',
        option: [
            {
                name: '0,20l',
                price: 'adfasd'
            },
            {
                name: '0,75l',
                price: 'adfasd'
            },
        ]
    },
    {
        name: 'asdsda',
        desc: 'asdasd',
        option: [
            {
                name: '0,20l',
                price: 'adfasd'
            },
            {
                name: '0,75l',
                price: 'adfasd'
            },
        ]
    },
]
