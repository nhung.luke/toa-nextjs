import Link from "next/link";
import {useEffect, useState} from "react";
import {stickyNav} from "../utils";

const Header = () => {
    useEffect(() => {
        stickyNav();
    }, []);

    const [toggle, setToggle] = useState(false);

    useEffect(() => {
        if (document.querySelector("header").className.includes("animated")) {
            setTimeout(() => {
                document.querySelector("header").classList.add("opened", "show");
            }, 800);
        }
    }, [toggle]);

    const [activeMenu, setActiveMenu] = useState("");
    const activeMenuSet = (value) =>
            setActiveMenu(activeMenu === value ? "" : value),
        activeLi = (value) =>
            value === activeMenu ? {display: "block"} : {display: "none"};

    return (
        <header className={`kf-header ${toggle ? "animated" : ""}`}>
            {/* topline */}
            <div className="kf-topline">
                <div className="row">
                    <div className="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                        {/* hours */}
                        <div className="kf-h-group">
                            <i className="far fa-clock"/> <em>Öffnungszeiten :</em> 11:30 am -
                            10:00 pm
                        </div>
                    </div>
                    <div className="col-xs-12 col-sm-6 col-md-4 col-lg-4 align-center">
                        {/* social */}
                        <div className="kf-h-social">
                            <a href="https://www.facebook.com/tasteofasia.lichtenfels" target="blank">
                                <i className="fab fa-facebook-f"/>
                            </a>
                            {/*<a href="twitter.com" target="blank">*/}
                            {/*    <i className="fab fa-twitter"/>*/}
                            {/*</a>*/}
                            <a href="https://www.instagram.com/taste.of.asia.lichtenfels/" target="blank">
                                <i className="fab fa-instagram"/>
                            </a>
                            <a href="https://youtube.com" target="blank">
                                <i className="fab fa-youtube"/>
                            </a>
                        </div>
                    </div>
                    <div className="col-xs-12 col-sm-6 col-md-4 col-lg-4 align-right">
                        {/* location */}
                        <div className="kf-h-group">
                            <i className="fas fa-map-marker-alt"/> <em>Standort: </em>
                            Bahnhofstraße 5, 96215 Lichtenfels, Germany
                        </div>
                    </div>
                </div>
            </div>
            {/* navbar */}
            <div className="kf-navbar">
                <div className="row">
                    <div className="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                        {/* logo */}
                        <div className="kf-logo">
                            <Link href="/">
                                <img src="images/toa_logo.png" alt="image"/>
                            </Link>
                        </div>
                    </div>
                    <div className="col-xs-12 col-sm-6 col-md-6 col-lg-6 align-center">
                        {/* main menu */}
                        <div className="kf-main-menu">
                            <ul>
                                <li>
                                    <Link href="/">
                                        Startseite
                                    </Link>
                                </li>
                                {/*<li>*/}
                                {/*  <Link href="about">About</Link>*/}
                                {/*</li>*/}
                                <li>
                                    <Link href="menu">
                                        Menü
                                    </Link>
                                </li>
                                {/*<li>*/}
                                {/*  <a href="#">*/}
                                {/*    Pages*/}
                                {/*    <i className="las la-angle-down" />*/}
                                {/*  </a>*/}
                                {/*  <ul>*/}
                                {/*    <li>*/}
                                {/*      <Link href="services">Service</Link>*/}
                                {/*    </li>*/}
                                {/*    <li>*/}
                                {/*      <Link href="reservation">Reservation</Link>*/}
                                {/*    </li>*/}
                                {/*    <li>*/}
                                {/*      <Link href="history">History</Link>*/}
                                {/*    </li>*/}
                                {/*    <li>*/}
                                {/*      <Link href="team">Our Chefs</Link>*/}
                                {/*    </li>*/}
                                {/*    <li>*/}
                                {/*      <Link href="gallery">Gallery</Link>*/}
                                {/*    </li>*/}
                                {/*    <li>*/}
                                {/*      <Link href="faq">FAQ</Link>*/}
                                {/*    </li>*/}
                                {/*  </ul>*/}
                                {/*</li>*/}
                                {/*<li>*/}
                                {/*  <a href="#">*/}
                                {/*    Blog*/}
                                {/*    <i className="las la-angle-down" />*/}
                                {/*  </a>*/}
                                {/*  <ul>*/}
                                {/*    <li>*/}
                                {/*      <Link href="blog-grid">Blog Grid</Link>*/}
                                {/*    </li>*/}
                                {/*    <li>*/}
                                {/*      <Link href="blog">Blog Standard</Link>*/}
                                {/*    </li>*/}
                                {/*    <li>*/}
                                {/*      <Link href="blog-single">Blog Single</Link>*/}
                                {/*    </li>*/}
                                {/*  </ul>*/}
                                {/*</li>*/}
                                <li>
                                    <Link href="gallery">Galerie</Link>
                                </li>
                                <li>
                                    <Link href="reservation">Reservierung</Link>
                                </li>
                                <li>
                                    <Link href="contacts">Kontakt</Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div className="col-xs-12 col-sm-6 col-md-3 col-lg-3 align-right">
                        {/* menu btn */}
                        <a
                            href="#"
                            className={`kf-menu-btn ${toggle ? "active" : ""}`}
                            onClick={() => setToggle(!toggle)}
                        >
                            <span/>
                        </a>
                        {/* btn */}
                        {/*<Link href="https://taste-of-asia.order.app.hd.digital" className="kf-btn h-btn" target={"_blank"}>*/}
                        <Link href="/reservation" className="kf-btn h-btn" >
                            <span>Tisch reservieren</span>
                        </Link>
                    </div>
                </div>
            </div>
            {/* mobile navbar */}
            <div className="kf-navbar-mobile">
                {/* mobile menu */}
                <div className="kf-main-menu">
                    <ul>
                        <li className="has-children">
                            <Link href="/">Startseite</Link>
                        </li>
                        <li className="has-children">
                            <Link href="menu">Menü</Link>
                        </li>
                        <li>
                            <Link href="gallery">Galerie</Link>
                        </li>
                        <li>
                            <Link href="reservation">Reservierung</Link>
                        </li>
                        <li>
                            <Link href="contacts">Kontakt</Link>
                        </li>
                    </ul>
                </div>
                {/* mobile topline */}
                <div className="kf-topline">
                    <div className="row">
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            {/* mobile btn */}
                            <Link href="/reservation" className="kf-btn h-btn" target={"_blank"}>
                                <span>Tisch reservieren</span>
                                <i className="fas fa-chevron-right"/>
                            </Link>
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            {/* social */}
                            <div className="kf-h-social">
                                <a href="https://www.facebook.com/tasteofasia.lichtenfels" target="blank">
                                    <i className="fab fa-facebook-f"/>
                                </a>
                                {/*<a href="twitter.com" target="blank">*/}
                                {/*    <i className="fab fa-twitter"/>*/}
                                {/*</a>*/}
                                <a href="https://www.instagram.com/taste.of.asia.lichtenfels/" target="blank">
                                    <i className="fab fa-instagram"/>
                                </a>
                                <a href="https://youtube.com" target="blank">
                                    <i className="fab fa-youtube"/>
                                </a>
                            </div>
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            {/* hours */}
                            <div className="kf-h-group">
                                <i className="far fa-clock"/> <em>Öffnungszeiten :</em> 11:30 am
                                - 10:00 pm
                            </div>
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            {/* location */}
                            <div className="kf-h-group">
                                <i className="fas fa-map-marker-alt"/> <em>Standort :</em>
                                Bahnhofstraße 5, 96215 Lichtenfels, Germany
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    );
};
export default Header;
