import Image from "next/image";

const Footer = () => {
    return (
        <div className="kf-footer">
            <div className="container">
                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                        {/* logo */}
                        <div
                            className="kf-logo element-anim-1 scroll-animate"
                            data-animate="active"
                        >
                            <a href="/">
                                <Image
                                    src={`/images/toa_logo.png`}
                                    height={70}
                                    width={200}
                                    alt="Ing"
                                />
                            </a>
                        </div>
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                        {/* hours */}
                        <div
                            className="kf-f-hours element-anim-1 scroll-animate"
                            data-animate="active"
                        >
                            <h5>Öffnungszeiten</h5>
                            <ul>
                                <li>
                                    <i className="las la-calendar mr-2"></i>
                                    Täglich geöffnet
                                    <em>11:30 am - 10:00 pm</em>
                                </li>
                            </ul>

                        </div>
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                        {/* contact */}
                        <div
                            className="kf-f-contact element-anim-1 scroll-animate"
                            data-animate="active"
                        >
                            <h5>Kontaktiere uns</h5>

                            <ul>
                                <li>
                                    <i className="las la-map-marker"/>
                                    <em>Standort :</em>
                                    Bahnhofstraße 5, 96215 Lichtenfels, Germany
                                </li>
                                <li>
                                    <i className="las la-envelope-open-text"/>
                                    <em>E-Mail :</em>
                                    lichtenfels@tasteofasia.restaurant
                                </li>
                                <li>
                                    <i className="las la-phone"/>
                                    <em>Telefonnummer:</em>
                                    +49 (0)9571 169 02 58
                                </li>
                            </ul>

                        </div>
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                        {/* gallery */}
                        <h5>Verbindung</h5>
                        <div className="flex row justify-content-start mb-3 fs-4">
                            <a href="https://www.facebook.com/tasteofasia.lichtenfels" style={{width: 36}}
                               target="blank">
                                <i className="fab fa-facebook-f"/>
                            </a>
                            <a href="https://www.instagram.com/taste.of.asia.lichtenfels/" style={{width: 40}}
                               target="blank">
                                <i className="fab fa-instagram"/>
                            </a>
                            <a href="https://youtube.com" style={{width: 40}} target="blank">
                                <i className="fab fa-youtube"/>
                            </a>
                        </div>
                        <div
                            className="kf-f-gallery element-anim-1 scroll-animate"
                            data-animate="active"
                        >
                            <h5>Galerie</h5>
                            <ul>
                                <li>
                                    <a
                                        href="images/toa_kg_1.jpg"
                                        className="kf-image-hover has-popup-image"
                                    >
                                        <Image
                                            src={`/images/toa_kg_1.jpg`}
                                            height={80}
                                            width={80}
                                            alt="Ing"
                                        />
                                    </a>
                                </li>
                                <li>
                                    <a
                                        href="images/toa_kg_2.jpg"
                                        className="kf-image-hover has-popup-image"
                                    >
                                        <Image
                                            src={`/images/toa_kg_2.jpg`}
                                            height={80}
                                            width={80}
                                            alt="Ing"
                                        />
                                    </a>
                                </li>
                                <li>
                                    <a
                                        href="images/toa_kg_3.jpg"
                                        className="kf-image-hover has-popup-image"
                                    >
                                        <Image
                                            src={`/images/toa_kg_3.jpg`}
                                            height={80}
                                            width={80}
                                            alt="Ing"
                                        />
                                    </a>
                                </li>
                                <li>
                                    <a
                                        href="images/toa_kg_4.jpg"
                                        className="kf-image-hover has-popup-image"
                                    >
                                        <Image
                                            src={`/images/toa_kg_4.jpg`}
                                            height={80}
                                            width={80}
                                            alt="Ing"
                                        />
                                    </a>
                                </li>
                                <li>
                                    <a
                                        href="images/toa_kg_5.jpg"
                                        className="kf-image-hover has-popup-image"
                                    ><Image
                                        src={`/images/toa_kg_5.jpg`}
                                        height={80}
                                        width={80}
                                        alt="Ing"
                                    />
                                    </a>
                                </li>
                                <li>
                                    <a
                                        href="images/toa_kg_6.jpg"
                                        className="kf-image-hover has-popup-image"
                                    >
                                        <Image
                                            src={`/images/toa_kg_6.jpg`}
                                            height={80}
                                            width={80}
                                            alt="Ing"
                                        />
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 align-center">
                        {/* copyright */}
                        <div
                            className="kf-copyright element-anim-1 scroll-animate"
                            data-animate="active"
                        >
                            {/*Copyright © 2022 Kaffen. All Rights Reserved.*/}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};
export default Footer;
