import Isotope from "isotope-layout";
import { useEffect, useRef, useState } from "react";
import GalleryItem from "@/src/components/gallery/GalleryItem";

const PhotoGalleryIsotope = () => {
  // Isotope
  const isotope = useRef();
  const [filterKey, setFilterKey] = useState("*");
  useEffect(() => {
    setTimeout(() => {
      isotope.current = new Isotope(".kf-gallery-items", {
        itemSelector: ".kf-gallery-col",
        //    layoutMode: "fitRows",
        percentPosition: true,
        masonry: {
          columnWidth: ".kf-gallery-col",
        },
        animationOptions: {
          duration: 750,
          easing: "linear",
          queue: false,
        },
      });
    }, 1000);
    //     return () => isotope.current.destroy();
  }, []);
  useEffect(() => {
    if (isotope.current) {
      filterKey === "*"
        ? isotope.current.arrange({ filter: `*` })
        : isotope.current.arrange({ filter: `.${filterKey}` });
    }
  }, [filterKey]);
  const handleFilterKeyChange = (key) => () => {
    setFilterKey(key);
    const filterLinks = document.querySelectorAll(".kf-filter a");
    filterLinks.forEach((filter) => {
      const filterValue = filter.getAttribute("data-href");
      if (filterValue == key) {
        filter.classList.add("active");
      } else {
        filter.classList.remove("active");
      }
    });
  };
  return (
    <section className="section kf-gallery">
      <div className="container">
        <div className="kf-titles align-center">
          <div
            className="kf-subtitle element-anim-1 scroll-animate"
            data-animate="active"
          >
            Unsere Fotogalerie
          </div>
          <h3
            className="kf-title element-anim-1 scroll-animate"
            data-animate="active"
          >
            Schaut in unsere Fotogalerie
          </h3>
        </div>
        <div
          className="kf-filter kf-filter-gal element-anim-1 scroll-animate"
          data-animate="active"
        >
          <a
            className="c-pointer active"
            onClick={handleFilterKeyChange("*")}
            data-href="*"
          >
            Alle Fotos
          </a>

          <a
            className="c-pointer"
            data-href="restaurant"
            onClick={handleFilterKeyChange("restaurant")}
          >
            Restaurantbereich
          </a>
          <a
            className="c-pointer"
            data-href="food"
            onClick={handleFilterKeyChange("food")}
          >
            Die Gastronomie
          </a>

        </div>
        <div className="kf-gallery-items row">

          <GalleryItem
              genre={'restaurant'}
              name={'Das Restauranttor'}
              subname={'Restaurant'}
              image={'images/restaurant/20.png'}
          />

          <GalleryItem
              genre={'restaurant'}
              name={'Das Restauranttor'}
              subname={'Restaurant'}
              image={'images/restaurant/23.png'}
          />
          <GalleryItem
              genre={'food'}
              name={'Uc Vit Nuong - Ducky Duck'}
              subname={'HAUPTSPEISEN'}
              image={'images/bestMenu/16.png'}
              styleImg={{objectPosition: 'center 86%'}}
          />

          <GalleryItem
              genre={'restaurant'}
              name={'Der Restaurant-Außenbereich'}
              subname={'Restaurant'}
              image={'images/restaurant/21.jpeg'}
          />

          <GalleryItem
              genre={'food'}
              name={'Bo Dua - Pineapple Gold.'}
              subname={'HAUPTSPEISEN'}
              image={'images/bestMenu/20.jpg'}
              styleImg={{objectPosition: 'center 86%'}}
          />

          <GalleryItem
              genre={'food'}
              name={'Set Sashimi'}
              subname={'Sashimi'}
              image={'images/foodImg/sashimi1.jpg'}
          />

          <GalleryItem
              genre={'restaurant'}
              name={'Der Restaurant-Außenbereich'}
              subname={'Restaurant'}
              image={'images/restaurant/22.jpeg'}
          />

          <GalleryItem
              genre={'food'}
              name={'MENU 7-TASTE OF ASIA'}
              subname={'SUSHI MENÜS'}
              image={'images/bgFood/BgMenu1.jpg'}
          />

          <GalleryItem
              genre={'restaurant'}
              name={'Im Inneren des Restaurants'}
              subname={'Restaurant'}
              image={'images/restaurant/1.jpg'}
          />

          <GalleryItem
              genre={'restaurant'}
              name={'Im Inneren des Restaurants'}
              subname={'Restaurant'}
              image={'images/restaurant/2.jpg'}
          />

          <GalleryItem
              genre={'food'}
              name={'MENÜ 6-LOST IN TOKYO'}
              subname={'SUSHI MENÜS'}
              image={'images/bestMenu/combo1.jpeg'}
          />

          <GalleryItem
              genre={'restaurant'}
              name={'Im Inneren des Restaurants'}
              subname={'Restaurant'}
              image={'images/restaurant/3.jpg'}
          />

          <GalleryItem
              genre={'food'}
              name={'Tempura Chicken.'}
              subname={'TEMPURA'}
              image={'images/bgFood/tempura1.jpg'}
          />

          <GalleryItem
              genre={'restaurant'}
              name={'Lobby'}
              subname={'Restaurant'}
              image={'images/restaurant/4.jpg'}
          />

          <GalleryItem
              genre={'restaurant'}
              name={'Lobby'}
              subname={'Restaurant'}
              image={'images/restaurant/5.jpg'}
          />

          <GalleryItem
              genre={'food'}
              name={'Pho - Reisbandnudel-Suppe'}
              subname={'ASIATISCHE NUDEL GERICHTE'}
              image={'images/bestMenu/1.jpg'}
          />

          <GalleryItem
              genre={'food'}
              name={'Udon Xao - Gebratene Udon'}
              subname={'ASIATISCHE NUDEL GERICHTE'}
              image={'images/bestMenu/15Sub.jpg'}
          />

          <GalleryItem
              genre={'restaurant'}
              name={'Vor dem Restaurant'}
              subname={'Restaurant'}
              image={'images/restaurant/6.jpg'}
          />

          <GalleryItem
              genre={'food'}
              name={'Tom Dua - Coconut Prawn'}
              subname={'HAUPTSPEISEN'}
              image={'images/bestMenu/21.jpg'}
              styleImg={{objectPosition: 'center 67%'}}
          />

          <GalleryItem
              genre={'restaurant'}
              name={'Im Inneren des Restaurants'}
              subname={'Restaurant'}
              image={'images/restaurant/7.jpg'}
          />

          <GalleryItem
              genre={'restaurant'}
              name={'Im Inneren des Restaurants'}
              subname={'Restaurant'}
              image={'images/restaurant/8.jpg'}
          />

          <GalleryItem
              genre={'food'}
              name={'Dimsum - Teigtaschen'}
              subname={'VORSPEISEN'}
              image= {'images/bestMenu/7.jpeg'}
          />

          <GalleryItem
              genre={'restaurant'}
              name={'Im Inneren des Restaurants'}
              subname={'Restaurant'}
              image={'images/restaurant/9.jpg'}
          />

          <GalleryItem
              genre={'food'}
              name={'Tom Chien Com - Klebreisflocken'}
              subname={'VORSPEISEN'}
              image= {'images/bestMenu/5.jpeg'}
          />

          <GalleryItem
              genre={'food'}
              name={'Tuna Tataki'}
              subname={'JAPANISCHE VORSPEISEN'}
              image= {'images/bestMenu/14.jpg'}
              styleImg={{objectPosition: 'center 96%'}}
          />

          <GalleryItem
              genre={'restaurant'}
              name={'Im Inneren des Restaurants'}
              subname={'Restaurant'}
              image={'images/restaurant/10.jpg'}
          />

          <GalleryItem
              genre={'restaurant'}
              name={'Im Inneren des Restaurants'}
              subname={'Restaurant'}
              image={'images/restaurant/11.jpg'}
          />

          <GalleryItem
              genre={'restaurant'}
              name={'Im Inneren des Restaurants'}
              subname={'Restaurant'}
              image={'images/restaurant/12.jpg'}
          />

          <GalleryItem
              genre={'restaurant'}
              name={'Im Inneren des Restaurants'}
              subname={'Restaurant'}
              image={'images/restaurant/14.jpg'}
          />

          <GalleryItem
              genre={'food'}
              name={'Taste of Asia Extreme Special'}
              subname={'SUSHI EXTREME'}
              image={'images/bgFood/SushiExtreme.jpg'}
          />

          <GalleryItem
              genre={'restaurant'}
              name={'Bar'}
              subname={'Restaurant'}
              image={'images/restaurant/13.jpg'}
          />

          <GalleryItem
              genre={'food'}
              name={' Spicy Signature Ramen Brühe'}
              subname={'RAMEN NUDELN'}
              image={'images/foodImg/Ramen1.webp'}
          />

          <GalleryItem
              genre={'food'}
              name={'Gyu-Don.'}
              subname={'DONBURI'}
              image={'images/foodImg/GyuDon.jpeg'}
          />


          <GalleryItem
              genre={'restaurant'}
              name={'Im Inneren des Restaurants'}
              subname={'Restaurant'}
              image={'images/restaurant/15.jpg'}
          />

          <GalleryItem
              genre={'restaurant'}
              name={'Im Inneren des Restaurants'}
              subname={'Restaurant'}
              image={'images/restaurant/16.jpg'}
          />

          <GalleryItem
              genre={'restaurant'}
              name={'Vor dem Restaurant'}
              subname={'Restaurant'}
              image={'images/restaurant/17.jpg'}
          />

          <GalleryItem
              genre={'food'}
              name={'Goi Du Du - Papaya Salat'}
              subname={'VORSPEISEN'}
              image={'images/bestMenu/2.jpeg'}
              styleImg={{objectPosition: 'center 87%'}}
          />

          <GalleryItem
              genre={'food'}
              name={'Nem Ran - Frühlingsrollen'}
              subname={'VORSPEISEN'}
              image={'images/bestMenu/4.jpeg'}
              styleImg={{objectPosition: 'center 100%'}}
          />

          <GalleryItem
              genre={'restaurant'}
              name={'Im Inneren des Restaurants'}
              subname={'Restaurant'}
              image={'images/restaurant/18.jpg'}
          />


          <GalleryItem
              genre={'restaurant'}
              name={''}
              subname={'Restaurant'}
              image={'images/restaurant/18.jpg'}
          />

          <GalleryItem
              genre={'food'}
              name={'Oktopus. Salat Oktopus'}
              subname={'JAPANISCHE VORSPEISEN'}
              image={'images/bestMenu/13.jpg'}
              styleImg={{objectPosition: 'center 90%'}}
          />

          <GalleryItem
              genre={'food'}
              name={'Yaki Tori.'}
              subname={'VORSPEISEN'}
              image={'images/bestMenu/12.png'}
              styleImg={{objectPosition: 'center 80%'}}
          />

          <GalleryItem
              genre={'food'}
              name={'Ca Hoi Nuong - Teri Sake Lachs'}
              subname={'HAUPTSPEISEN'}
              image={'images/bestMenu/17Sub.jpg'}
              // styleImg={{objectPosition: 'center 50%'}}
          />

      <GalleryItem
              genre={'food'}
              name={'Kappa. Gurke '}
              subname={'VEGETARISCHE SUSHI'}
              image={'images/bgFood/vegetarisch.jpg'}
              styleImg={{objectPosition: '20%'}}
          />



          {/*<div className="kf-gallery-col col-xs-12 col-sm-12 col-md-6 col-lg-4 all fast-food">*/}
          {/*  <div*/}
          {/*    className="kf-gallery-item element-anim-1 scroll-animate"*/}
          {/*    data-animate="active"*/}
          {/*  >*/}
          {/*    <div className="image kf-image-hover">*/}
          {/*      <a href="images/ins_gal1.jpg" className="has-popup-gallery">*/}
          {/*        <img src="images/ins_gal1.jpg" alt="image" />*/}
          {/*      </a>*/}
          {/*    </div>*/}
          {/*    <div className="desc">*/}
          {/*      <div className="name">*/}
          {/*        <div className="value">italian burger</div>*/}
          {/*      </div>*/}
          {/*      <div className="subname">*/}
          {/*        <div className="value">fast food</div>*/}
          {/*      </div>*/}
          {/*    </div>*/}
          {/*  </div>*/}
          {/*</div>*/}
          {/*<div className="kf-gallery-col col-xs-12 col-sm-12 col-md-6 col-lg-4 all dinner-menu">*/}
          {/*  <div*/}
          {/*    className="kf-gallery-item element-anim-1 scroll-animate"*/}
          {/*    data-animate="active"*/}
          {/*  >*/}
          {/*    <div className="image kf-image-hover">*/}
          {/*      <a href="images/ins_gal2.jpg" className="has-popup-gallery">*/}
          {/*        <img src="images/ins_gal2.jpg" alt="image" />*/}
          {/*      </a>*/}
          {/*    </div>*/}
          {/*    <div className="desc">*/}
          {/*      <div className="name">*/}
          {/*        <div className="value">italian pasta</div>*/}
          {/*      </div>*/}
          {/*      <div className="subname">*/}
          {/*        <div className="value">dinner menu</div>*/}
          {/*      </div>*/}
          {/*    </div>*/}
          {/*  </div>*/}
          {/*</div>*/}
          {/*<div className="kf-gallery-col col-xs-12 col-sm-12 col-md-6 col-lg-4 all fast-food">*/}
          {/*  <div*/}
          {/*    className="kf-gallery-item element-anim-1 scroll-animate"*/}
          {/*    data-animate="active"*/}
          {/*  >*/}
          {/*    <div className="image kf-image-hover">*/}
          {/*      <a href="images/ins_gal3.jpg" className="has-popup-gallery">*/}
          {/*        <img src="images/ins_gal3.jpg" alt="image" />*/}
          {/*      </a>*/}
          {/*    </div>*/}
          {/*    <div className="desc">*/}
          {/*      <div className="name">*/}
          {/*        <div className="value">Chicken burger</div>*/}
          {/*      </div>*/}
          {/*      <div className="subname">*/}
          {/*        <div className="value">fast food</div>*/}
          {/*      </div>*/}
          {/*    </div>*/}
          {/*  </div>*/}
          {/*</div>*/}
          {/*<div className="kf-gallery-col col-xs-12 col-sm-12 col-md-6 col-lg-4 all special-pizza">*/}
          {/*  <div*/}
          {/*    className="kf-gallery-item element-anim-1 scroll-animate"*/}
          {/*    data-animate="active"*/}
          {/*  >*/}
          {/*    <div className="image kf-image-hover">*/}
          {/*      <a href="images/ins_gal4.jpg" className="has-popup-gallery">*/}
          {/*        <img src="images/ins_gal4.jpg" alt="image" />*/}
          {/*      </a>*/}
          {/*    </div>*/}
          {/*    <div className="desc">*/}
          {/*      <div className="name">*/}
          {/*        <div className="value">italian pizza</div>*/}
          {/*      </div>*/}
          {/*      <div className="subname">*/}
          {/*        <div className="value">special pizza</div>*/}
          {/*      </div>*/}
          {/*    </div>*/}
          {/*  </div>*/}
          {/*</div>*/}
          {/*<div className="kf-gallery-col col-xs-12 col-sm-12 col-md-6 col-lg-4 all hot-coffee">*/}
          {/*  <div*/}
          {/*    className="kf-gallery-item element-anim-1 scroll-animate"*/}
          {/*    data-animate="active"*/}
          {/*  >*/}
          {/*    <div className="image kf-image-hover">*/}
          {/*      <a href="images/ins_gal5.jpg" className="has-popup-gallery">*/}
          {/*        <img src="images/ins_gal5.jpg" alt="image" />*/}
          {/*      </a>*/}
          {/*    </div>*/}
          {/*    <div className="desc">*/}
          {/*      <div className="name">*/}
          {/*        <div className="value">cappuccino</div>*/}
          {/*      </div>*/}
          {/*      <div className="subname">*/}
          {/*        <div className="value">hot coffee</div>*/}
          {/*      </div>*/}
          {/*    </div>*/}
          {/*  </div>*/}
          {/*</div>*/}
          {/*<div className="kf-gallery-col col-xs-12 col-sm-12 col-md-6 col-lg-4 all fast-food">*/}
          {/*  <div*/}
          {/*    className="kf-gallery-item element-anim-1 scroll-animate"*/}
          {/*    data-animate="active"*/}
          {/*  >*/}
          {/*    <div className="image kf-image-hover">*/}
          {/*      <a href="images/ins_gal6.jpg" className="has-popup-gallery">*/}
          {/*        <img src="images/ins_gal6.jpg" alt="image" />*/}
          {/*      </a>*/}
          {/*    </div>*/}
          {/*    <div className="desc">*/}
          {/*      <div className="name">*/}
          {/*        <div className="value">Chicken nuggets</div>*/}
          {/*      </div>*/}
          {/*      <div className="subname">*/}
          {/*        <div className="value">fast food</div>*/}
          {/*      </div>*/}
          {/*    </div>*/}
          {/*  </div>*/}
          {/*</div>*/}
          {/*<div className="kf-gallery-col col-xs-12 col-sm-12 col-md-6 col-lg-4 all hot-coffee">*/}
          {/*  <div*/}
          {/*    className="kf-gallery-item element-anim-1 scroll-animate"*/}
          {/*    data-animate="active"*/}
          {/*  >*/}
          {/*    <div className="image kf-image-hover">*/}
          {/*      <a href="images/grid_gal1.jpg" className="has-popup-gallery">*/}
          {/*        <img src="images/grid_gal1.jpg" alt="image" />*/}
          {/*      </a>*/}
          {/*    </div>*/}
          {/*    <div className="desc">*/}
          {/*      <div className="name">*/}
          {/*        <div className="value">Black coffee</div>*/}
          {/*      </div>*/}
          {/*      <div className="subname">*/}
          {/*        <div className="value">hot coffee</div>*/}
          {/*      </div>*/}
          {/*    </div>*/}
          {/*  </div>*/}
          {/*</div>*/}
          {/*<div className="kf-gallery-col col-xs-12 col-sm-12 col-md-6 col-lg-4 all dinner-menu">*/}
          {/*  <div*/}
          {/*    className="kf-gallery-item element-anim-1 scroll-animate"*/}
          {/*    data-animate="active"*/}
          {/*  >*/}
          {/*    <div className="image kf-image-hover">*/}
          {/*      <a href="images/menu_r4.jpg" className="has-popup-gallery">*/}
          {/*        <img src="images/menu_r4.jpg" alt="image" />*/}
          {/*      </a>*/}
          {/*    </div>*/}
          {/*    <div className="desc">*/}
          {/*      <div className="name">*/}
          {/*        <div className="value">Spaghetti</div>*/}
          {/*      </div>*/}
          {/*      <div className="subname">*/}
          {/*        <div className="value">dinner menu</div>*/}
          {/*      </div>*/}
          {/*    </div>*/}
          {/*  </div>*/}
          {/*</div>*/}
          {/*<div className="kf-gallery-col col-xs-12 col-sm-12 col-md-6 col-lg-4 all dinner-menu">*/}
          {/*  <div*/}
          {/*    className="kf-gallery-item element-anim-1 scroll-animate"*/}
          {/*    data-animate="active"*/}
          {/*  >*/}
          {/*    <div className="image kf-image-hover">*/}
          {/*      <a href="images/menu_r6.jpg" className="has-popup-gallery">*/}
          {/*        <img src="images/menu_r6.jpg" alt="image" />*/}
          {/*      </a>*/}
          {/*    </div>*/}
          {/*    <div className="desc">*/}
          {/*      <div className="name">*/}
          {/*        <div className="value">Sea Fish</div>*/}
          {/*      </div>*/}
          {/*      <div className="subname">*/}
          {/*        <div className="value">dinner menu</div>*/}
          {/*      </div>*/}
          {/*    </div>*/}
          {/*  </div>*/}
          {/*</div>*/}
        </div>
      </div>
    </section>
  );
};
export default PhotoGalleryIsotope;
