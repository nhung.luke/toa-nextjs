import React from 'react';

const GalleryItem = ({name, subname, genre, image, styleImg}) => {
    return (
        <div className={`kf-gallery-col col-xs-12 col-sm-12 col-md-6 col-lg-4 all ${genre}`}>
            <div
                className="kf-gallery-item element-anim-1 scroll-animate"
                data-animate="active"
            >
                <div className="image kf-image-hover">
                    <a href={`/${image}`} className="has-popup-gallery">
                        <img src={`/${image}`} alt="image" style={styleImg} />
                    </a>
                </div>
                <div className="desc">
                    <div className="name">
                        <div className="value">{name}</div>
                    </div>
                    <div className="subname">
                        <div className="value">{subname}</div>
                    </div>
                </div>
            </div>
        </div>

    );
};

export default GalleryItem;