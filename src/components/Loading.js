import React from 'react'

const Loading = () => {
  return (
    <div className="fixed top-0 left-0 right-0 bottom-0 bg-black bg-opacity-50 flex justify-center items-center z-50">
        <i className="fa fa-spinner fa-spin fa-3x fa-fw text-white" aria-hidden="true"></i>
        <span className="text-white text-18 font-bold ml-10">Loading...</span>
    </div>
  )
}

export default Loading