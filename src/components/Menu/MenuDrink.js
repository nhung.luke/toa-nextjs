import React from 'react';
import {TitleMenuCate} from "@/pages/menu";
import Image from "next/image";

const MenuDrink = ({id, children, title, image, alt, classImg}) => {
    return (
        <section className={`section kf-menu }`}  id={id}>
            <div className="container">
                <TitleMenuCate title={title}/>
                <div className="row justify-center relative">

                    <div className={'lg:absolute lg:brightness-40 h-full col-xs-12 col-sm-12 col-md-12'}>
                        <Image className={`w-full h-full m-auto object-contain  max-h-[400px] max-w-[600px] ${classImg}`}
                             {...image}
                             alt={alt}/>
                    </div>

                    {children}


                </div>
            </div>
        </section>
    );
};

export default MenuDrink;