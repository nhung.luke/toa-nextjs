import React from 'react';
import Image from "next/image";

const BestMenu = ({name, subName, price, image, imageSub}) => {
    return (
        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <div
                className="kf-menu-item element-anim-1 scroll-animate"
                data-animate="active"
            >
                <div className="image kf-image-hover">
                    <a href={imageSub? `/${imageSub}` : `/${image}`} className="has-popup-image">
                        <Image
                            src={`/${image}`}
                            fill
                            style={{objectFit: "cover"}}
                            alt="Background Image"
                        />
                    </a>
                </div>
                <div className="desc">
                    <h5 className="name">{name}</h5>
                    <div className="subname text-[#f1a848]">{subName}</div>
                    <div className="price">
                        <span>€{price}</span>
                    </div>
                </div>
            </div>
        </div>

    );
};

export default BestMenu;