import React from 'react';
import {appetizerMenu} from "@/src/dataMenu";
import {TitleMenuCate} from "@/pages/menu";
import Image from "next/image";


const MenuFood = ({title='', data = [], id = '', banner='', propsStyle}) => {
    return (
                <div className="col-xs-12 col-sm-12 col-md-12 col-xl-6" id={id}>
                    {title && <TitleMenuCate title={title}/>}
                    <div
                        className="kf-menu-image-2 left element-anim-1 scroll-animate"
                        data-animate="active"
                        style={{
                            ...propsStyle
                        }}
                    >
                        <Image fill  src={banner.src} alt={''} className={'object-cover'}/>
                    </div>

                    <div className="kf-menu-items-2">

                        {data.map((item, index) => {
                            const {key, name, sup, desc, price, img, options} = item;
                            return (
                                <div key={key}
                                     className="kf-menu-item-2 element-anim-1 scroll-animate"
                                     data-animate="active">
                                    {/*<div className="image kf-image-hover">*/}
                                    {/*    <a href="images/menu_r1.jpg" className="has-popup-image">*/}
                                    {/*        <img src={img ? img.src :"images/toa_food_5.jpg"} alt="image"/>*/}
                                    {/*    </a>*/}
                                    {/*</div>*/}

                                    <div className="detail">
                                        <h4 className="name">{`${key}. ${name}`}
                                            <sup className={'cate'}>{sup}</sup>
                                        </h4>
                                        {desc && <div className="desc whitespace-normal">{desc}</div>}
                                        {options && options.map((option, optionIndex) => {
                                                const alphabeticalIndex = String.fromCharCode(97 + optionIndex)
                                                return (
                                                    <div className="relative" key={optionIndex}>
                                                        <div className="option">
                                                            {`${alphabeticalIndex}. ${option.name}`}
                                                            {option.sup &&
                                                                <sup className={'cate'}>{option.sup}</sup>}
                                                        </div>
                                                        {option.desc && <div className="desc whitespace-normal">{option.desc}</div>}
                                                        <div className="price">
                                                            <span>{option.price}€</span>
                                                        </div>
                                                    </div>
                                                )
                                            }
                                        )}
                                        {price && <div className="price">
                                            <span>{price}€</span>
                                        </div>}
                                    </div>
                                </div>
                            );
                        })}
                    </div>
                </div>
    );
};

export default MenuFood;