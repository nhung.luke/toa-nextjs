import React, { useState } from 'react'
import {  toast } from 'react-toastify';
import Loading from './Loading';

const ReservationForm = () => {
    const initialReservation = {
        name: '',
        email: '',
        tel: '',
        persons: '',
        date: '',
        time: '',
        message: ''
    };

    const [reservationInformation, setReservationInformation] = useState(initialReservation)
    const [errors, setErrors] = useState({});
    const [loading, setLoading] = useState(false);

    const handleOnChange = (e) =>{
      const { name, value } = e.target;
      setReservationInformation(prevState => ({
        ...prevState,
        [name]: value
      }));
    }

    const validateForm = () => {
      const newErrors = {};
      if (!reservationInformation.name) 
        newErrors.name = 'Bitte geben Sie Ihren vollständigen Namen ein!';
      if (!reservationInformation.email) 
        newErrors.email = 'Bitte geben Sie Ihre E-Mail-Adresse ein!';
      if (!reservationInformation.tel) 
        newErrors.tel = 'Bitte geben Sie Ihre Telefonnummer ein!';
      if (!reservationInformation.persons) 
        newErrors.persons = 'Bitte geben Sie die Anzahl der Personen ein!';
      if (!reservationInformation.date) 
        newErrors.date = 'Bitte geben Sie Ihr Reservierungsdatum ein!';
      if (!reservationInformation.time) 
        newErrors.time = 'Bitte wählen Sie Ihre Reservierungszeit aus!';

      setErrors(newErrors);
      return Object.keys(newErrors).length === 0;
    };

    const sendReservationRequest = async () => {
      setLoading(true);
      try {
        const response = await fetch('/api/sendReservation', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(reservationInformation),
        });
          toast.success('Erfolgreiches Reservierungsanfrage gesendet!');
          setReservationInformation(initialReservation)
      } catch (error) {
        toast.error('Beim Senden der Reservierungsanfrage ist ein Fehler aufgetreten!');
      }
      finally {
        setLoading(false); 
      }
    }
      
    const handleSubmitForm = async (e) =>{
      e.preventDefault();
      if (!validateForm()) {
        toast.error('Bitte füllen Sie alle erforderlichen Felder aus!');
        return;
      }
      sendReservationRequest()
    }

  return (
    <>
        <div className="container">
            <div className="kf-titles align-center">
              <div className="kf-subtitle">Buchung eines Tisches'</div>
              <h3 className="kf-title">Reservierung vornehmen</h3>
            </div>
            {loading && <Loading />} 
            <form id="rform" method="post">
              <div className="row">
                <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                  <div className="kf-field">
                    <input type="text" name="name" 
                    value={reservationInformation.name} 
                    onChange={handleOnChange} placeholder="Vollständiger Name" required/>
                    <i className="far fa-user" />
                    {errors && <div className="text-danger small">{errors.name}</div>}
                  </div>
                </div>
                <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                  <div className="kf-field">
                    <input
                      type="email"
                      name="email"
                      value={reservationInformation.email} 
                      onChange={handleOnChange}
                      placeholder="E-Mail-Adresse"
                      required
                    />
                    <i className="fas fa-at" />
                    {errors && <div className="text-danger small">{errors.email}</div>}
                  </div>
                </div>
                <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                  <div className="kf-field">
                    <input type="tel" name="tel" 
                    value={reservationInformation.tel} 
                    onChange={handleOnChange} 
                    placeholder="Telefonnummer"
                    required
                    />
                    <i className="fas fa-mobile-alt" />
                    {errors && <div className="text-danger small">{errors.tel}</div>}
                  </div>
                </div>
                <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                  <div className="kf-field">
                    <input type="number" name="persons" 
                    min={1}
                    max={100}
                    value={reservationInformation.persons} 
                    onChange={handleOnChange} 
                    placeholder="Anzahl der Personen"
                    required
                    />
                    
                    {errors && <div className="text-danger small">{errors.persons}</div>}
                  </div>
                </div>
                <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                  <div className="kf-field">
                    <input type="text"
                     value={reservationInformation.date} 
                     onChange={handleOnChange}
                     name="date"
                     placeholder="dd/mm/yyyy"
                     onFocus={e => e.target.type="date"}
                     required
                     />
                    <i className="far fa-calendar-alt" />
                    {errors && <div className="text-danger small">{errors.date}</div>}
                  </div>
                </div>
                <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                  <div className="kf-field">
                    <select type='text' name="time"
                    value={reservationInformation.time} 
                    onChange={handleOnChange}>
                      <option value="">Uhrzeit</option>
                      <option value="11:30">11:30</option>
                      <option value="12:00">12:00</option>
                      <option value="12:30">12:30</option>
                      <option value="13:00">13:00</option>
                      <option value="13:30">13:30</option>
                      <option value="14:00">14:00</option>
                      <option value="14:30">14:30</option>
                      <option value="15:00">15:00</option>
                      <option value="15:30">15:30</option>
                      <option value="16:00">16:00</option>
                      <option value="16:30">16:30</option>
                      <option value="17:00">17:00</option>
                      <option value="17:30">17:30</option>
                      <option value="18:00">18:00</option>
                      <option value="18:30">18:30</option>
                      <option value="19:00">19:00</option>
                      <option value="19:30">19:30</option>
                      <option value="20:00">20:00</option>
                      <option value="20:30">20:30</option>
                      <option value="21:00">21:00</option>
                      <option value="21:30">21:30</option>
                      <option value="22:00">22:00</option>
                    </select>
                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                    {errors && <div className="text-danger small">{errors.time}</div>}
                  </div>
                </div>
                <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <div className="kf-field">
                    <textarea type="text" name="message" 
                    value={reservationInformation.message} 
                    onChange={handleOnChange}
                    placeholder="Nachricht"/>
                    <i className="far fa-comment-dots" />{" "}
                  </div>
                </div>
                <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <div className="kf-bts">
                    <a
                      href="#"
                      className="kf-btn"
                      onClick={handleSubmitForm}
                    >
                      <span>TISCH RESERVIEREN</span>
                      <i className="fas fa-chevron-right" />
                    </a>
                  </div>
                </div>
              </div>
            </form>
        </div>
      </>
  )
}

export default ReservationForm