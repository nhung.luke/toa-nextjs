import {sliderProps} from "@/src/sliderProps";
import {Swiper, SwiperSlide} from "swiper/react";
import Image from "next/image";

const InstaCarousel = () => {

    const gallery = Array.apply(null, Array(26)).map(function (_, index) {
        const i = index + 1
        return <SwiperSlide key={"food" + i} className="swiper-slide">
            <div className="slide-item">
                <div className="image kf-image-hover">
                    <a href={`images/toa_food_${i}.jpg`} key={'imgfood' + i} data-fancybox="gallery"
                       data-caption={`Food ${i}`}>
                        <Image
                            src={`/images/toa_food_${i}.jpg`}
                            fill
                            style={{objectFit: "cover"}}
                            alt="Ing"
                        />
                    </a>
                </div>
            </div>
        </SwiperSlide>
    })
    const onInit = () => {
        console.log('lightGallery has been initialized');
    };
    return (
        <div
            className="section kf-insta-carousel element-anim-1 scroll-animate"
            data-animate="active"
        >
            <div className="container">
                <Swiper {...sliderProps.kfInstaCarousel} className="swiper-container">
                    <div className="swiper-wrapper">
                        {gallery}
                    </div>
                </Swiper>

            </div>
        </div>
    );
};
export default InstaCarousel;
