import {sliderProps} from "@/src/sliderProps";
import {Swiper, SwiperSlide} from "swiper/react";
import Image from "next/image";

const TestimonialsCarousel = () => {
    return (
        <section
            className="section kf-testimonials kf-testimonials-2 section-bg"
            // style={{backgroundImage: "url(images/testimonials_bg2.jpeg)"}}
        >
            <div className="">

                <Image
                    src="/images/toa_bg1.jpg"
                    layout='fill'
                    objectFit='cover'
                    alt="Background Image"
                    style={{opacity:'0.7'}}
                />
            </div>
            <div className="container">
                <div className="kf-titles align-center">
                    <div
                        className="kf-subtitle element-anim-1 scroll-animate"
                        data-animate="active"
                    >
                        Kundenfeedback
                    </div>
                    <h3
                        className="kf-title element-anim-1 scroll-animate"
                        data-animate="active"
                    >
                        Was unsere Kunden sagen
                    </h3>
                </div>
                <div className="kf-testimonials-carousel">
                    <Swiper
                        {...sliderProps.kfTestimonialsCarousel}
                        className="swiper-container"
                    >
                        <div className="swiper-wrapper">
                            <SwiperSlide className="swiper-slide">
                                <div
                                    className="slide-item element-anim-1 scroll-animate"
                                    data-animate="active"
                                >
                                    <div className="image">
                                        <img src="https://lh3.googleusercontent.com/a-/ALV-UjXLDIynL7-Y3NmiPdh0rrIZ2P2JivZK23orBpNDGYqTJg=w120-h120-p-rp-mo-br100" alt="image"/>
                                    </div>
                                    <div className="desc">
                                        <div className="stars">
                                            <i className="fas fa-star"/>
                                            <i className="fas fa-star"/>
                                            <i className="fas fa-star"/>
                                            <i className="fas fa-star"/>
                                            <i className="fas fa-star"/>
                                        </div>
                                        <div className="text">
                                            Wir haben uns spontan entschieden dieses Lokal zu besuchen, da uns die
                                            Innenarchitektur am besten in Chemnitz gefallen hat. Das Essen war wie
                                            erwartet, überragend gut. Man schmeckte die Frische des Fisches heraus und
                                            die Bedienung war höchst freundlich. Eines der besseren Lokale in Chemnitz.
                                        </div>
                                        <h5 className="name">
                                            Chico
                                        </h5>
                                    </div>
                                </div>
                            </SwiperSlide>
                            <SwiperSlide className="swiper-slide">
                                <div
                                    className="slide-item element-anim-1 scroll-animate"
                                    data-animate="active"
                                >
                                    <div className="image">
                                        <img src="https://lh3.googleusercontent.com/a-/ALV-UjUOjdD3K5uTIWstdVgL6aeTs2_414guOLOr8NAW0cKqaOZX=w72-h72-p-rp-mo-br100" alt="image"/>
                                    </div>
                                    <div className="desc">
                                        <div className="stars">
                                            <i className="fas fa-star"/>
                                            <i className="fas fa-star"/>
                                            <i className="fas fa-star"/>
                                            <i className="fas fa-star"/>
                                            <i className="fas fa-star"/>
                                        </div>
                                        <div className="text">
                                            Sehr lecker und das Personal ist sehr freundlich .besonders das Mixgetränk hat einen sehr duftenden und besonderen Geschmack. Wir hatten einen sehr gemütlichen Abend .Wir kommen wieder
                                        </div>
                                        <h5 className="name">
                                            Maria Fischer
                                        </h5>
                                    </div>
                                </div>
                            </SwiperSlide>
                            <SwiperSlide className="swiper-slide">
                                <div
                                    className="slide-item element-anim-1 scroll-animate"
                                    data-animate="active"
                                >
                                    <div className="image">
                                        <img src="https://lh3.googleusercontent.com/a-/ALV-UjVr5fbCwNdCqcw-lBYqVqEWZxtcs_cZA04QyYC33j34sQQ=w72-h72-p-rp-mo-ba4-br100" alt="image"/>
                                    </div>
                                    <div className="desc">
                                        <div className="stars">
                                            <i className="fas fa-star"/>
                                            <i className="fas fa-star"/>
                                            <i className="fas fa-star"/>
                                            <i className="fas fa-star"/>
                                            <i className="fas fa-star"/>
                                        </div>
                                        <div className="kf-text">
                                            Waren dort essen auf Grund einer Empfehlung. Drinnen ein schönes asiatisches Ambiente, was uns sehr gut gefiel. Was nicht so schön war, das die Tische nicht eingedeckt waren. Das hat so was vom Imbissflair. Auch standen die Tische sehr nah beieinander und es war sehr laut drinnen. Das Essen war sehr gut und auch schön angerichtet. Der Service war gut und schnell. Kommen nochmal wieder, da noch viele leckere Sachen auf der Speisekarte stehen.
                                        </div>
                                        <h5 className="name">
                                            Wilhelm Ablass
                                        </h5>
                                    </div>
                                </div>
                            </SwiperSlide>
                            <SwiperSlide className="swiper-slide">
                                <div
                                    className="slide-item element-anim-1 scroll-animate"
                                    data-animate="active"
                                >
                                    <div className="image">
                                        <img src="https://lh3.googleusercontent.com/a-/ALV-UjVCDf75LzmFrL1R6Qm6H26X2A9XyxEupdEyaBU2ZAA0Nv4=w72-h72-p-rp-mo-ba4-br100" alt="image"/>
                                    </div>
                                    <div className="desc">
                                        <div className="stars">
                                            <i className="fas fa-star"/>
                                            <i className="fas fa-star"/>
                                            <i className="fas fa-star"/>
                                            <i className="fas fa-star"/>
                                            <i className="fas fa-star"/>
                                        </div>
                                        <div className="kf-text">
                                            Sehr lecker und gut. Bedienung sehr aufmerksam und freundlich. Küche frisch und schön pur im Geschmack!!! Die Einrichtung mit viel Stil und nicht so kitschig. Absolut zu empfehlen.😍
                                            Heute zum wiederholtem Mal dort gewesen. Noch besserer Service und das Essen wieder sooooo lecker!!!!
                                        </div>
                                        <h5 className="name">
                                            Melanie Greulich
                                        </h5>
                                    </div>
                                </div>
                            </SwiperSlide>
                            <SwiperSlide className="swiper-slide">
                                <div
                                    className="slide-item element-anim-1 scroll-animate"
                                    data-animate="active"
                                >
                                    <div className="image">
                                        <img src="https://lh3.googleusercontent.com/a-/ALV-UjX_q0__V1Jwuq2h2efKDEzNrdaDxGCnnzKFaC76dRpP1w=w72-h72-p-rp-mo-br100" alt="image"/>
                                    </div>
                                    <div className="desc">
                                        <div className="stars">
                                            <i className="fas fa-star"/>
                                            <i className="fas fa-star"/>
                                            <i className="fas fa-star"/>
                                            <i className="fas fa-star"/>
                                        </div>
                                        <div className="kf-text">
                                            Es war wirklich lecker und sehr gemütlich draußen. Kellner waren freundlich.
                                            Es gibt eine volle Auswahl an asiatischen Gerichten, egal ob japanisch,
                                            chinesisch oder vietnamesisch. Tolle selbst gemachte Limonaden oder
                                            Cocktails. Sehr zu empfehlen.
                                        </div>
                                        <h5 className="name">
                                            An Ja
                                        </h5>
                                    </div>
                                </div>
                            </SwiperSlide>
                            <SwiperSlide className="swiper-slide">
                                <div
                                    className="slide-item element-anim-1 scroll-animate"
                                    data-animate="active"
                                >
                                    <div className="image">
                                        <img src="https://lh3.googleusercontent.com/a-/ALV-UjU0LW1ynYv9kjAq8RURqbOlrPvHFvUUtcDKzV_4Cs3aag=w72-h72-p-rp-mo-ba3-br100" alt="image"/>
                                    </div>
                                    <div className="desc">
                                        <div className="stars">
                                            <i className="fas fa-star"/>
                                            <i className="fas fa-star"/>
                                            <i className="fas fa-star"/>
                                            <i className="fas fa-star"/>
                                            <i className="fas fa-star"/>
                                        </div>
                                        <div className="kf-text">
                                            Ich war jetzt schon mehrmals hier essen und bin immer wieder sehr begeistert. Bevorzugt habe ich Sushi gegessen und muss sagen, dass ich bis jetzt noch kein besseres gegessen habe. Auch Freunde waren von der Qualität anderer Gerichte sehr begeistert. Das Preis-Leistungsverhältnis ist völlig gerechtfertigt und in Ordnung.
                                            Wenn das Restaurant sehr voll ist, kann es schon mal vorkommen, dass man eine etwas längere Wartezeit hat. Der Chef ist sehr freundlich, ebenso das restliche Personal. Die Speisen werden richtig schön angerichtet, mit Liebe zum Detail.
                                            Ich kann dieses Restaurant sehr empfehlen.
                                        </div>
                                        <h5 className="name">
                                            Victoria Männel
                                        </h5>
                                    </div>
                                </div>
                            </SwiperSlide>
                        </div>
                        <div className="swiper-pagination"/>
                    </Swiper>
                </div>
            </div>
        </section>
    );
};
export default TestimonialsCarousel;
