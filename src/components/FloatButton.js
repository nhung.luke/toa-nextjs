import React, {useState} from 'react';
import {Button, Dropdown, DropdownTrigger, DropdownMenu, DropdownItem} from "@nextui-org/react";
import MenuIcon from "@/public/icon/MenuIcon";

const FloatButton = ({items, onAction}) => {
    const [isOpen, setIsOpen] = useState(false);
    return (
        <>
            <div className="fixed bottom-4 right-4 z-50 xl:hidden">
                <Dropdown isOpen={isOpen} onOpenChange={setIsOpen} placement="top-right">
                    <DropdownTrigger>
                        <Button
                            isIconOnly
                            // color="primary"
                            size={'lg'}
                            color="danger"
                            aria-label="Floating Button"
                            className="rounded-full shadow-lg"
                        >
                                <MenuIcon style={{ fontSize: '2.5rem' }} />

                        </Button>

                    </DropdownTrigger>
                    <DropdownMenu aria-label="Listbox" className="w-48"
                                  itemClasses={{
                                      base: [
                                          "rounded-md",
                                          "text-default-500",
                                          "transition-opacity",
                                          "data-[hover=true]:text-foreground",
                                          "data-[hover=true]:bg-default-100",
                                          "dark:data-[hover=true]:bg-default-50",
                                          "data-[selectable=true]:focus:bg-default-50",
                                          "data-[pressed=true]:opacity-70",
                                          "data-[focus-visible=true]:ring-default-500",
                                      ],
                                  }}
                                  items={items}
                                  onAction={(item) => onAction(item)}
                    >
                        {(item) => (
                            <DropdownItem
                                key={item.id}
                            >
                                {item.name}
                            </DropdownItem>
                        )}
                    </DropdownMenu>
                </Dropdown>
            </div>
        </>
    );
};

export default FloatButton;