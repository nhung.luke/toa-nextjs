import React, {useState} from 'react'
import {toast} from 'react-toastify';
import Loading from './Loading';

const ContactForm = () => {
    const initialContact = {
        name: '',
        email: '',
        tel: '',
        subject: '',
        message: ''
    };

    const [contactInformation, setContactInformation] = useState(initialContact)
    const [errors, setErrors] = useState({});
    const [loading, setLoading] = useState(false);

    const handleOnChange = (e) => {
        const {name, value} = e.target;
        setContactInformation(prevState => ({
            ...prevState,
            [name]: value
        }));
    }

    const validateForm = () => {
        const newErrors = {};
        if (!contactInformation.name) 
            newErrors.name = 'Bitte geben Sie Ihren vollständigen Namen ein!';
        if (!contactInformation.email) 
            newErrors.email = 'Bitte geben Sie Ihre E-Mail-Adresse ein!';
        if (!contactInformation.tel) 
            newErrors.tel = 'Bitte geben Sie Ihre Telefonnummer ein!';
        if (!contactInformation.subject) 
            newErrors.subject = 'Bitte geben Sie den Betreff Ihrer Nachricht ein!';
        if (!contactInformation.message) 
            newErrors.message = 'Bitte geben Sie Ihre Nachricht ein!';

        setErrors(newErrors);
        return Object.keys(newErrors).length === 0;
    };

    const sendMessageRequest = async () => {
        setLoading(true);
        try {
            const response = await fetch('/api/sendMessage', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(contactInformation),
            });
            toast.success('Nachrichtenanfrage erfolgreich gesendet!');
            setContactInformation(initialContact)
        } catch (error) {
            toast.error('Beim Senden der Nachrichtenanfrage ist ein Fehler aufgetreten!');
        } finally {
            setLoading(false);
        }
    }

    const handleSubmitForm = async (e) => {
        e.preventDefault();
        if (!validateForm()) {
            toast.error('Bitte füllen Sie alle erforderlichen Felder aus!');
            return;
        }
        sendMessageRequest()
    }


    return (
        <div className="container">
            <div
                className="kf-reservation-form element-anim-1 scroll-animate"
                data-animate="active"
            >
                <div className="kf-titles align-center">
                    <div className="kf-subtitle">KONTAKTIEREN SIE UNS</div>
                    <h3 className="kf-title">Senden Sie uns eine Nachricht</h3>
                </div>
                {loading && <Loading/>}
                <form id="cform" method="post">
                    <div className="row">
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                            <div className="kf-field">
                                <input type="text" name="name"
                                       value={contactInformation.name}
                                       onChange={handleOnChange}
                                       placeholder="Vollständiger Name"
                                       required
                                />
                                <i className="far fa-user"/>
                                {errors && <div className="text-danger small">{errors.name}</div>}
                            </div>
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                            <div className="kf-field">
                                <input
                                    type="email"
                                    name="email"
                                    value={contactInformation.email}
                                    onChange={handleOnChange}
                                    placeholder="E-Mail-Adresse"
                                    required
                                />
                                <i className="fas fa-at"/>
                                {errors && <div className="text-danger small">{errors.email}</div>}
                            </div>
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                            <div className="kf-field">
                                <input type="tel" name="tel"
                                       value={contactInformation.tel}
                                       onChange={handleOnChange}
                                       placeholder="Telefonnummer"
                                       required
                                />
                                <i className="fas fa-mobile-alt"/>
                                {errors && <div className="text-danger small">{errors.tel}</div>}
                            </div>
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div className="kf-field">
                                <input type="text" name="subject"
                                       value={contactInformation.subject}
                                       onChange={handleOnChange}
                                       placeholder="Titel"
                                       required
                                />
                                <i className="far fa-envelope"/>{" "}
                                {errors && <div className="text-danger small">{errors.subject}</div>}
                            </div>
                        </div>

                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div className="kf-field">
                    <textarea type="text" name="message"
                              value={contactInformation.message}
                              onChange={handleOnChange}
                              placeholder="Nachricht"
                              required
                    />
                                <i className="far fa-comment-dots"/>{" "}
                                {errors && <div className="text-danger small">{errors.message}</div>}
                            </div>
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div className="kf-bts">
                                <a
                                    className="kf-btn"
                                    onClick={handleSubmitForm}
                                >
                                    <span>SENDEN SIE UNS EINE NACHRICHT</span>
                                    <i className="fas fa-chevron-right"/>
                                </a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default ContactForm