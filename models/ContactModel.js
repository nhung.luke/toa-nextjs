import mongoose from 'mongoose';

const contactSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    tel: {
        type: String,
        required: true
    },
    subject: {
        type: String,
        required: true
    },
    message: {
        type: String,
        required: true
    },
}, { collection: 'contacts', timestamps: true });

const ContactModel = mongoose.models.ContactModel || mongoose.model('ContactModel', contactSchema);
export default ContactModel;
