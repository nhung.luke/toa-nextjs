import mongoose from 'mongoose';

const reservationSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    tel: {
        type: String,
        required: true
    },
    persons: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        required: true
    },
    time: {
        type: String,
        required: true
    },
    message: String
}, { collection: 'reservations', timestamps: true });

const ReservationModel = mongoose.models.ReservationModel || mongoose.model('ReservationModel', reservationSchema);
export default ReservationModel;
